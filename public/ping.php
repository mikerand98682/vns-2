<?php

// ping.php is a target for the ajax vnsJs.keepSessionAlive() function.
session_start();
$_SESSION["vnsping"] = date(DATE_RFC2822);
echo "Session pinged: " . $_SESSION["vnsping"];
