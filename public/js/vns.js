/* global vnsJsConfig */

/**
 * Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
 * 
 * @param {type} vnsJs
 * @param {type} $
 * @param {type} undefined
 * @returns {undefined}
 */
(function (vnsJs, $, undefined) {

    "use strict";

    // /////////////////////////////////////////////////////////////////////////
    // Private Properties.
    // /////////////////////////////////////////////////////////////////////////
    //var privateProp = '';
    var hasChanges         = false, // If true, message popup appears before allowing users to leave page.
        profileFormLocked  = false, // Used after form is locked to prevent repeat header updates.
        passwordFormLocked = false; // Used after form is locked to prevent repeat header updates.

    // /////////////////////////////////////////////////////////////////////////
    // Public Properties
    // /////////////////////////////////////////////////////////////////////////
    //vnsJs.publicProp = "Public Property";

    // /////////////////////////////////////////////////////////////////////////
    // Private Methods
    // /////////////////////////////////////////////////////////////////////////
    /*
    function privateMethod(callback) {
        if (typeof callback === 'function' && callback()) { callback(); }
    }
    */

    function addFormManagement(formEle) {

        if (!formEle.is('form')) { return; }

        $(formEle).find(':submit').prop('disabled', true);

        $(formEle).find(':input').keypress(function(e) { // text written
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
            return;
        });
        $(formEle).find(':input').change(function(e) { // text written
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
            return;
        });
        
        $(formEle).find(':input').keyup(function(e) {
            if (e.keyCode == 8 || e.keyCode == 46) { //backspace and delete key
                $(formEle).find(':submit').prop('disabled', false);
                hasChanges = true;
            } else { // rest ignore
                e.preventDefault();
            }
        });
        $(formEle).find(':text').bind('paste', function(e) { // text pasted
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });

        $(formEle).find('select').change(function(e) { // select element changed
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });

        $(formEle).find(':radio').change(function(e) { // radio changed
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });
        
        $(formEle).find(':checkbox').change(function(e) { // checkbox changed
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });

        $(formEle).find(':password').keypress(function(e) { // password written
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });
        $(formEle).find(':password').bind('paste', function(e) { // password pasted
            $(formEle).find(':submit').prop('disabled', false);
            hasChanges = true;
        });
    }

    function isEmpty(valueIn) {
        if (typeof valueIn === 'undefined') {
            //console.log('isEmpty(' + valueIn + ') === \'undefined\'');
            return true;
        } else if (!valueIn) {
            //console.log('isEmpty(!' + valueIn + ')');
            return true;
        } else if (valueIn === null) {
            //console.log('isEmpty(' + valueIn + ') === null');
            return true;
        } else if (valueIn === 'null') {
            //console.log('isEmpty(' + valueIn + ') === \'null\'');
            //console.log('typeof: ' + typeof valueIn);
            return true;
        } else if (typeof valueIn === 'string' && valueIn.trim().length === 0) {
            //console.log('isEmpty(' + valueIn + ') is string with 0 length');
            return true;
        } else if (valueIn === '') {
            //console.log('isEmpty(' + valueIn + ') is 0 length');
            return true;
        } else {
            //console.log('isEmpty(' + valueIn + ') is not empty');
            //console.log(valueIn);
            return false;
        }
    }

    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }

    function getRadioValue(eleName) {
        var rv = '';
        $('input[name=\'' + eleName + '\']').each(function () {
            if ($(this).prop('checked')) {
                rv = $(this).val();
            }
        });
        return rv;
    }

    /**
     * If password form has changed element this is run to lock the profile form, which is on the same page.
     */
    function lockProfileForm() {
        if (profileFormLocked) { return; }
        profileFormLocked = true;
        $('.profile-title').html(
                $('.profile-title').html() + ' (Save password changes or refesh page to change profile.)'
            );
        $('#profile_FORM #user_fullname').prop('disabled', true);
        $('#profile_FORM #username').prop('disabled', true);
        $('#profile_FORM #affiliation_id').prop('disabled', true);
        $('#profile_FORM #send_reminder').prop('disabled', true);
        $('#profile_FORM :submit').prop('disabled', true);
    }

    /**
     * If profile form has changed element this is run to lock the password form, which is on the same page.
     */
    function lockPasswordForm() {
        if (passwordFormLocked) { return; }
        passwordFormLocked = true;
        $('.password-title').html(
                $('.password-title').html() + ' (Save profile changes or refesh page to change password.)'
            );
        $('#password_FORM #password').prop('disabled', true);
        $('#password_FORM #verify').prop('disabled', true);
        $('#password_FORM :submit').prop('disabled', true);
    }

    // /////////////////////////////////////////////////////////////////////////
    // Public Methods
    // /////////////////////////////////////////////////////////////////////////
    // vnsJs.publicMethod = function () { };

    vnsJs.confirm = function (title, message, callback) {

        document.querySelector('.modal-title').innerHTML = title.replace(/\\/g, '');
        document.querySelector('.modal-body').innerHTML = message.replace(/\\/g, '');
        document.querySelector('#vnsModalCancelBtn').innerHTML = 'Cancel';
        document.querySelector('#vnsModalConfirmBtn').classList.remove('hidden');
        document.getElementById(`vnsModalConfirmBtn`).addEventListener('click', callback);
        $('#vnsModal').modal('show');
    };

    vnsJs.alert = function (title, message) {
        
        document.querySelector('.modal-title').innerHTML = title.replace(/\\/g, '');
        document.querySelector('.modal-body').innerHTML = message.replace(/\\/g, '');
        document.querySelector('#vnsModalCancelBtn').innerHTML = 'Close';
        document.querySelector('#vnsModalConfirmBtn').classList.add('hidden');
        $('#vnsModal').modal('show');
    };

    // Need to manually close modal, bootstrap functions are broken for some reason.
    let modalCloseEles = document.querySelectorAll(`.modal-close`);
    if(modalCloseEles) {
        modalCloseEles.forEach((ele)=>{
            ele.addEventListener('click', ()=>{ $('#vnsModal').modal('hide'); });
        });
    }

    vnsJs.initialize = function () {

        window.onbeforeunload = function () {
            if (hasChanges) {
                return `
                    You have made changes on this page that you have not yet submitted. If you navigate away from this 
                    page you will lose your unsaved changes.
                `;
            }
        };

        $('.vns-confirm').click(function (evt) {
            var href = $(this).attr('href');
            evt.preventDefault();
            vnsJs.confirm($(this).attr('title'), $(this).attr('msg'), function () {
                window.location = href;
            });
        });
       
        // Profile Form ///////////////////////////////////////////////////////
        addFormManagement($('#profile_FORM'));

        // If any profile_FORM fields change need to disable password_FORM
        $('#profile_FORM #user_fullname').change(function () { lockPasswordForm(); });
        $('#profile_FORM #username').change(function () { lockPasswordForm(); });
        $('#profile_FORM #affiliation_id').change(function () { lockPasswordForm(); });
        $('#profile_FORM #send_reminder').change(function () { lockPasswordForm(); });

        // Need to do an ajax check for unique username on change.
        $('#profile_FORM #username').change(function () {
        
            var username = $('#username').val();
            if (isEmpty(username)) {
                $('.username-used-msg').fadeOut().empty();
                return;
            }

            $.getJSON(vnsJsConfig.baseUrl + 'api/usernameused/' + username, function(data) {
                if (data === 'unavailable') {
                    if ($('.username-used-msg').length === 0) {
                        $('#username').after('<span class=\'username-used-msg\' style=\'display:none\'></span>');
                    }
                    $('.username-used-msg').html(
                                        'Username ' + username + ' is already used, please choose a different name.'
                                    ).fadeIn();
                } else {
                    $('.username-used-msg').fadeOut().empty();
                }
            });
        });

        // Submit new profile, validate on submit.
        $('#profile_FORM').submit(function (evt) {

            var isValid = true, msg = '';

            // fullname has a value?
            if (isEmpty($('#user_fullname').val())) {
                $('#user_fullname').addClass('vsc-has-error');
                isValid = false;
                msg += 'Name requires a value.<br/><br/>';
            } else {
                $('#user_fullname').removeClass('vsc-has-error');
            }

            // username has a value?
            if (isEmpty($('#username').val())) {

                $('#username').addClass('vsc-has-error');
                isValid = false;
                msg += 'Username requires a value.<br/><br/>';

            } else if ($('.username-used-msg').length > 0 && !isEmpty($('.username-used-msg').html())) { // unique?

                $('#username').addClass('vsc-has-error');
                isValid = false;
                msg += $('.username-used-msg').html() + '<br/><br/>';

            } else {

                $('#username').removeClass('vsc-has-error');
            }

            // Affiliation has a value?
            if (isEmpty($('#affiliation_id').val())) {

                $('#affiliation_id').addClass('vsc-has-error');
                isValid = false;
                msg += 'Please choose an affiliation.<br/><br/>';

            } else {

                $('#affiliation_id').removeClass('vsc-has-error');
            }

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#profile_FORM :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Password Form //////////////////////////////////////////////////////
        addFormManagement($('#password_FORM'));

        // If any password_FORM fields change, need to disable profile_FORM
        $('#password_FORM #password').change(function () { lockProfileForm(); });
        $('#password_FORM #verify').change(function () { lockProfileForm(); });

        $('#password_FORM').submit(function (evt) {

            var isValid = true, msg = '';

            // Do password and verify have values?
            if (isEmpty($('#password').val())) {
                $('#password').addClass('vsc-has-error');
                isValid = false;
                msg += 'Password requires a value.<br/><br/>';
            } else {
                $('#password').removeClass('vsc-has-error');
            }

            if (isEmpty($('#verify').val())) {
                $('#verify').addClass('vsc-has-error');
                isValid = false;
                msg += 'Verify requires a value.<br/><br/>';
            } else {
                $('#verify').removeClass('vsc-has-error');
            }

            // Do they match?
            if (isValid && $('#password').val() !== $('#verify').val()) {
                $('#password').addClass('vsc-has-error');
                $('#verify').addClass('vsc-has-error');
                isValid = false;
                msg += 'Password and Verify are different, they must be the same value.<br/><br/>';
            }

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#password_FORM :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Subscribe Form /////////////////////////////////////////////////////
        addFormManagement($('#subscribe_FORM'));

        // Do an ajax check for unique username on change.
        $('#subscribe_FORM #username').change(function () {
        
            var username = $('#username').val();
            if (isEmpty(username)) {
                $('.username-used-msg').fadeOut().empty();
                return;
            }

            $.getJSON(vnsJsConfig.baseUrl + 'api/usernameused/' + username, function(data) {
                if (data === 'unavailable') {
                    if ($('.username-used-msg').length === 0) {
                        $('#username').after('<span class=\'username-used-msg\' style=\'display:none\'></span>');
                    }
                    $('.username-used-msg').html(
                                            'Username ' + username + ' is already used, please choose a different name.'
                                        ).fadeIn();
                } else {
                    $('.username-used-msg').fadeOut().empty();
                }
            });
        });

        // Do an ajax check for unique email on change.
        $('#subscribe_FORM #email').change(function () {
        
            var email = $('#email').val();
            if (isEmpty(email)) {
                $('.email-used-msg').fadeOut().empty();
                return;
            }

            $.getJSON(vnsJsConfig.baseUrl + 'api/emailused/' + email, function(data) {
                if (data === 'unavailable') {
                    if ($('.email-used-msg').length === 0) {
                        $('#email').after('<span class=\'email-used-msg\' style=\'display:none\'></span>');
                    }
                    $('.email-used-msg').html(
                                        'Email ' + email + ' is already used, please choose a different email address.'
                                    ).fadeIn();
                } else {
                    $('.email-used-msg').fadeOut().empty();
                }
            });
        });

        // Submit new profile, validate on submit.
        $('#subscribe_FORM').submit(function (evt) {

            var isValid = true, msg = '';

            // user_fullname has a value?
            if (isEmpty($('#user_fullname').val())) {
                $('#user_fullname').addClass('vsc-has-error');
                isValid = false;
                msg += 'Name requires a value.<br/><br/>';
            } else {
                $('#fuser_ull_name').removeClass('vsc-has-error');
            }

            // username has a value?
            if (isEmpty($('#username').val())) {

                $('#username').addClass('vsc-has-error');
                isValid = false;
                msg += 'Username requires a value.<br/><br/>';

            } else if ($('.username-used-msg').length > 0 && !isEmpty($('.username-used-msg').html())) { // unique?

                $('#username').addClass('vsc-has-error');
                isValid = false;
                msg += $('.username-used-msg').html() + '<br/><br/>';

            } else {

                $('#username').removeClass('vsc-has-error');
            }

            // Email has a value?
            if (isEmpty($('#email').val())) {

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += 'Email requires a value.<br/><br/>';

            } else if (!isValidEmail($('#email').val())) { // Is email a valid format?

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += $('#email').val() + ' is an invalid email address.<br/><br/>';

            } else if ($('.email-used-msg').length > 0 && !isEmpty($('.email-used-msg').html())) { // is it unique?

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += $('.email-used-msg').html() + '<br/><br/>';

            } else {

                $('#email').removeClass('vsc-has-error');
            }

            // Affiliation has a value?
            if (isEmpty($('#affiliation_id').val())) {

                $('#affiliation_id').addClass('vsc-has-error');
                isValid = false;
                msg += 'Please choose an affiliation.<br/><br/>';

            } else {

                $('#affiliation_id').removeClass('vsc-has-error');
            }

            // Do password and verify have values?
            if (isEmpty($('#password').val())) {
                $('#password').addClass('vsc-has-error');
                isValid = false;
                msg += 'Password requires a value.<br/><br/>';
            } else {
                $('#password').removeClass('vsc-has-error');
            }

            if (isEmpty($('#verify').val())) {
                $('#verify').addClass('vsc-has-error');
                isValid = false;
                msg += 'Verify requires a value.<br/><br/>';
            } else {
                $('#verify').removeClass('vsc-has-error');
            }

            // Do they match?
            if (isValid && $('#password').val() !== $('#verify').val()) {
                $('#password').addClass('vsc-has-error');
                $('#verify').addClass('vsc-has-error');
                isValid = false;
                msg += 'Password and Verify are different, they must be the same value.<br/><br/>';
            }

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#subscribe_FORM :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Recover Form //////////////////////////////////////////////////////
        addFormManagement($('#recover_FORM'));

        // Submit recover request.
        $('#recover_FORM').submit(function (evt) {

            var isValid = true, msg = '';

            // Email has a value?
            if (isEmpty($('#email').val())) {

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += 'Email requires a value.<br/><br/>';

            } else if (!isValidEmail($('#email').val())) { // Is email a valid format?

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += $('#email').val() + ' is an invalid email address.<br/><br/>';

            } else {

                $('#email').removeClass('vsc-has-error');
            }

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#recover_FORM :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Email Form /////////////////////////////////////////////////////////
        // Email form appears in two places, create page and update page.
        addFormManagement($('#email_FORM'));

        // Do an ajax check for unique email on change.
        $('#email_FORM #email').change(function () {
        
            var email = $('#email_FORM #email').val(),
                aid = $('#email_FORM #aid').val(); // Will be 0 if creating new email.

            if (isEmpty(email)) {
                $('.email-used-msg').fadeOut().empty();
                return;
            }

            $.getJSON(vnsJsConfig.baseUrl + 'api/emailused/' + email, function(data) {
                if (data === 'unavailable' || (aid == 0 && data === 'mine')) {
                    if ($('.email-used-msg').length === 0) {
                        $('#email').after('<span class=\'email-used-msg\' style=\'display:none\'></span>');
                    }
                    $('.email-used-msg').html(
                                    'Email ' + email + ' is already used, please choose a different email address.'
                                ).fadeIn();
                } else {
                    $('.email-used-msg').fadeOut().empty();
                }
            });
        });

        $('#email_FORM').submit(function (evt) {

            var isValid = true, msg = '';

            // Email has a value?
            if (isEmpty($('#email').val())) {

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += 'Email requires a value.<br/><br/>';

            } else if (!isValidEmail($('#email').val())) { // Is email a valid format?

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += $('#email').val() + ' is an invalid email address.<br/><br/>';
            
            } else if ($('.email-used-msg').length > 0 && !isEmpty($('.email-used-msg').html())) { // is it unique?

                $('#email').addClass('vsc-has-error');
                isValid = false;
                msg += $('.email-used-msg').html() + '<br/><br/>';

            } else {

                $('#email').removeClass('vsc-has-error');
            }

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#email_FORM :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Confirm Form(s) /////////////////////////////////////////////////////////
        $('.confirm-form').each(function () { // May be several confirm forms on a page.

            addFormManagement($(this));

            // Disable other confirm forms once user edits current one.
            $(this).find('[name="confirmation_code"]').keyup(function () {
                var aid = $(this).attr('aid');
                $('[name="confirmation_code"]').each(function() {
                    if ($(this).attr('aid') != aid) {
                        //console.log($(this).attr('aid') + ' != ' + aid);
                        $(this).prop('disabled', true);
                        $(this).prop('placeholder', 'Confirm other or refesh.');
                    }
                });
            });

            //
            $(this).submit(function (evt) {

                var isValid = true, msg = '', confirmation_code = $(this).find('[name="confirmation_code"]');

                // code has a value?
                if (isEmpty(confirmation_code.val()) || !$.isNumeric(confirmation_code.val())) {

                    $(confirmation_code).addClass('vsc-has-error');
                    isValid = false;
                    msg += 'Confirmation code requires a numeric value.<br/><br/>';

                } else {

                    $(confirmation_code).removeClass('vsc-has-error');
                }

                if (isValid) {
                    hasChanges = false;
                } else {
                    evt.preventDefault();
                    vnsJs.alert('Please correct these errors and resubmit.', msg);
                    $(this).find(' :submit').addClass('btn-danger').removeClass('btn-primary');
                }
            });

        });


        // Volcano Preference Form ////////////////////////////////////////////
        addFormManagement($('#volcpref_FORM'));

        //
        $('#volcpref_FORM').submit(function (evt) {

            var isValid = true, msg = ''; // Not doing validation on this form yet. TODO: need to check for option combinations that don't make sense.

            if (isValid) {
                hasChanges = false;
            } else {
                evt.preventDefault();
                vnsJs.alert('Please correct these errors and resubmit.', msg);
                $('#volcpref_FORM').find(' :submit').addClass('btn-danger').removeClass('btn-primary');
            }
        });

        // Unsubscribe Form(s) /////////////////////////////////////////////////////////
        $('#unsubscribe_FORM').submit(function (evt) {

            evt.preventDefault();
            vnsJs.confirm(
                    'Confirm Unsubscribe', 
                    'Unsubscribe from the Volcano Notification Service? All account information will be removed.', 
                function () {
                window.location = $('#unsubscribe_FORM').attr('action');
            });
        });

        // End Init //
    };

}(window.vnsJs = window.vnsJs || {}, jQuery));

$(document).ready(function () {

    'use strict';

    // IE fix for console
    if (!window.console) {
        var console = {
                log : function(){},
                warn : function(){},
                error : function(){},
                time : function(){},
                timeEnd : function(){}
             };
    }
    vnsJs.initialize();
});