/* global vnsJsConfig */
/**
 * Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
 * 
 * @param {type} vnsVMap
 * @param {type} $
 * @param {type} undefined
 * @returns {undefined}
 */
(function (vnsVMap, $, undefined) {

    "use strict";

    // /////////////////////////////////////////////////////////////////////////
    // Private Properties.
    // /////////////////////////////////////////////////////////////////////////
    //var privateProp = '';
    var t, l;

    var icons = {
        'unassigned_unassigned': {'width':18,'height':14},
        'green_normal':          {'width':21,'height':16},
        'yellow_advisory':       {'width':27,'height':20},
        'yellow_watch':          {'width':27,'height':20},
        'orange_watch':          {'width':33,'height':24},
        'orange_warning':        {'width':33,'height':24},
        'red_warning':           {'width':40,'height':31},
        'red_watch':             {'width':40,'height':31}
    };

    var map = {},          // esri.Map
        myAllObsHash = [], // Data from embedded json, #volcprefs-all-json, my all_OBS selections.
        myPrefsHash = [],  // Data from embedded json, #volcprefs-json, my volcpref selections.
        allVolcsHash = {}, // Data from the volcanoes.json file as an object where volcano_cd is the hash key.
        volcData = [],     // Array of volcano data along with user selections. For current state and to redraw map.
        vhpStatus = [];    // Array of data from vhp_status.xml


    // Object containing an element of volc data.
    var volcDataRec = function () {

        'use strict';

        this.vcd = '';            // HANS Volcano CD
        this.vnum = '';           // GVP Volcano ID
        this.vname = '';          // Volcano Name
        this.latitude = 0.0;      // Volcano Latitude
        this.longitude = 0.0;     // Volcano Longitude
        this.cc = '';             // Volcano Color Code
        this.al = '';             // Volcano Alert Level
        this.vimg = '';           // Volcano Image URL
        this.href = '';           // Volcano Link URL
        this.obs_abbr = '';       // Observatory abbreviation.
        this.region = '';         // Volcano region.
        this.selected = false;    // True if user currently has this volcnao selected.
        this.obsSelected = false; // True if selected through observatory.
    };

    // /////////////////////////////////////////////////////////////////////////
    // Public Properties
    // /////////////////////////////////////////////////////////////////////////
    //vnsVMap.publicProp = "Public Property";

    // /////////////////////////////////////////////////////////////////////////
    // Private Methods
    // /////////////////////////////////////////////////////////////////////////
    /*
    function privateMethod(callback) {
        if (typeof callback === 'function' && callback()) { callback(); }
    }
    */

    // Used to do initial build of the volcData object from source json files.
    // callback should build/render initial map.
    function buildVolcData(callback) {

        // Loop through all vhpStatus records
        for (var idx = 0; idx < vhpStatus.length; idx++) {

            var vdRec = new volcDataRec(),
                marker = vhpStatus[idx];

            // Transform marker information from vhp_status.xml into data object
            vdRec.vcd = marker.getAttribute("vcd");             // HANS Volcano CD
            vdRec.vnum = marker.getAttribute("vnum");           // GVP Volcano ID
            vdRec.vname = marker.getAttribute("volcano_name");  // Volcano Name
            vdRec.latitude = marker.getAttribute("latitude");   // Volcano Latitude
            vdRec.longitude = marker.getAttribute("longitude"); // Volcano Longitude
            vdRec.cc = marker.getAttribute("color_code");       // Volcano Color Code
            vdRec.al = marker.getAttribute("alert_level");      // Volcano Alert Level
            vdRec.vimg = marker.getAttribute("image");          // Volcano Image URL
            vdRec.href = marker.getAttribute('obslink');        // Volcano Link URL

            // Get observatory and region from volcanoes.json
            if (allVolcsHash[vdRec.vcd]) {
                vdRec.obs_abbr = allVolcsHash[vdRec.vcd].obs_abbr;
                vdRec.region = allVolcsHash[vdRec.vcd].region;
            }

            // Now determine if this volcano has a volcpref.
            vdRec.obsSelected = false;
            vdRec.selected = false;
            if (myAllObsHash[vdRec.obs_abbr]) { // Is there a value in the all observatory hashmap?
                vdRec.selected = true;
                vdRec.obsSelected = true;
            } else if (!vdRec.selected && myPrefsHash[vdRec.vcd]) { // Not selected by observatory, is there a volcano selection?
                vdRec.selected = true;
            }

            volcData.push(vdRec);
        }

        if (typeof callback === 'function' && callback()) { callback(); }
    }

    // Shows volcano labels on map.
    function mapMouseOver(evt){
        $("#mh").html(evt.graphic.attributes.vname);
        t = $('#vmap_DIV').position().top + evt.screenPoint.y + 24; //top_offset;
        l = $('#vmap_DIV').position().left + evt.screenPoint.x + 12; // Left offset
        $('#mh').css({'display':'block', 'position':'absolute', 'top':t + 'px', 'left':l + 'px'});
    }

    // Hides volcano lables on map.
    function mapMouseOut(evt){
        $("#mh").css({'display':'none'});
    }

    // This simply puts markers with higher colors after lower ones. Result: on the map, highest colors are on top.
    function reorderMarkers(markers) {

        var unassigned = [],
            green      = [],
            yellow     = [],
            orange     = [],
            red        = [],
            markersOut = [];

        for (var idx = 0; idx < markers.length; idx++) {
            switch (markers[idx].getAttribute("color_code")) {
                case 'UNASSIGNED':
                    unassigned.push(markers[idx]);
                    break;
                case 'GREEN':
                    green.push(markers[idx]);
                    break;
                case 'YELLOW':
                    yellow.push(markers[idx]);
                    break;
                case 'ORANGE':
                    orange.push(markers[idx]);
                    break;
                case 'RED':
                    red.push(markers[idx]);
                    break;
                default:
                    unassigned.push(markers[idx]);
                    break;
            }
        }

        markersOut = markersOut.concat(unassigned, green, yellow, orange, red); // Last will be on top of others on map.
        return markersOut;
    }

    // Draws the map. Should allow for redraws if data array changes (if we want to get more responsive
    // with the interface).
    function drawMap() {

        // Add elements map should be placed in if not already present.
        if (!$('#map').length) {

            $('#vmap_DIV').html(
                '<div class="panel panel-primary">' +
                '    <div class="panel-heading">' +
                '        <h3 class="panel-title">Available Volcanoes - Map Selector</h3>' +
                '    </div>' +
                '    <div class="panel-body" id="map"></div>' +
                '    &nbsp;<i>Use map to choose volcanoes or select/remove with options below. Circles with solid borders are selected.</i>' +
                '</div>' +
                '<div id="mh"></div>'
            ).show();
        }

        // Build the map.
        var volcLayer = {},      // esri.layers.GraphicsLayer()
            vdRec = {},          // One volcDataRec
            point = {},          // esri.geometry.Point()
            roundSymbol = {},    // esri.symbol.SimpleMarkerSymbol()
            font = {},           // esri.symbol.Font()
            textSymbol = {},     // esri.symbol.TextSymbol()
            graphic = {},        // esri.Graphic()
            textGraphic = {},    // esri.Graphic()
            addHtml = '',        // HTML code for infoTemplate that adds a volcano
            addTemplate = {},    // esri.InfoTemplate()
            delHtml = '',        // HTML code for infoTemplate that removes a volcano
            delTemplate = {};    // esri.InfoTemplate(), remove volcano selection

        require([
                "esri/map",
                "esri/Color",
                "esri/geometry/Point",
                "esri/graphic",
                "esri/layers/GraphicsLayer",
                "esri/symbol",
                "esri/symbols/Font",
                "esri/symbols/SimpleLineSymbol",
                "esri/symbols/SimpleMarkerSymbol",
                "dojo/domReady!"
            ], function (
                Map,
                Color,
                Point,
                Graphic,
                GraphicsLayer,
                symbol,
                Font,
                SimpleLineSymbol,
                SimpleMarkerSymbol
            ) {

            var colors = {
                'RED'        : new Color([255, 0, 0, 0.9]),
                'ORANGE'     : new Color([255, 128, 0, 0.9]),
                'YELLOW'     : new Color([255, 255, 0, 0.9]),
                'GREEN'      : new Color([0, 255, 0, 0.9]),
                'UNASSIGNED' : new Color([255, 255, 255, 0.9])
            };

            addHtml =
                '<div style=\'float:left;margin-right:1em;\'>' +
                '    <b><a href=\'${href}\'>Volcano Page</a></b><br/>' +
                '    Color Code: ${cc}<br/>Alert Level: ${al}<br/>' +
                '    <a href=\'volcanoes/add/${vcd}\'>Receive Notices for ${vname}</a>' +
                '</div>' +
                '<div style=\'float:left;margin:0;\'>' +
                '    <a href=\'${href}\'><img src=\'${vimg}\' width=\'85px;\' /></a>' +
                '</div>';

            addTemplate = new esri.InfoTemplate("${vname}", addHtml);

            delHtml =
                '<div style=\'float:left;margin-right:1em;\'>' +
                '    <b><a href=\'${href}\'>Volcano Page</a></b><br/>' +
                '    Color Code: ${cc}<br/>Alert Level: ${al}<br/>' +
                '    <a href=\'volcanoes/delete/${vcd}\'>Stop Receiving Notices for ${vname}</a>' +
                '</div>' +
                '<div style=\'float:left;margin:0;\'>' +
                '    <a href=\'${href}\'><img src=\'${vimg}\' width=\'85px;\' /></a>' +
                '</div>';

            delTemplate = new esri.InfoTemplate("${vname}", delHtml);

            map = new Map("map", {
                    basemap: "gray",
                    center: [ -160, 45 ],
                    zoom: 3,
                    logo: false//,
                    //showAttribution: false
                });

            volcLayer = new GraphicsLayer();

            for (var idx = 0; idx < volcData.length; idx++) {

                vdRec = volcData[idx];

                point = new Point(vdRec.longitude, vdRec.latitude);

                if (vdRec.obsSelected) {
                    vdRec.vcd = 'all_' + vdRec.obs_abbr;
                    vdRec.vname = myAllObsHash[vdRec.obs_abbr].obs_fullname;
                } // 

                if (vdRec.selected) {
                    roundSymbol = new SimpleMarkerSymbol("solid", 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 'black', 1), colors[vdRec.cc]);
                    graphic = new Graphic(point, roundSymbol, vdRec, delTemplate);
                } else {
                    roundSymbol = new SimpleMarkerSymbol("solid", 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_DOT, 'black', 1), colors[vdRec.cc]);
                    graphic = new Graphic(point, roundSymbol, vdRec, addTemplate);
                }

                volcLayer.add(graphic);

                font = new symbol.Font("12pt", Font.STYLE_ITALIC, Font.VARIANT_NORMAL, Font.WEIGHT_BOLD, "Arial");
                textSymbol = new symbol.TextSymbol(vdRec.vname, font, new dojo.Color("white"));
                textGraphic = new Graphic(point, textSymbol, vdRec, addTemplate);
                textGraphic.hide();
                volcLayer.add(textGraphic);
            }
            map.addLayer(volcLayer);
            dojo.connect(volcLayer, "onMouseOver", mapMouseOver);
            dojo.connect(volcLayer, "onMouseOut", mapMouseOut);
        });
    }

    // /////////////////////////////////////////////////////////////////////////
    // Public Methods
    // /////////////////////////////////////////////////////////////////////////
    // vnsVMap.publicMethod = function () { };

    vnsVMap.initialize = function () {

        // Load each type of data into memory then draw map.
        $.get(vnsVars.vhpStatusPath, function(data){ // Loads vhp_status.xml

            vhpStatus = reorderMarkers(data.documentElement.getElementsByTagName("marker"));

            $.getJSON(vnsJsConfig.baseUrl + 'resources/json/volcanoes.json', function (data) {

                // Build allVolcsHash from volcanoes.json, avoids repeated looping of data.
                for (var idx = 0; idx < data.length; idx++) {
                    allVolcsHash[data[idx].volcano_cd] = data[idx];
                }

                // Build hashmap of all_OBS selections.
                var myAllObs = JSON.parse($('#volcprefs-all-json').html());
                for (var idx = 0; idx < myAllObs.length; idx++) {
                    myAllObsHash[myAllObs[idx].obs_abbr] = myAllObs[idx];
                }

                // Build hashmap of volcpref options.
                var myPrefs = JSON.parse($('#volcprefs-json').html());
                for (var idx = 0; idx < myPrefs.length; idx++) {
                    myPrefsHash[myPrefs[idx].volcano_cd] = myPrefs[idx];
                }

                // Take loaded data, hashmaps and make the volcData array of volcanoDataRec objects and then draw the map.
                buildVolcData(function () {
                    drawMap(); // Finally draw the map!
                });
            });
        });

    }; // End vnsVMap.initialize //

}(window.vnsVMap = window.vnsVMap || {}, jQuery));

$(document).ready(function () {

    'use strict';
    vnsVMap.initialize();
});