<?php

use Phalcon\Session\Manager;
use Phalcon\Session\Adapter\Stream;
use Phalcon\Mvc\ViewBaseInterface;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Flash\Session;

try {

    $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");

    $adapter = new Phalcon\Logger\Adapter\Stream($config->application->logsDir . 'vns.log');
    $logger = new Phalcon\Logger\Logger('messages', [ 'main' => $adapter ]);
    
    define('BASE_PATH', dirname(__DIR__ . '/..'));
    define('APP_PATH', BASE_PATH . '/app');
    
    $loader = new \Phalcon\Autoload\Loader();

    if (!function_exists('str_contains')) {
        function str_contains($haystack, $needle) {
            return $needle != '' && mb_strpos($haystack, $needle) != false;
        }
    }

    // read user config - in case we need to connect to a different db with different usernames/passwords/etc.

    $loader->setNamespaces(array(
        "VHP\Vns\Models" => "../app/models/",
        "VHP\Vns\Controllers" => "../app/controllers/",
        "VHP\Vns\Forms" => "../app/forms/",
        "VHP\Vns\Library" => "../app/library/"
    ));

    $loader->register();

    //require_once __DIR__ . "/../vendor/autoload.php";

    $di = new \Phalcon\DI\FactoryDefault();

    $di->set("config", $config);
    if (isset($user_config)) {
        $di->set("user_config", $user_config);
    }

    $di->set("router", function () use ($config) {

        /*
          $router = new \Phalcon\Mvc\Router\Annotations(FALSE);
          $router->addResource("Api", "/api");
         */

        $router = new \Phalcon\Mvc\Router();
        $router->addGet("/api/getpost/{:int}", array(
            "controller" => "api",
            "action" => "getpost"
        ));

        $router->addPost("/api/addpost", array(
            "controller" => "api",
            "action" => "addpost"
        ));

        return $router;
    });

    $di->set("dispatcher", function () {

        $dispatcher = new \Phalcon\Mvc\Dispatcher();

        // create manager to look for not found errors
        $em = new \Phalcon\Events\Manager();
        $em->attach("dispatch", function ($event, $dispatcher, $exception) {
            if ($event->getType() == "beforeException") {
                switch ($exception->getCode()) {
                    case \Phalcon\Dispatcher\Exception::EXCEPTION_HANDLER_NOT_FOUND:
                    case \Phalcon\Dispatcher\Exception::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(array("controller" => "index", "action" => "notfound"));
                        return FALSE;
                }
            }
        });

        $dispatcher->setDefaultNamespace("VHP\Vns\Controllers");
        $dispatcher->setEventsManager($em);
        return $dispatcher;
    });

    /**
     * create db connection to user db -- we'll just create a separate connection here, in case we need to
     * use separate usernames/passwords to connect to the user auth db
     */
    // Database connection for reading data from localhost. 
    $di->setShared("dbr", function () use ($config) {
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql([
            "host"       => $config->readdb->host,
            "username"   => $config->readdb->username,
            "password"   => $config->readdb->password,
            "dbname"     => $config->readdb->name,
            "charset"    => "utf8",
            'persistent' => true,
            'options'    => [
                PDO::ATTR_PERSISTENT                   => true,
                PDO::MYSQL_ATTR_SSL_CA                 => "/etc/pki/tls/certs/ca-bundle.crt",
                PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => 'TrustServerCertificate'
            ]
        ]);

        // log all sql queries to a file -- turn this off when moving into production.
        // TODO: could probably check the config file for production environment and turn this off
        /*
          $eventsManager = new \Phalcon\Events\Manager();
          $logger = new \Phalcon\Logger\Adapter\File($config->application->logsDir . "sql.log");
          $eventsManager->attach("db", function($event, $connection) use ($logger){
          if($event->getType() == "beforeQuery"){
          $logger->log($connection->getRealSQLStatement());
          }
          });

          $connection->setEventsManager($eventsManager);
         */
        return $connection;
    });

    // Database connection for writing data to master database.
    $di->setShared("dbu", function () use ($config) {
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql([
            "host"       => $config->updatedb->host,
            "username"   => $config->updatedb->username,
            "password"   => $config->updatedb->password,
            "dbname"     => $config->updatedb->name,
            "charset"    => "utf8",
            'persistent' => true,
            'options'    => [
                PDO::ATTR_PERSISTENT                   => true,
                PDO::MYSQL_ATTR_SSL_CA                 => "/etc/pki/tls/certs/ca-bundle.crt",
                PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => 'TrustServerCertificate'
            ]
        ]);
        return $connection;
    });

    $di->setShared("cookies", function () {
        return new \Phalcon\Http\Response\Cookies();
    });

    // the 'flash' object lets us flash messages to the user -- using bootstrap css classes to style the related messages
    $di->set('flash', function () {
            $flash = new Session();
            $flash->setCssClasses(
                    [
                        "error" => "alert alert-danger vsc-alert",
                        "success" => "alert alert-success vsc-alert",
                        "notice" => "alert alert-info vsc-alert",
                        "warning" => "alert alert-warning vsc-alert"
                    ]
            );
            $flash->setAutoescape(false);
            return $flash;
	});
    
    // primary volt engine for rendering files to the browser
    // turn off the compileAlways flag on production (check config file?)
    $di->setShared('volt', function (ViewBaseInterface $view) use ($di, $config) {

        $volt = new Volt($view, $di);
        $compile = ($config->application->mode != 'production');
        $volt->setOptions(
                [
                    'always' => $compile,
                    'path' => __DIR__ . '/../cache/volt/',
                ]
        );
        
        $compiler = $volt->getCompiler();
        $compiler->addFunction("is_a", "is_a");

        return $volt;
    });
    /*
    // create a volt just for rendering to variables
    $di->set("rendering_volt", function ($view, $di) use ($config) {

        $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
        $volt->setOptions(array(
            "compiledPath" => __DIR__ . "/../cache/volt/",
            "compileAlways" => ($config->application->mode != 'production') // Compile always when not production.
        ));

        $compiler = $volt->getCompiler();
        $compiler->addFunction("is_a", "is_a");

        return $volt;
    }, TRUE);
    */
    // primary volt engine for rendering files to the browser
    // turn off the compileAlways flag on production (check config file?)
    $di->setShared('rendering_volt', function (ViewBaseInterface $view) use ($di, $config) {

        $volt = new Volt($view, $di);
        $compile = ($config->application->mode != 'production');
        $volt->setOptions(
                [
                    'always' => $compile,
                    'path' => __DIR__ . '/../cache/volt/',
                ]
        );
        
        $compiler = $volt->getCompiler();
        $compiler->addFunction("is_a", "is_a");
        return $volt;
    });
    
    $di->set("view", function () use ($config) {
        
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir("../app/views/");
        $view->registerEngines(array(".volt" => "volt"));
        $view->rewriteUriMJR = '/' . basename($_SERVER['REQUEST_URI']);
        $view->config = $config;
        return $view;
    });

    // create a view just for rendering templates etc to variables
    $di->set("rendering_view", function () use ($config) {
        
        $view = new \Phalcon\Mvc\View\Simple();
        $view->setViewsDir("../app/views/");
        $view->registerEngines(array(".volt" => "rendering_volt"));
        $view->rewriteUriMJR = '/' . basename($_SERVER['REQUEST_URI']);
        $view->config = $config;
        return $view;
    });

    $di->set("url", function () use ($config) {
        $url = new \Phalcon\Mvc\Url();
        //$url->setBaseUri($config->application->baseUrl);
        $url->setBaseUri('/vns2/');
        return $url;
    });

    // Start the session the first time when some component request the session service
    $di->setShared('session', function () {
        $session = new Manager();
	$files = new Stream();
	$session->setAdapter($files);
	$session->start();
        return $session;
    });

    // Set up forms array for pages containing multiple forms....
    $di["forms"] = function () {
        return new \Phalcon\Forms\Manager();
    };

    // // //
    $app = new \Phalcon\Mvc\Application($di);
    
    echo $app->handle($_GET["_url"] ?? '/')->getContent();
    
} catch (\Phalcon\Exception $e) {
    
    echo $e->getMessage();
}