{% extends "templates/base_vis.volt" %}

{% block title %}My Notification Types{% endblock %}

{% block content %}

<div class="page-header"><h1>My Notification Types</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">My Notification Types</h3>
            </div>
            <div class="panel-body">
                {% set lastCat = "" %}
                {% for noticeType in noticeTypes %}
                    {% if lastCat != noticeType.notice_category %}
                        {% if lastCat != "" and catTotal == 0 %}None Selected{% endif %}
                        {% set lastCat = noticeType.notice_category %}
                        {% set catTotal = 0 %}
                        <h4>{{ noticeType.notice_category }}</h4>
                    {% endif %}
                    {% set hasType = false %}
                    {% for mail_type in user.mail_type %}
                        {% if mail_type.hans_type_id == noticeType.legacy_type_id %}{% set hasType = true %}{% endif %}
                    {% endfor %}
                    {% if hasType %}
                        {% set catTotal = catTotal + 1 %}
                        <div class="row vsc-padded">
                            <div class="col-md-9">
                                <b>{{ noticeType.notice_type }}</b><br/>
                                <i>{{ noticeType.type_description }}</i>
                            </div>
                            <div class="col-md-3">
                                {{ link_to("notifications/remove/" ~ noticeType.legacy_type_id, "Remove Type", 'class':'vns-confirm','title':'Remove','msg':'Remove notification type?') }}
                            </div>
                        </div>
                    {% endif %}
                {% endfor %}
                {% if lastCat != "" and catTotal == 0 %}None Selected{% endif %}
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Available Notification Types</h3>
            </div>
            <div class="panel-body">
                {% set lastCat = "" %}
                {% for noticeType in noticeTypes %}
                    {% if noticeType.type_deleted_ind == 'Y' %}{% continue %}{% endif %}
                    {% if lastCat != noticeType.notice_category %}
                        {% if lastCat != "" and catTotal == 0 %}All Included{% endif %}
                        {% set lastCat = noticeType.notice_category %}
                        {% set catTotal = 0 %}
                        <h4>{{ noticeType.notice_category }}</h4>
                    {% endif %}
                    {% set hasType = false %}
                    {% for mail_type in user.mail_type %}
                        {% if mail_type.hans_type_id == noticeType.legacy_type_id %}{% set hasType = true %}{% endif %}
                    {% endfor %}
                    {% if !hasType %}
                        {% set catTotal = catTotal + 1 %}
                        <div class="row vsc-padded">
                            <div class="col-md-9">
                                <b>{{ noticeType.notice_type }}</b><br/>
                                <i>{{ noticeType.type_description }}</i>
                            </div>
                            <div class="col-md-3">{{ link_to("notifications/add/" ~ noticeType.legacy_type_id, "Include") }}</div>
                        </div>
                    {% endif %}
                {% endfor %}
                {% if lastCat != "" and catTotal == 0 %}All Included{% endif %}
            </div>
        </div>
        <!--
        {% if noticeActivity is defined %}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Notification Activity</h3>
                </div>
                <div class="panel-body">

                    <b><i>How often have various notice types been sent in the last year?</i></b>
                    <br/><br/>

                    {% set lastObs = "" %}
                    {% set lastCat = "" %}

                    {% for noticeType in noticeActivity %}

                        {% if lastObs != noticeType.obs_fullname or lastCat != noticeType.notice_category %}
                            {% set lastObs = noticeType.obs_fullname %}
                            {% set lastCat = noticeType.notice_category %}
                            <h4>{{ noticeType.obs_fullname }} - {{ noticeType.notice_category }}</h4>
                            <div class="row vsc-padded">
                                <div class="col-md-6"><b>Notice Type</b></div>
                                <div class="col-md-3"><b>Last Sent (UTC)</b></div>
                                <div class="col-md-3"><b>Total Sent</b></div>
                            </div>
                        {% endif %}

                        <div class="row vsc-padded">
                            <div class="col-md-6">{{ noticeType.notice_type }}</div>
                            <div class="col-md-3">{{ noticeType.sent_utc }}</div>
                            <div class="col-md-3">{{ noticeType.obstot }}</div>
                        </div>

                    {% endfor %}

                </div>
            </div>
        {% endif %}
        -->

    </div>
</div>

{% endblock %}