{% extends "templates/base_vis.volt" %}

{% block title %}My Profile{% endblock %}

{% block content %}

<div class="page-header"><h1>My Profile</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title profile-title">Edit My Profile</h3>
            </div>
            <div class="panel-body">

                {{ form(["action": config.application.baseUrl ~ "profile/profile", "role": "form", "id": "profile_FORM", "class": "form"]) }}

                    {{ hidden_field("user_id")}}

                    <div class="form-group">
                        <label for="user_fullname" class="control-label">Name</label>
                        {{ text_field("user_fullname", "size": 20, "class": "form-control", "placeholder" : "Full Name")}}
                    </div>

                    <div class="form-group">
                        <label for="username" class="control-label">Username</label>
                        {{ text_field("username", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "Username")}}
                    </div>

                    <div class="form-group">
                        <label for="affiliation_id" class="control-label">Affiliation</label>
                        {{ select(['affiliation_id', affiliations, 'using': ['affiliation_id', 'affiliation'], 'useEmpty': true, 'emptyText': 'Choose Affiliation...', 'emptyValue': '', "class": "form-control"]) }}
                    </div>

                    <div class="form-group">
                        <label for="send_reminder" class="control-label">Send Reminder</label>
                        {% if user.send_reminder == "y" %}
                            <input type="checkbox" name="send_reminder" id="send_reminder" value="y" checked="checked"/>
                        {% else %}
                            <input type="checkbox" name="send_reminder" id="send_reminder" value="y"/>
                        {% endif %}
                        <br/><i>Reminders are sent infrequently with a summary of your account settings.</i>
                    </div>
                    <br/>
                    {{ submit_button("Submit", "class": "btn btn-primary btn-sm pull-right") }}
                    <br/>
                {{ close('form') }}

            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title password-title">Change My Password</h3>
            </div>
            <div class="panel-body">
                <p>
                    Passwords must be between 4 and 15 characters, and contain at least one number.
                </p>
                {{ form(["action": config.application.baseUrl ~ "profile/password", "role": "form", "id": "password_FORM", "class": "form"]) }}

                    {{ hidden_field("user_id")}}

                    <div class="form-group">
                        <label for="password" class="control-label">New Password</label>
                        {{ password_field("password", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "New Password")}}
                    </div>

                    <div class="form-group">
                        <label for="password" class="control-label">Verify</label>
                        {{ password_field("verify", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "Verify")}}
                    </div>

                    {{ submit_button("Change", "class": "btn btn-primary btn-sm pull-right") }}
                    <br/>
                {{ close('form') }}
            </div>
        </div>

    </div>
</div>

{% endblock %}