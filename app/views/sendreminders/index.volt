{% extends "templates/base_vis.volt" %}

{% block title %}Send Reminders{% endblock %}

{% block content %}

<div class="page-header"><h1>Send Reminders</h1></div>

<div class="well">
	Opening this page sends reminders to users that haven't received one in the last 6 months. It should be called 
    via cron/wget (or similar process) from an internal server.
</div>

{% endblock %}