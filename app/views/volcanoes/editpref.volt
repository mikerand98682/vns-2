{% extends "templates/base_vis.volt" %}

{% block title %}My Volcanoes - Edit preferences for {{ volcano_name }}{% endblock %}

{% block content %}

    <div class="page-header"><h1>My Volcanoes - Edit preferences for {{ volcano_name }}</h1></div>

    <div class="row vsc-padded">

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit preferences for {{ volcano_name }}</h3>
                </div>
                <div class="panel-body">

                    {% set ccColor = "black" %}
                    {% set colorCode = "UNASSIGNED" %}
                    {% if currentCodes.color_code is defined and currentCodes.color_code != "UNASSIGNED" and currentCodes.color_code != "" %}
                        {% set ccColor = currentCodes.color_code|lower %}
                        {% set colorCode = currentCodes.color_code %}
                    {% endif %}
                    {% if ccColor == "yellow" %}{% set ccColor = "gold" %}{% endif %}
                    Current Color Code: <b style="color:{{ ccColor }}">{{ colorCode }}</b><br/>

                    {% set alertLevel = "UNASSIGNED" %}
                    {% if currentCodes.alert_level is defined %}{% set alertLevel = currentCodes.alert_level %}{% endif %}
                    Current Alert Level: <b style="color:black">{{ alertLevel }}</b></br>
                    <br/>
                    {# TODO: Need to write instructions.
                    {{ link_to("https://www.usgs.gov/natural-hazards/volcano-hazards/notifications", "Click here for an explanation of these color codes and alert levels.") }}<br/>
                    <br/>
                    #}
                    <h4>Send me alerts for {{ volcano_name }} when:</h4>
                    {{ form(["action": config.application.baseUrl ~ "volcanoes/editpref", "role": "form", "id": "volcpref_FORM", "class": "form"]) }}

                        {{ hidden_field("volcano_cd")}}
                        {{ hidden_field("volcpref_id")}}
                        {{ hidden_field("obs_abbr")}}

                        <div class="row vsc-padded">
                            <div class="col-md-3">
                                <h4>Current color code is:</h4>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ select(['aviat_qual', aviat_qual_Options, 'useEmpty': true, 'emptyText': 'Choose..', 'emptyValue': '', "class": "form-control"]) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ select(['color_code', color_code_Options, 'useEmpty': true, 'emptyText': 'Any', 'emptyValue': '', "class": "form-control"]) }}
                                </div>
                            </div>
                        </div>
                        {# DOLATER: Selectors don't make sense, less than unassigned? Greater than RED? Need to refine. #}
                        <div class="row vsc-padded">
                            <div class="col-md-3">
                                <h4>Current alert level is:</h4>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{ select(['hzd_qual', hzd_qual_Options, 'useEmpty': true, 'emptyText': 'Choose..', 'emptyValue': '', "class": "form-control"]) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ select(['alert_level', alert_level_Options, 'useEmpty': true, 'emptyText': 'Any', 'emptyValue': '', "class": "form-control"]) }}
                                </div>
                            </div>
                        </div>

                        {{ submit_button("Save Changes", "class": "btn btn-primary btn-sm pull-right", "id": "volprefSubmit") }}
                        <br/>
                    {{ close('form') }}
                </div>
            </div>

        </div>
    </div>

    <script>
        var formmodified = 0;

        window.onbeforeunload = confirmExit;
        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }

        $("#volprefSubmit").prop('disabled', true);
        var formChanged = false;

        $("#volprefSubmit").click(function () {
            formmodified = 0;
        });

        $("#profile_FORM").change(function () {
            if (!formChanged) {
                formmodified = 1;
                $("#volprefSubmit").prop('disabled', false);
            }
            ;
        });
    </script>

{% endblock %}