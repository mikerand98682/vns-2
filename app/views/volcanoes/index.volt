{% extends "templates/base_vis.volt" %}

{% block title %}My Volcanoes{% endblock %}

{% block content %}

<div class="page-header"><h1>My Volcanoes</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div id="vmap_DIV" style='display:none'></div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">My Observatories and Volcanoes</h3>
            </div>
            <div class="panel-body">

                {% set hasSingle = false %}
                {% set someHidden = false %}

                {% for volcpref in volcprefs %}
                    {% set hasObs = false %}
                    {% for volcprefAll in volcprefsAll %}
                        {% if volcprefAll.volcano_cd == "all_" ~ volcpref.obs_abbr %}
                            {% set hasObs = true %}
                            {% set someHidden = true %}
                        {% endif %}
                    {% endfor %}
                    {% if not hasObs %}{% set hasSingle = true %}{% endif %}
                {% endfor %}

                {% if volcprefsAll|length > 0 %}
                    <h4>Selected Observatories</h4>
                    {% for volcprefAll in volcprefsAll %}
                        <div class="row vsc-padded">
                            <div class="col-md-9">{{ volcprefAll.vpref }}</div>
                            <div class="col-md-3">
                                {{ link_to("volcanoes/editpref/" ~ volcprefAll.volcano_cd, "Edit") }} | 
                                {{ link_to("volcanoes/delete/" ~ volcprefAll.volcano_cd, "Remove", 'class':'vns-confirm','title':'Remove Observatory','msg':'Remove observatory?') }}
                            </div>
                        </div>
                    {% endfor %}
                {% endif %}

                {% if volcprefsAll|length > 0 and hasSingle %}<hr/>{% endif %}

                {% if hasSingle %}

                    <h4>Selected Volcanoes</h4>
                    <div class="row vsc-padded">
                        <div class="col-md-3"><h4>Volcano</h4></div>
                        <div class="col-md-2"><h4>Region</h4></div>
                        <div class="col-md-4"><h4>Volcano Observatory</h4></div>
                        <div class="col-md-3"></div>
                    </div>

                    {% for volcpref in volcprefs %}

                        {% set hasObs = false %}
                        {% for volcprefAll in volcprefsAll %}
                            {% if volcprefAll.volcano_cd == "all_" ~ volcpref.obs_abbr %}{% set hasObs = true %}{% endif %}
                        {% endfor %}

                        {% if not hasObs %}
                            <div class="row vsc-padded">
                                <div class="col-md-3">{{ volcpref.vpref }}</div>
                                <div class="col-md-2">{{ volcpref.region }}</div>
                                <div class="col-md-4">{{ volcpref.obs_fullname }}</div>
                                <div class="col-md-3">
                                    {{ link_to("volcanoes/editpref/" ~ volcpref.volcano_cd, "Edit") }} | 
                                    {{ link_to("volcanoes/delete/" ~ volcpref.volcano_cd, "Remove", 'class':'vns-confirm','title':'Remove Volcano','msg':'Remove volcano?') }}
                                </div>
                            </div>
                        {% endif %}

                    {% endfor %}

                {% endif %}

                {% if volcprefsAll|length == 0 and volcprefs|length == 0 %}
                    <b><i>No selected observatories or volcanoes, select below.</i></b>
                {% else %}
                    <hr/>{{ link_to("volcanoes/deleteAll", "Remove All Observatory and Volcano Selections", 'class':'vns-confirm','title':'Remove All Selections','msg':'Remove all observatory and volcano selections? You will not receive notices without any selections.') }}
                    {% if someHidden %}
                        <br/>
                        <i>Some selected volcanoes are hidden because they belong to an included observatory, but will be removed if remove all is clicked.</i>
                    {% endif %}
                {% endif %}

            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Select Observatories and Volcanoes</h3>
            </div>
            <div class="panel-body">

                <h4>Available Volcano Observatories</h4>
                {% for obs in observatories %}
                    {% set hasAllObs = false %}
                    {% for volcprefAll in volcprefsAll %}
                        {% if "all_" ~ obs.obs_abbr == volcprefAll.volcano_cd %}{% set hasAllObs = true %}{% endif %}
                    {% endfor %}
                    {% if not hasAllObs %}
                        <div class="row vsc-padded">
                            <div class="col-md-6">
                                {{ obs.obs_fullname }}
                            </div>
                            <div class="col-md-6">
                                {{ link_to("volcanoes/addobservatory/" ~ obs.obs_abbr, "Add All Volcanoes") }}
                            </div>
                        </div>
                    {% endif %}
                {% endfor %}

                <hr/>

                <h4>Available Volcanoes</h4>
                {{ form(["action": config.application.baseUrl ~ "volcanoes/add", "role": "form", "id": "volcano_select_FORM", "class": "form form-inline"]) }}
                    <div class="form-group">
                        <select name="volcano_cd" class="form-control">
                            <option value=''></option>
                            {% for volcano in volcanoes %}
                                {% set hasAllObs = false %}
                                {% for volcprefAll in volcprefsAll %}
                                    {% if "all_" ~ volcano.observatory.obs_abbr == volcprefAll.volcano_cd %}{% set hasAllObs = true %}{% endif %}
                                {% endfor %}
                                {% set hasVolc = false %}
                                {% for volcpref in volcprefs %}
                                    {% if volcano.volcano_cd == volcpref.volcano_cd %}{% set hasVolc = true %}{% endif %}
                                {% endfor %}
                                {% if !hasAllObs and !hasVolc %}
                                    <option value="{{ volcano.volcano_cd }}">{{ volcano.volcano_name }} ({{ volcano.region }})</option>
                                {% endif %}
                            {% endfor %}            
                        </select>
                    </div>
                    {{ submit_button("Submit", "class": "btn btn-primary btn-sm") }}
                    <br/>
                {{ close('form') }}

                <hr/>

                <h4>Available Regions</h4>

                <div class="row vsc-padded">
                    <div class="col-md-3"><h4>Region</h4></div>
                    <div class="col-md-4"><h4>Observatory</h4></div>
                    <div class="col-md-2"><h4>Selected</h4></div>
                    <div class="col-md-3"><h4></h4></div>
                </div>
                {% for region in regions %}
                    {% set hasAllObs = false %}
                    {% set myTot = 0 %}
                    {% for volcprefAll in volcprefsAll %}
                        {% if "all_" ~ region.obs_abbr == volcprefAll.volcano_cd %}{% set hasAllObs = true %}{% endif %}
                    {% endfor %}
                    {% for volcpref in volcprefs %}
                        {% if volcpref.region == region.region %}{% set myTot = myTot + 1 %}{% endif %}
                    {% endfor %}
                    <div class="row vsc-padded">
                        <div class="col-md-3">{{ region.region }}</div>
                        <div class="col-md-4">{{ region.obs_fullname }}</div>
                        <div class="col-md-2">
                            {% if hasAllObs %}
                                {{ region.vtot ~ " of " ~ region.vtot }}
                            {% else %}
                                {{ myTot ~ " of " ~ region.vtot }}
                            {% endif %}
                        </div>
                        <div class="col-md-3">
                            {% if hasAllObs %}
                                Included with observatory
                            {% elseif myTot == region.vtot %}
                                All selected
                            {% elseif myTot > 0 %}
                                {{ link_to("volcanoes/addregion/" ~ region.region, "Add Remaining") }} | 
                                {{ link_to("volcanoes/region/" ~ region.region, "Choose") }}
                            {% else %}
                                {{ link_to("volcanoes/addregion/" ~ region.region, "Add All") }} | 
                                {{ link_to("volcanoes/region/" ~ region.region, "Choose") }}
                            {% endif %}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
    </div>
</div>

<!-- Volcano Map components. -->
<script type="text/javascript">
    (function (vnsVars) {
        vnsVars.vhpStatusPath = "{{ config.application.vhpStatusPath }}";
    }(window.vnsVars = window.vnsVars || {}));
</script>

{{ javascript_include('js/jquery-3.7.1.min.js') }}
{{ javascript_include('js/popper.min.js') }}
{{ javascript_include('js/bootstrap.min.js') }}
    
<link rel="stylesheet" href="https://js.arcgis.com/3.14/esri/css/esri.css">
<script type="application/json" id="volcprefs-json">{{ prefsData }}</script>
<script type="application/json" id="volcprefs-all-json">{{ allPrefsData }}</script>
<script src="https://js.arcgis.com/3.14/"></script>
{{ javascript_include("js//vns-volcanomap.js") }}
<!-- Volcano Map components - DONE -->

{% endblock %}