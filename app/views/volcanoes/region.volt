{% extends "templates/base_vis.volt" %}

{% block title %}My Volcanoes - {{ region }}{% endblock %}

{% block content %}

<div class="page-header"><h1>My Volcanoes - {{ region }}</h1></div>

<div class="row vsc-padded">

	<div class="col-md-3">
		{{ left_menu }}
	</div>

	<div class="col-md-9">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">{{ region }}</h3>
			</div>
			<div class="panel-body">

				{% set hasAllObs = false %}
				{% for volcpref in volcprefs %}
					{% for volcano in volcanoes %}
						{% if "all_" ~ volcano.observatory.obs_abbr == volcpref.volcano_cd %}{% set hasAllObs = true %}{% endif %}
					{% endfor %}
				{% endfor %}

				{% set allSelected = true %}

				{% for volcano in volcanoes %}

					{% set hasVolc = false %}
					{% for volcpref in volcprefs %}
						{% if volcano.volcano_cd == volcpref.volcano_cd %}{% set hasVolc = true %}{% endif %}
					{% endfor %}

					<div class="row vsc-padded">
						<div class="col-md-4">{{ volcano.volcano_name }}</div>
						<div class="col-md-8">
							{% if hasAllObs %}
								Included with observatory
							{% elseif hasVolc %}
								{{ link_to("volcanoes/delete/" ~ volcano.volcano_cd, "Included - Remove") }}
							{% else %}
								{% set allSelected = false %}
								{{ link_to("volcanoes/add/" ~ volcano.volcano_cd, "Select") }}
							{% endif %}
						</div>
					</div>
				
				{% endfor %}
			</div>
		</div>

	</div>
</div>

{% endblock %}