{% extends "templates/base_vis.volt" %}

{% block title %}Home{% endblock %}

{% block content %}

<h2>Volcano Notification Service (VNS)</h2>

<div class="row vsc-padded">

    <div class="col-md-4">

        {% if config.application.mode == "maintenance" %}

            <b class="text-danger">{{ config.application.maintMsg }}</b>

        {% elseif !isReplicationCurrent %}

            <b class="text-danger">
                VNS is currently unavailable. Problems are usually resolved quickly. Please try again in a few hours.
            </b>
            <br/><br/><br/>
            <small>Cause: R.D.</small>

        {% else %}

            {% if session.get('userid') == null %}

                <h3>{{ link_to("subscribe", "Subscribe to VNS") }}</h3>
                <i>{{ link_to("subscribe", "Click here to subscribe to VNS") }}</i>
                <br/><hr/>
                <b class="red">This version of VNS will be retired soon.</b> 
                <br/>
                <a href="/vns3">Visit the new VNS here</a> if you would 
                like to set up an account. We will migrate remaining users before this version is retired.
                <hr/>
                <b>Manage My Account</b>
                {{ form(["action": config.application.baseUrl ~ "index/login", "role": "form", "id": "login_FORM", "class": "form-inline"]) }}
                    {{ text_field("username", "size": 20, "maxlength": 60, "placeholder" : "Username", "aria-label" : "Username")}}

                    {{ hidden_field("token", "value": session.token) }}

                    {{ password_field("password", "size": 20, "maxlength": 60, "placeholder" : "Password", "aria-label" : "Password")}}

                    <br/><br/>
                    {{ submit_button("Login", "class": "btn btn-primary btn-sm", "aria-label" : "Login Submit Button") }}
                    <br/>
                {{ close('form') }}

                <br/> 
                <b>{{ link_to("subscribe/recover", "Recover account (lost username or password).") }}</b>
                
            {% else %} 

                {{ link_to("summary", "Manage My Account")}}<br/>
                {{ link_to("index/logout", "Logout") }}

            {% endif %}

        {% endif %}

    </div>

    <div class="col-md-8 well">

        <p>
            The Volcano Notification Service (VNS) is a free service that sends you notification emails about volcanic activity happening at US monitored volcanoes.
            You can customize the VNS to only deliver notifications for certain volcanoes, or a range of volcanoes, as well as choose the separate notification types you want to receive.
        </p>

        <p>
            Notifications are issued by these U.S. Volcano Observatories: Alaska (AVO), California (CalVO), Cascades (CVO), Hawaiian (HVO),
            Northern Marianas Islands (NMI) and Yellowstone (YVO).
        </p>

    </div>
</div>

{% endblock %}