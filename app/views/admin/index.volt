{% extends "templates/base_vis.volt" %}

{% block title %}Admin{% endblock %}

{% block content %}

    <div class="page-header"><h1>Admin</h1></div>

    <div class="row vsc-padded">

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin</h3>
                </div>
                <div class="panel-body">

                    <h4>Users</h4>

                    {{ link_to("admin/search", "Search for users") }}

                    <br/>

                    <h4>JSON</h4>
                    {{ link_to("api/makeJson", "Make/Refresh JSON Files")}} - creates 
                    {{ link_to("resources/json/observatories.json", "observatories.json") }}, 
                    {{ link_to("resources/json/regions.json", "regions.json") }} and 
                    {{ link_to("resources/json/volcanoes.json", "volcanoes.json") }} 
                    for use with JavaScript functions.

                    <h4>Daily Report</h4>
                    {{ link_to("senddaily", "Send daily report to users that have not received it in the last 23 hours. AUTOMATED") }}

                    <h4>Reminders</h4>
                    {{ link_to("sendreminders", "Send account reminders to users wishing to receive them. AUTOMATED") }}

                    <h4>Message All Users</h4>
                    {{ link_to("sendtoall", "Send a message to all active recipient email addresses.") }}
                    
                </div>
            </div>

        </div>
    </div>

{% endblock %}