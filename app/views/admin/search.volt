{% extends "templates/base_vis.volt" %}

{% block title %}Search{% endblock %}

{% block content %}

<div class="page-header"><h1>Admin - Search</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Search Text</h3>
            </div>
            <div class="panel-body">

                {{ form(["action": config.application.baseUrl ~ "admin/search", "role": "form", "id": "search_FORM", "class": "form"]) }}
                    <div class="form-group">
                        <label for="search" class="control-label">Search</label>
                        {{ text_field("search", "size": 40, "maxlength": 60, "class": "form-control", "placeholder" : "Username, name or email address")}}
                    </div>
                    {{ submit_button("Search for Users", "class": "btn btn-primary btn-sm pull-right") }}
                    <br/>
                {{ close('form') }}

                <br/>

                {% set lastUserId = "" %}

                {% if results is defined and results %}

                    <h4>Search Results</h4>
                    <div class="row vsc-padded">
                        <div class="col-md-3"><h4>Username</h4></div>
                        <div class="col-md-3"><h4>Name</h4></div>
                        <div class="col-md-3"><h4>Email</h4></div>
                        <div class="col-md-3"></div>
                    </div>

                    {% for result in results %}

                        {% set mailStatus = "" %}
                        {% if result.active == "n" %}{% set mailStatus = mailStatus ~ " <b style='color:red'>INACTIVE</b> " %}{% endif %}
                        {% if result.confirmed == "n" %}{% set mailStatus = mailStatus ~ " <b style='color:red'>UNCONFIRMED</b> " %}{% endif %}

                        {% if lastUserId != result.user_id %}

                            {% if lastUserId != "" %}<hr/>{% endif %}

                            {% set lastUserId = result.user_id %}

                            <div class="row vsc-padded">
                                <div class="col-md-3">{{ result.username }}</div>
                                <div class="col-md-3">{{ result.user_fullname }}</div>
                                <div class="col-md-4">{{ result.email }} {{ mailStatus }}</div>
                                <div class="col-md-2">{{ link_to("admin/user/" ~ result.user_id, "Administer User")}}</div>
                            </div>

                        {% else %}

                            <div class="row vsc-padded">
                                <div class="col-md-3"></div>
                                <div class="col-md-3"></div>
                                <div class="col-md-4">{{ result.email }} {{ mailStatus }}</div>
                                <div class="col-md-2"></div>
                            </div>

                        {% endif %}

                    {% endfor %}

                {% else %}

                    <h4>Search did not return any results.</h4>

                {% endif %}
            </div>
        </div>


    </div>
</div>

{% endblock %}