{% extends "templates/base_vis.volt" %}

{% block title %}Admin - Stop Address{% endblock %}

{% block content %}

<div class="page-header"><h1>Admin - Stop Address</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Stop Address</h3>
            </div>
            <div class="panel-body">

                Enter email address below. Upon submit a final message will be sent to this user, notifying them that 
                this address will stop receiving email from VNS.
                <br/><br/>
                This functionality provides an easy way to disable an address when we receive complaints.
                <br/><br/>

                {{ form(["action": config.application.baseUrl ~ "admin/stopaddress", "role": "form", "id": "stopaddress_FORM", "class": "form"]) }}

                    <div class="form-group">
                        <label for="stopaddress" class="control-label">Address To Stop</label>
                        {{ text_field("stopaddress", "size": 30, "maxlength": 60, "class": "form-control", "placeholder" : "Email address")}}
                    </div>
                    <b>{{ msg }}</b><br/>
                    {{ submit_button("Stop After Final Message", "class": "btn btn-primary btn-sm pull-right") }}
                    <br/>
                {{ close('form') }}
                <br/><br/>
                Use the {{ link_to("admin/search", "Search for users") }} page to find and further edit a user account 
                or re-inable an address.

            </div>
        </div>

    </div>
</div>

{% endblock %}