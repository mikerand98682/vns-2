{% extends "templates/base_vis.volt" %}

{% block title %}Admin - User{% endblock %}

{% block content %}

    <div class="page-header"><h1>Admin - User Administration</h1></div>

    <div class="row vsc-padded">

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">User Administration</h3>
                </div>
                <div class="panel-body">

                    <h4>User Information</h4>

                    <div class="row vsc-padded">
                        <div class="col-md-2"><b>Username:</b></div>
                        <div class="col-md-10">{{ user.username }}</div>
                    </div>

                    <div class="row vsc-padded">
                        <div class="col-md-2"><b>Name:</b></div>
                        <div class="col-md-10">{{ user.user_fullname }}</div>
                    </div>

                    <div class="row vsc-padded">
                        <div class="col-md-2"><b>Last Login:</b></div>
                        <div class="col-md-10">{{ user.lastlogin }}</div>
                    </div>

                    <div class="row vsc-padded">
                        <div class="col-md-2"><b>Added:</b></div>
                        <div class="col-md-10">{{ user.added }}</div>
                    </div>

                    <div class="row vsc-padded">
                        <div class="col-md-2"><b>Reminders:</b></div>
                        <div class="col-md-10">
                            {% if user.send_reminder == 'y' %}
                                Enabled
                            {% else %}
                                Disabled
                            {% endif %}
                        </div>
                    </div>

                    <br/>

                    {{ link_to("admin/deleteuser/" ~ user.user_id, "Delete This User") }}

                    <br/><br/>

                    {{ link_to("admin/masquerade/" ~ user.user_id, "Become this user (masquerade) to view and change their information.") }}

                    <br/><br/>

                    <h4>Change Password</h4>

                    {{ form(["action": config.application.baseUrl ~ "admin/password", "role": "form", "id": "password_FORM", "class": "form"]) }}

                        {{ hidden_field("user_id")}}

                        <div class="form-group">
                            <label for="password" class="control-label">New Password</label>
                            {{ password_field("password", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "New Password")}}
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">Verify</label>
                            {{ password_field("verify", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "Verify")}}
                        </div>

                        {{ submit_button("Change Password", "class": "btn btn-primary btn-sm pull-right") }}
                        <br/>
                    {{ close('form') }}

                    <br/>

                    <h4>Email Addresses</h4>
                    <div class="row vsc-padded">
                        <div class="col-md-4"><h4>Address</h4></div>
                        <div class="col-md-2"><h4>Type</h4></div>
                        <div class="col-md-2"><h4>Active?</h4></div>
                        <div class="col-md-2"><h4>Confirmed?</h4></div>
                        <div class="col-md-2"></div>
                    </div>

                    {% for address in addresses %}

                        <div class="row vsc-padded">
                            <div class="col-md-4">{{ address['email'] }}</div>
                            <div class="col-md-2">{{ address['email_format'] }}</div>
                            <div class="col-md-2">
                                {% if  address['active'] == "y" %}
                                    Active<br/>
                                    {{ link_to("admin/deactivate/" ~ address['address_id'], "Deactivate") }}<hr/>
                                    {{ link_to("admin/stopaddress/" ~ address['address_id'], "Deactivate & Email User") }}
                                {% else %}
                                    <b style='color:red'>INACTIVE</b>
                                    <br/>{{ address['deactivated_date'] }}
                                    {% if address['deactivatedMsg'] is defined %}
                                        {{ address['deactivatedMsg'] }}
                                    {% endif %}
                                    <br/>{{ link_to("admin/activate/" ~ address['address_id'], "Activate") }}

                                {% endif %}
                            </div>
                            <div class="col-md-2">
                                {% if address['confirmed'] == "y" %}
                                    Confirmed<br/>{{ link_to("admin/unconfirm/" ~ address['address_id'], "Unconfirm") }}
                                {% else %}
                                    <b style='color:red'>UNCONFIRMED</b><br/>Conf. Cd: {{ address['confirmation_code'] }}<br/>
                                    {{ link_to("admin/confirm/" ~ address['address_id'], "Confirm") }}
                                {% endif %}
                            </div>
                            <div class="col-md-2">
                                {{ link_to("admin/deleteaddress/" ~ address['address_id'], "Delete Address") }}
                            </div>
                        </div>

                        {% if address['receive_daily_summary_ind'] == 'y' %} 
                            <div class="row vsc-padded">
                                <div class="col-md-1"></div>
                                <div class="col-md-11"><b>Receives daily summary report.</b></div>
                            </div>
                        {% endif %}

                        {% if address['deepdive']['user_id'] is defined %}

                            <div class="row vsc-padded">
                                <div class="col-md-1"></div>
                                <div class="col-md-11">

                                    <b>Notice Types:</b>
                                    <ul>
                                        {% for index, nt in address['deepdive']['notice_types'] %}
                                            <li>{{ nt['notice_type'] }}</li>
                                            {% endfor %}
                                    </ul>

                                    <b>Volcanoes:</b><br/>
                                    <ul>
                                        {% for index, volc in address['deepdive']['volcanoes'] %}
                                            <li>
                                                {{ volc['volcano_name'] }}
                                                {% if volc['qualifiers']['hzd_qual_text'] is defined %}
                                                    <small><i> | {{ volc['qualifiers']['hzd_qual_text'] }}</i></small>
                                                {% endif %}
                                                {% if volc['qualifiers']['aviat_qual_text'] is defined %}
                                                    <small><i> | {{ volc['qualifiers']['aviat_qual_text'] }}</i></small>
                                                {% endif %}
                                            </li>
                                        {% endfor %}
                                    </ul>

                                    <b>Recent Mail History:</b>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Sent Date</th>
                                                <th>subject</th>
                                                <th>Error</th>
                                                <th>Sent</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {% for index, mail in address['deepdive']['sent_mail_history'] %}
                                                <tr>
                                                    <td>{{ mail['sent_dttm'] }}</td>
                                                    <td>{{ mail['subject'] }}</td>
                                                    <td>{{ mail['error_ind'] }}</td>
                                                    <td>{{ mail['sent_ind'] }}</td>
                                                </tr>
                                            {% endfor %}
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        {%  endif %}

                        <hr/>
                    {% endfor %}

                </div>
            </div>

        </div>
    </div>

{% endblock %}