{% extends "templates/base_vis.volt" %}

{% block title %}Unsubscribe{% endblock %}

{% block content %}

<div class="page-header"><h1>Unsubscribe</h1></div>

<div class="row vsc-padded">

    {% if config.application.mode == "maintenance" %}
    
        <b class="text-danger">{{ config.application.maintMsg }}</b>

    {% elseif !isReplicationCurrent %}

        <b class="text-danger">
            VNS is currently unavailable. Problems are usually resolved quickly. Please try again in a few hours.
        </b>
        <br/><br/><br/>
        <small>Cause: R.D.</small>
            
    {% else %}
        
        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">
            <b>{{ unsubscribeMsg }}</b>
        </div>
    
    {% endif %}

</div>

{% endblock %}