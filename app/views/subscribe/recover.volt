{% extends "templates/base_vis.volt" %}

{% block title %}Recover Account{% endblock %}

{% block content %}

<div class="page-header"><h1>Recover Account</h1></div>

<div class="row vsc-padded">
    
    {% if config.application.mode == "maintenance" %}
    
        <b class="text-danger">{{ config.application.maintMsg }}</b>
    
    {% elseif !isReplicationCurrent %}

        <b class="text-danger">
            VNS is currently unavailable. Problems are usually resolved quickly. Please try again in a few hours.
        </b>
        <br/><br/><br/>
        <small>Cause: R.D.</small>
                
    {% else %}

        <div class="col-md-3">
            {{ left_menu }}
        </div>
        <div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Recover Account</h3>
                </div>
                <div class="panel-body">
                    {{ form(["action": config.application.baseUrl ~ "subscribe/recover", "role": "form", "id": "recover_FORM", "class": "form"]) }}
                        <div class="form-group">
                            <label for="email" class="control-label">Email Address</label>
                            {{ text_field("email", "size": 40, "maxlength": 60, "class": "form-control", "placeholder" : "Email Address")}}
                            {{ hidden_field("token", "value": session.token) }}
                        </div>
                        {{ submit_button("Recover Account", "class": "btn btn-primary btn-sm pull-right") }}
                        <br/>
                    {{ close('form') }}
                </div>
            </div>
        </div>
                
    {% endif %}
    
</div>

{% endblock %}