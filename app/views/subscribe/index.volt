{% extends "templates/base_vis.volt" %}

{% block title %}Subscribe{% endblock %}

{% block content %}

<div class="page-header"><h1>Subscribe</h1></div>

<div class="row vsc-padded">

    {% if config.application.mode == "maintenance" %}
    
        <b class="text-danger">{{ config.application.maintMsg }}</b>

    {% elseif !isReplicationCurrent %}

        <b class="text-danger">
            VNS is currently unavailable. Problems are usually resolved quickly. Please try again in a few hours.
        </b>            
        <br/><br/><br/>
        <small>Cause: R.D.</small>

    {% else %}

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            Current volcano updates are always available at:
            <b>
                <a href="https://www.usgs.gov/programs/VHP/volcano-updates">
                    https://www.usgs.gov/programs/VHP/volcano-updates</a>
            </b>
            <br/><br/>
            
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Subscribe</h3>
                </div>
                <div class="panel-body">

                    {{ form(["action": config.application.baseUrl ~ "subscribe", "role": "form", "id": "subscribe_FORM", "class": "form"]) }}

                        <div class="form-group">
                            <label for="user_fullname" class="control-label">Name</label>
                            {{ text_field("user_fullname", "size": 20, "class": "form-control", "placeholder" : "Full Name")}}
                        </div>

                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>
                            {{ text_field("username", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "Username")}}
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Email Address</label>
                            {{ text_field("email", "size": 40, "maxlength": 60, "class": "form-control", "placeholder" : "Email Address")}}
                        </div>

                        <div class="form-group">
                            <label for="affiliation_id" class="control-label">Affiliation</label>
                            {{ select(['affiliation_id', affiliations, 'using': ['affiliation_id', 'affiliation'], 'useEmpty': true, 'emptyText': 'Choose Affiliation...', 'emptyValue': '', "class": "form-control"]) }}
                        </div>

                        <div class="form-group">
                            <label for="send_reminder" class="control-label">Send Reminder</label>
                            <input type="checkbox" name="send_reminder" id="send_reminder" value="y"/>
                            <br/><i>Reminders are sent infrequently with a summary of your account settings.</i>
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">New Password</label>
                            {{ password_field("password", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "New Password")}}
                        </div>

                        <div class="form-group">
                            <label for="verify" class="control-label">Verify</label>
                            {{ password_field("verify", "size": 20, "maxlength": 60, "class": "form-control", "placeholder" : "Verify")}}
                        </div>

                        {{ submit_button("Subscribe", "class": "btn btn-primary btn-sm pull-right") }}
                        <br/>
                    {{ close('form') }}
                </div>
            </div>

        </div>
                
    {% endif %}
</div>

{% endblock %}