{% extends "templates/base_vis.volt" %}

{% block title %}Unsubscribe{% endblock %}

{% block content %}

    <div class="page-header"><h1>Unsubscribe</h1></div>

    <div class="row vsc-padded">

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Unsubscribe</h3>
                </div>
                <div class="panel-body">
                    {{ form(["action": config.application.baseUrl ~ "subscribe/deleteaccount", "role": "form", "id": "unsubscribe_FORM", "class": "form"]) }}
                        Unsubscribe from the Volcano Notification Service? All account information will be removed.
                        {{ submit_button("Unsubscribe", "class": "btn btn-primary btn-sm pull-right") }}
                        <br/>
                    {{ close('form') }}
                </div>
            </div>

        </div>
    </div>

{% endblock %}