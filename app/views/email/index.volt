{% extends "templates/base_vis.volt" %}

{% block title %}My Email Addresses{% endblock %}

{% block content %}

<div class="page-header"><h1>My Email Addresses</h1></div>

<div class="row vsc-padded">

    <div class="col-md-3">
        {{ left_menu }}
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary">

            <div class="panel-heading">
                <h3 class="panel-title">Register New Email Address</h3>
            </div>
            <div class="panel-body">

                {{ form(["action": config.application.baseUrl ~ "email/create", "role": "form", "id": "email_FORM", "class": "form row vsc-padded"]) }}

                    {{ hidden_field("address_id", "value": "0") }}

                    <div class="form-group col-md-4">
                        <label for="email" class="control-label">Address</label>
                        {{ text_field("email", "size": 40, "maxlength": 60, "class": "form-control", "placeholder" : "Email Address")}}
                    </div>

                    <div class="form-group col-md-3">
                        <label for="email_format" class="control-label">Message Format</label>
                        {{ select(["email_format", formatOptions, "class": "form-control"]) }}
                    </div>

                    <div class="form-group col-md-4">
                        <label for="receive_daily_summary_ind" class="control-label">Daily Report</label>
                        {{ select(["receive_daily_summary_ind", summaryOptions, "class": "form-control"]) }}
                    </div>

                    <div class="col-md-1" style="padding-top: 2em;">
                        {{ submit_button("Save", "class": "btn btn-primary btn-sm pull-right") }}
                    </div>

                {{ close('form') }}

            </div>

            <p class="vsc-padded">
                The daily report is a summary sent to each selected and active email address. It details currently 
                elevated volcanoes or is empty if none are elevated.
            </p>

        </div>

        {% set needsConfirmation = false %}
        {% for address in addresses %}
            {% if address.confirmed == "n" %}{% set needsConfirmation = true %}{% endif %}
        {% endfor %}
        {% if needsConfirmation %}
            {{ confirm_interface }}
        {% endif %}

        {% for address in invalidAddresses %}
            <div class="alert alert-danger" role="alert">
                Warning: Email address {{ address }} appears to be invalid.
            </div>
        {% endfor %}

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Current Addresses</h3>
            </div>
            <div class="panel-body">
                <div class="row vsc-padded">
                    <div class="col-md-3"><h4>Address</h4></div>
                    <div class="col-md-2"><h4>Daily Report</h4></div>
                    <div class="col-md-1"><h4>Status</h4></div>
                    <div class="col-md-2"><h4>Format</h4></div>
                    <div class="col-md-4"></div>
                </div>
                {% for address in addresses %}
                    {% if address.confirmed != "y" %}{% continue %}{% endif %}
                    <div class="row vsc-padded">
                        <div class="col-md-3">{{ address.email }}</div>
                        <div class="col-md-2">
                            {% if address.receive_daily_summary_ind == "y" %}
                                {% if address.active == "y" %}
                                    Receive
                                {% else %}
                                    <del>Receive</del>
                                {% endif %}
                            {% endif %}
                        </div>
                        <div class="col-md-1">
                            {% if address.active == "y" %}
                                Active
                            {% else %}
                                <b style="color:red">Not Active</b>
                            {% endif %}
                        </div>
                        <div class="col-md-2">{{ address.email_format }}</div>
                        <div class="col-md-4">
                            {{ link_to("email/edit/" ~ address.address_id, "Edit Address") }} | 
                            {{ link_to("email/remove/" ~ address.address_id, "Remove", 'class':'vns-confirm','title':'Remove Address','msg':'Remove email address?') }}
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>

    </div>
</div>

{% endblock %}