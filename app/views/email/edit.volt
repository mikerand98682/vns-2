{% extends "templates/base_vis.volt" %}

{% block title %}My Email Addresses - Edit{% endblock %}

{% block content %}

    <div class="page-header"><h1>My Email Addresses - Edit</h1></div>

    <div class="row vsc-padded">

        <div class="col-md-3">
            {{ left_menu }}
        </div>

        <div class="col-md-9">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Email Address</h3>
                </div>
                <div class="panel-body">

                    {{ form(["action": config.application.baseUrl ~ "email/edit", "role": "form", "id": "email_FORM", "class": "form"]) }}

                        {{ hidden_field("address_id") }}

                        <div class="form-group">
                            <label for="email" class="control-label">Email Address</label>
                            {{ text_field("email", "size": 40, "maxlength": 60, "class": "form-control", 
                                            "placeholder" : "Email Address")}}
                        </div>

                        <div class="form-group">
                            <label for="active" class="control-label">Active</label>
                            {{ select(["active", activeOptions, "class": "form-control"]) }}
                            {% if maRec.deactivatedMsg is defined %}
                                <small>{{ maRec.deactivatedMsg }}</small>
                            {% endif %}
                        </div>

                        <div class="form-group">
                            <label for="email_format" class="control-label">Message Format</label>
                            {{ select(["email_format", formatOptions, "class": "form-control"]) }}
                        </div>

                        <div class="form-group">
                            <label for="receive_daily_summary_ind" class="control-label">Daily Summary Report</label>
                            {{ select(["receive_daily_summary_ind", summaryOptions, "class": "form-control"]) }}
                        </div>

                        {{ submit_button("Update", "class": "btn btn-primary btn-sm pull-right", "id": "emailSubmit") }}
                        <br/>
                    {{ close('form') }}

                </div>
            </div>

        </div>
    </div>

{% endblock %}