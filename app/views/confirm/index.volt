{% extends "templates/base_vis.volt" %}

{% block title %}Confirm Email Address{% endblock %}

{% block content %}

<h2>Volcano Notification Service (VNS)</h2>

{% if config.application.mode == "maintenance" %}

    <br/><br/>
    <b class="text-danger">{{ config.application.maintMsg }}</b>

{% elseif !isReplicationCurrent %}

    <br/><br/>
    <b class="text-danger">
        VNS is currently unavailable. Problems are usually resolved quickly. Please try again in a few hours.
    </b>
    <br/><br/><br/>
    <small>Cause: R.D.</small>

{% else %}

    <div class="row vsc-padded">

        <div class="col-md-4">

            {% if session.get('userid') == null %}

                <b>{{ link_to("subscribe", "Subscribe to VNS") }}</b>
                <br/><br/>
                <b>Manage My Account</b>
                {{ form(["action": config.application.baseUrl ~ "index/login", "role": "form", "id": "login_FORM", "class": "form-inline"]) }}
                    {{ text_field("username", "size": 20, "maxlength": 60, "placeholder" : "Username")}}
                    {{ hidden_field("token", "value": session.token) }}
                    {{ password_field("password", "size": 20, "maxlength": 60, "placeholder" : "Password")}}
                    <br/><br/>
                    {{ submit_button("Login", "class": "btn btn-primary btn-sm pull-right") }}
                    <br/>
                {{ close('form') }}

            {% else %}

                {{ link_to("summary", "Manage My Account")}}<br/>
                {{ link_to("index/logout", "Logout") }}

            {% endif %}

            <br/>
            {{ link_to("subscribe/recover", "Recover account (lost username or password).") }}

        </div>

        <div class="col-md-8 well">

            <h4>{{ confirmationStatus }}</h4>
            {% if confirmed %}
                <br/>
                Congratulations! Your email address is active, your account is set up and you should start receiving 
                notices. 
                <br/><br/>
                Typical VNS accounts receive email from all volcano observatories when they issue a status report, 
                information statement or event response. These types of notices are generally issued during volcanic 
                unrest.
                <br/><br/>
                You may review your account settings after logging in. Daily, weekly and monthly updates are also 
                available.
            {% endif %}
        </div>
    </div>

{% endif %}
    
{% endblock %}