<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Addresses Waiting To Be Confirmed</h3>
    </div>
    <div class="panel-body">
        {% for address in addresses %}
            {% if address.confirmed == "y" %}{% continue %}{% endif %}
            <div class="row vsc-padded">
                <div class="col-md-5">
                    <div class="row vsc-padded">
                        <div class="col-md-6"><b>Address:</b></div>
                        <div class="col-md-6">{{ address.email }}</div>
                    </div>
                    <div class="row vsc-padded">
                        <div class="col-md-6"><b>Format:</b></div>
                        <div class="col-md-6">{{ address.email_format }}</div>
                    </div>
                    <div class="row vsc-padded">
                        <div class="col-md-6"><b>Daily Report:</b></div>
                        <div class="col-md-6">
                            {% if address.receive_daily_summary_ind == "y" %}
                                Receive when confirmed
                            {% else %}
                                Do Not Receive
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    {{ form(["action": config.application.baseUrl ~ "email/confirm", "role": "form", "id": "confirm_" ~ address.address_id ~ "_FORM", "class": "form-inline confirm-form"]) }}
                        {{ hidden_field("address_id", "id":"address_id" ~ address.address_id, "value": address.address_id)}}
                        <div class="form-group">
                            <label for="code" class="control-label">Confirmation Code</label>
                            {{ text_field("confirmation_code", "id": "confirmation_code" ~ address.address_id, "address_id": address.address_id, "size": 20, "class": "form-control", "placeholder" : "Confirmation Code")}}
                        </div>
                        {{ submit_button("Confirm", "class": "btn btn-primary btn-sm") }}
                        <br/>
                    {{ close('form') }}
                    <hr/>
                    {{ link_to("email/resend/" ~ address.address_id, "Resend Confirmation") }} | 
                    {{ link_to("email/edit/" ~ address.address_id, "Edit Address") }} | 
                    {{ link_to("email/remove/" ~ address.address_id, "Remove Address", 'class':'vns-confirm','title':'Remove Address','msg':'Remove email address?') }}
                </div>
            </div>
            <hr/>
        {% endfor %}
    </div>
</div>