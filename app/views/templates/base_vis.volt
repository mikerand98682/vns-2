<!doctype html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>USGS Volcano Notification Service (VNS) | {% block title %}{% endblock %}</title>

    <!-- CSS -->
    {{ stylesheet_link('css/vis/font-awesome.min.css') }}
    {{ stylesheet_link('css/vis/common.css') }}
    {{ stylesheet_link('css/vis/custom.css') }}
    {{ stylesheet_link('css/vis/app.css') }}

    {{ stylesheet_link('css/bootstrap.min.css') }}
    {{ stylesheet_link('css/sticky-footer.css') }}
    {{ stylesheet_link('css/vns.css') }}

</head>
<body> <!-- opening div for body -->
    <div class="content">
        <!-- BEGIN USGS Applications Header Template -->
        <header id="navbar" class="header-nav"  role="banner">
            <div class="tmp-container">
                <!-- primary navigation bar -->
                <!-- search bar-->
                <div class="header-search">
                    <a class="logo-header" href="https://www.usgs.gov/" title="Home">
                        <img class="img"  src="{{ config.application.baseUrl }}images/logo.png"  alt="Home" />
                    </a>
                    <form action="https://www.usgs.gov/science-explorer-results" method="GET" id="search-box">
                        <div class="fa-wrapper"><label for="se_search" class="only">Search USGS</label>
                            <input id="se_search" type="search" name="es" placeholder="Search USGS">
                            <button class="fa fa-search" type="submit">
                                <span class="only">Search</span>
                            </button></div>
                    </form>
                </div>
                <!-- end search bar-->
            </div> 
            <!-- end header-container-->
        </header>
        <!-- END USGS Applications Header Template -->
        <div id="maincontent" class="main-container"> <!-- opening div for main content -->

            <noscript>
            <br/><br/>
            Unfortunately, this application relies on JavaScript.
            <br/><br/>
            </noscript>
            <!-- -------------------------------------------------------------------------------------------------------
              -- Pause USGS Template
              ------------------------------------------------------------------------------------------------------ -->

            {% set controller = router.getControllerName() %}
            {% set action = router.getActionName() %}

            {% if config.application.mode != "production" %}
                {% if config.application.mode == "maintenance" %}
                    <div>
                        <br/>
                        <i><b>VNS is currently unavailable, please try again in a few hours.</b></i>
                    </div>
                {%else%}
                    <div>
                        <br/>
                        <i><b>VNS set to {{ config.application.mode }} mode. Email sent to {{ config.mail.devemail }} 
                                instead of recipients.</b></i>
                    </div>
                {% endif %}
            {% endif %}

            <div>
                {{ flash.output() }}
                {% block content %}{% endblock %}
                <br/><br/>
                <b>For help with VNS please email <a href="mailto:vnshelp@usgs.gov">vnshelp@usgs.gov</a>.</b>
                <br/><br/>
                <b>For answers to questions regarding content sent in Volcano Notifications, please contact the 
                    approprate volcano observatory:</b>
                <br/><br/>
                <ul>
                    <li><a href="https://avo.alaska.edu/contact">Alaska Volcano Observatory</a></li>
                    <li>
                        <a href="https://www.usgs.gov/observatories/calvo/connect">California Volcano Observatory</a>
                    </li>
                    <li><a href="https://www.usgs.gov/observatories/cvo/connect">Cascades Volcano Observatory</a></li>
                    <li><a href="https://www.usgs.gov/observatories/hvo/connect">Hawaiian Volcano Observatory</a></li>
                    <li>
                        <a href="https://www.usgs.gov/observatories/yvo/connect">Yellowstone Volcano Observatory</a>
                    </li>
                </ul>
                
                <br/><br/>
            </div>

            <!-- VNS Confirmation Modal -->
      
            <div class="modal" id="vnsModal" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modal title</h5>
                            <button type="button" class="btn-close modal-close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Modal body text goes here.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="vnsModalCancelBtn" class="btn btn-secondary modal-close" data-dismiss="modal">
                                Close</button>
                            <button type="button" id="vnsModalConfirmBtn" class="btn btn-primary">OK</button>
                        </div>
                    </div>
                </div>
            </div>

        <!-- -----------------------------------------------------------------------------------------------------------
          -- Resume USGS Template
          ---------------------------------------------------------------------------------------------------------- -->
        </div> <!-- / div for your main content-->

    </div><!-- / div for all content except footer -->
    <!-- BEGIN USGS Footer Template -->

    <footer class="footer">
        <div class="tmp-container">
            <!-- .footer-wrap -->
            <!-- .footer-doi -->
            <div class="footer-doi">
                <!-- footer nav links -->
                <ul class="menu nav">
                    <li class="first leaf menu-links menu-level-1">
                        <a href="https://www.doi.gov/privacy">DOI Privacy Policy</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.usgs.gov/policies-and-notices">Legal</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.usgs.gov/accessibility-and-us-geological-survey">Accessibility</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.usgs.gov/sitemap">Site Map</a></li>
                    <li class="last leaf menu-links menu-level-1">
                        <a href="https://answers.usgs.gov/">Contact USGS</a></li>
                </ul>
                <!--/ footer nav links -->      
            </div>
            <!-- /.footer-doi -->

            <hr>

            <!-- .footer-utl-links -->
            <div class="footer-doi">
                <ul class="menu nav">
                    <li class="first leaf menu-links menu-level-1">
                        <a href="https://www.doi.gov/">U.S. Department of the Interior</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.doioig.gov/">DOI Inspector General</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.whitehouse.gov/">White House</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.whitehouse.gov/omb/management/egov/">E-gov</a></li>
                    <li class="leaf menu-links menu-level-1">
                        <a href="https://www.doi.gov/pmb/eeo/no-fear-act">No Fear Act</a></li>
                    <li class="last leaf menu-links menu-level-1">
                        <a href="https://www.usgs.gov/about/organization/science-support/foia">FOIA</a></li>
                </ul>
            </div>			
            <!-- /.footer-utl-links -->
            <!-- .footer-social-links -->
            <div class="footer-social-links">
                <ul class="social">
                    <li class="follow">Follow</li>
                    <li class="twitter">
                        <a href="https://twitter.com/usgs" target="_blank">
                            <i class="fa fa-twitter-square"><span class="only">Twitter</span></i>
                        </a>
                    </li>
                    <li class="facebook">
                        <a href="https://facebook.com/usgeologicalsurvey" target="_blank">
                            <i class="fa fa-facebook-square"><span class="only">Facebook</span></i>
                        </a>
                    </li>
                    <li class="github">
                        <a href="https://github.com/usgs" target="_blank">
                            <i class="fa fa-github"><span class="only">GitHub</span></i>
                        </a>
                    </li>
                    <li class="flickr">
                        <a href="https://flickr.com/usgeologicalsurvey" target="_blank">
                            <i class="fa fa-flickr"><span class="only">Flickr</span></i>
                        </a>
                    </li>
                    <li class="youtube">
                        <a href="http://youtube.com/usgs" target="_blank">
                            <i class="fa fa-youtube-play"><span class="only">YouTube</span></i>
                        </a>
                    </li>
                    <li class="instagram">
                        <a href="https://instagram.com/usgs" target="_blank">
                            <i class="fa fa-instagram"><span class="only">Instagram</span></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.footer-social-links -->
        </div>
        <!-- /.footer-wrap -->	
    </footer>
    <!-- END USGS Footer Template- -->
    
    {{ javascript_include('js/jquery-3.7.1.min.js') }}
    {{ javascript_include('js/jquery.cookie.js') }}
    {{ javascript_include('js/date.js') }}
    {{ javascript_include('js/popper.min.js') }}
    {{ javascript_include('js/bootstrap.min.js') }}
    <script>
        vnsJsConfig = {};
        vnsJsConfig.baseUrl = '{{ config.application.baseUrl }}';
    </script>
    {{ javascript_include('js/vns.js') }}
    
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKQR8KP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

</body> <!-- closing div for body -->
</html>