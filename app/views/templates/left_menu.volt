{{ link_to("/", "VNS Home") }}<br/>

{% if session.userid %}

    {% if rewriteUriMJR == "/summary" %}
        <b>Manage My Account</b><br/>
    {% else %}
        {{ link_to("/summary", "Manage My Account") }}<br/>
    {% endif %}

    {% if rewriteUriMJR == "/profile" %}
        <b>My Profile/Change Password</b><br/>
    {% else %}
        {{ link_to("/profile", "My Profile/Change Password") }}<br/>
    {% endif %}

    {% if rewriteUriMJR == "/email" %}
        <b>My Email Addresses</b><br/>
    {% else %}
        {{ link_to("/email", "My Email Addresses") }}<br/>
    {% endif %}

    {% if "/email/edit" in rewriteUriMJR %}
        <b>- Edit Address</b><br/>
    {% endif %}

    {% if rewriteUriMJR == "/notifications" %}
        <b>My Notification Types</b><br/>
    {% else %}
        {{ link_to("/notifications", "My Notification Types") }}<br/>
    {% endif %}

    {% if rewriteUriMJR == "/volcanoes" %}
        <b>My Volcanoes</b><br/>
    {% else %}
        {{ link_to("/volcanoes", "My Volcanoes") }}<br/>
    {% endif %}

    {% if "/volcanoes/region" in rewriteUriMJR %}
        <b>- Select Volcanoes By Region</b><br/>
    {% endif %}

    {% if "/volcanoes/editpref" in rewriteUriMJR %}
        <b>- Edit volcano preferences.</b><br/>
    {% endif %}

    <!-- Documentation<br/> -->

    {% if rewriteUriMJR == "/subscribe/unsubscribe" %}
        <b>Unsubscribe</b><br/>
    {% else %}
        {{ link_to("/subscribe/unsubscribe", "Unsubscribe") }}<br/>
    {% endif %}

    {{ link_to('index/logout', 'Logout ' ~ session.username) }}<br/>

{% else %}

    {% if rewriteUriMJR == "/subscribe" %}
        <b>Subscribe</b><br/>
    {% else %}
        {{ link_to("/subscribe", "Subscribe") }}<br/>
    {% endif %}

    {% if rewriteUriMJR == "/subscribe/recover" %}
        <b>Recover Account</b><br/>
    {% else %}
        {{ link_to("/subscribe/recover", "Recover Account") }}<br/>
    {% endif %}

{% endif %}

<hr/>
<b><a href="https://www.usgs.gov/programs/VHP/volcano-updates">Current Volcano Updates</a></b>
<br/><br/>
<a href="https://earthquake.usgs.gov/ens/">Go to Earthquake Notification Service</a><br/>

{# ADMINS ONLY, Add administrators by including user id's in appropriate config.ini line. #}
{% if (session.isAdmin) %}
    <hr/>
    {% if rewriteUriMJR == "/admin" %}
        <b>Admin</b><br/>
    {% else %}
        {{ link_to("/admin", "Admin") }}<br/>
    {% endif %}

    {% if "/admin/user" in rewriteUriMJR %}
        <b>- User Administration</b><br/>
    {% endif %}

    {% if rewriteUriMJR == "/admin/search" %}
        <b>Admin - Search</b><br/>
    {% else %}
        {{ link_to("/admin/search", "Admin - Search") }}<br/>
    {% endif %}

    <!--
    <br/>
    {{ rewriteUriMJR }}
    -->

{% endif %}
