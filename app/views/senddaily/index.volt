{% extends "templates/base_vis.volt" %}

{% block title %}Send Daily Report{% endblock %}

{% block content %}

<div class="page-header"><h1>Send Daily Report</h1></div>

<div class="well">
	Opening this page sends the daily report to any users that haven't received
	it in the last 23 hours. It should be called via cron/wget (or similar process)
	from an internal server.
</div>

<h4>Daily Report - HTML Format</h4>
<div class="well">{{ reportHtml }}</div>

<h4>Daily Report - Text Format</h4>
<div class="well"><pre>{{ reportText }}</pre></div>

<h4>Daily Report - Short (cell) Format</h4>
<div class="well"><pre>{{ reportCell }}</pre></div>

{% endblock %}