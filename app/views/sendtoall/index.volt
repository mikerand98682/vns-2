{% extends "templates/base_vis.volt" %}

{% block title %}Send Message To All Users{% endblock %}

{% block content %}

<div class="page-header"><h1>Send Message To All Users</h1></div>

{% if config.application.mode != "production" %}
    <div class="well">
        <i><b>VNS set to {{ config.application.mode }} mode.<br/>Recipient override addresses sent normally, mass email 
                only sent to {{ config.mail.devemail }} instead of recipients.</b></i>
    </div>
{% endif %}

<div class="well">

    This provides a way to send a message to all active VNS users. It is recommended to craft your message in a text
    editor then paste the message body here. If there are any problems with the submission, your message may be lost.

    <br/><br/>
    An initial run of this process for about 45,000 recipients was extremely slow - likely because user settings were
    included. Processing rates were about 100 messages per minute. To accommodate restarts, this process will not 
    send another message if the subject + recipient (email) is found in the mailman.mailman table.
    
    <br/><br/>
    Use this in conjunction with the log on the server, {{ logfile }}, that is where you will see progress when this 
    page times out. You can specify the starting batch if resuming a send process, or leave it empty. Starting another
    mailing could send duplicate messages to recipients.
    
    <br/><br/>

    {{ form(["action": config.application.baseUrl ~ "sendtoall", "role": "form", "id": "sendtoall_FORM", "class": "form"]) }}
            
        <div class="form-group">
            <label for="subject" class="control-label">Subject</label>
            {{ text_field("subject", "class": "form-control", "placeholder" : "Subject", "value": subject)}}
        </div>
    
        <div class="form-group">
            <label for="body" class="control-label">Email Body</label>
            {{ text_area("body", "rows": "10", "class": "form-control", "placeholder" : "Email Body", "value": body)}}
        </div>

        <div class="form-group">
            <label for="include-settings" class="control-label">User Settings:</label>
            <input type="checkbox" name="include-settings" id="include-settings" {{ includeSettings ? "checked" : "" }} />
            <br/><i>Include user settings below message body.</i>
        </div>
        
        <div class="form-group">
            <label for="age-filter" class="control-label">Do not send if user has received any notice in...</label>
            <select name="age-filter" id="age-filter">
                <option value="all" {{ ageFilterDays == "all" ? "selected" : "" }}>Nope - send to everyone.</option>
                <option value="7" {{ ageFilterDays == "7" ? "selected" : "" }}>7 Days</option>
                <option value="21" {{ ageFilterDays == "21" ? "selected" : "" }}>21 Days</option>
                <option value="31" {{ ageFilterDays == "31" ? "selected" : "" }}>31 Days</option>
                <option value="45" {{ ageFilterDays == "45" ? "selected" : "" }}>45 Days</option>
            </select>
            <br/><i>If a user has received a message from VNS recently, do not send them this message.</i>
        </div>   
            
        <div class="form-group">
            <label for="recipients" class="control-label">Recipient Override</label>
            {{ text_field("recipients", "class": "form-control", "placeholder" : "Email addresses")}}
            <i>Comma delimited list of recipients. Leave empty to send to all ({{ arwTotal }}) active recipients.</i>
        </div>
        
        <hr/>
        
        <div class="form-group">
            <label for="dry-run" class="control-label">Dry Run:</label>
            <input type="checkbox" name="dry-run" id="dry-run" {{ dryRun ? "checked" : "" }}/>
            <br/><i>Check this box to process all data but not send anything.</i>
        </div>
        
        <div class="form-group">
            <label for="startingbatch" class="control-label">Starting Batch:</label>
            <input type="number" name="starting-batch" id="starting-batch" value="{{ startingBatch }}"/>
            <br/>
            <i>
                If resuming a send process, you can specify the starting bach number after viewing the log file,
                {{ logfile }}, to see where the send process stopped off. The first batch is 0. If using the same
                subject, duplicate emails should not be sent. If using a starting batch > 0, is recommended to use a 
                value that will overlap processing before the previous process stopped (so if the process stopped at
                batch 321, use a starting batch of 320).
            </i>
        </div>
            
        <div class="form-group">
            <label for="confirm-full-send" class="control-label">Confirm Full Send:</label>
            <input type="checkbox" name="confirm-full-send" id="confirm-full-send" />
            <br/><i>Check this box to confirm that you want to send to {{ arwTotal }} active users (<b>if recipient 
                    override is empty and no recent notice restriction</b>).</i>
        </div>
        
        {{ submit_button("Send", "class": "btn btn-primary btn-sm") }}
        
        <br/>
        
    {{ close('form') }}
    
</div>

{% endblock %}