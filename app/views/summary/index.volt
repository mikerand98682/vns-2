{% extends "templates/base_vis.volt" %}

{% block title %}Manage My Account{% endblock %}

{% block content %}

<div class="page-header"><h1>Manage My Account</h1></div>

<div class="row vsc-padded">

	<div class="col-md-3">
		{{ left_menu }}
	</div>

	<div class="col-md-9">
       
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">My Profile</h3>
			</div>
			<div class="panel-body">
				<div class="row vsc-padded">
					<div class="col-md-4">Username:</div>
					<div class="col-md-8">{{ user.username }}</div>
				</div>
				<div class="row vsc-padded">
					<div class="col-md-4">Name:</div>
					<div class="col-md-8">{{ user.user_fullname }}</div>
				</div>
				<div class="row vsc-padded">
					<div class="col-md-4">Affiliation:</div>
					<div class="col-md-8">{{ user.mail_affiliation.affiliation }}</div>
				</div>
				<div class="row vsc-padded">
					<div class="col-md-4">Reminders:</div>
					<div class="col-md-8">{% if user.send_reminder == "y" %}Enabled{% else %}Disabled{% endif %}</div>
				</div>
				<hr/>
				{{ link_to("profile", "Change this information / Change password") }}
			</div>
		</div>

        {% set needsConfirmation = false %}{# Set to true if needed, when looping through addresses below. #}
		<div class="panel panel-primary">
            
			<div class="panel-heading">
				<h3 class="panel-title">My Email Addresses</h3>
			</div>
			<div class="panel-body">
				<div class="row vsc-padded">
					<div class="col-md-3"><h4>Address</h4></div>
					<div class="col-md-3"><h4>Daily Report</h4></div>
					<div class="col-md-3"><h4>Status</h4></div>
					<div class="col-md-3"><h4>Format</h4></div>
				</div>
				{% set dailyReport = "" %}
				{% for address in addresses %}
					<div class="row vsc-padded">
						<div class="col-md-3">{{ address.email }}</div>
						<div class="col-md-3">
							{% if address.receive_daily_summary_ind == "y" %}
								{% if dailyReport != "blocked" %}{% set dailyReport = "enabled" %}{% endif %}
								{% if address.confirmed != "y" or address.active != "y" %}
									{% set dailyReport = "blocked" %}
									<del>Receive</del>
								{% else %}
									Receive
								{% endif %}
							{% endif %}
						</div>
						<div class="col-md-3">
							{% if address.confirmed != "y" %}
                                {% set needsConfirmation = true %}
								<b style="color:red">Not Confirmed</b> | 
							{% endif %}
							{% if address.active == "y" %}
								Active
							{% else %}
								<b style="color:red">Not Active</b>
							{% endif %}
						</div>
						<div class="col-md-3">{{ address.email_format }}</div>
					</div>
				{% endfor %}
        
				<br/>
				<i>
					The daily report is a summary sent to each selected and active email address.
					It details currently elevated volcanoes or is empty if none are elevated.
					Click change this information below to enable.
				</i>

				<hr/>
				{{ link_to("/email", "Change this information") }}
			</div>
		</div>
            
        {% if needsConfirmation %}
            {{ confirm_interface }}
        {% endif %}
            
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">My Notification Types</h3>
			</div>
			<div class="panel-body">

				{% if dailyReport != "" %}
					<h4>Daily Report</h4>
					{% if dailyReport == "enabled" %}
						Receiving the daily elevated volcanoes report.
					{% elseif dailyReport == "blocked" %}
						One or more of your email addresses needs to be confirmed or activated before the daily report
						can be sent there.
					{% else %}
						Not sure what's going on, shouldn't be here.
					{% endif %}
				{% endif %}

				{% set lastCat = "" %}
				{% for noticeType in noticeTypes %}
					{% if lastCat != noticeType.notice_category %}
						{% if lastCat != "" and catTotal == 0 %}None Selected{% endif %}
						{% set lastCat = noticeType.notice_category %}
						{% set catTotal = 0 %}
						<h4>{{ noticeType.notice_category }}</h4>
					{% endif %}
					{% set hasType = false %}
					{% for mail_type in user.mail_type %}
						{% if mail_type.hans_type_id == noticeType.legacy_type_id %}{% set hasType = true %}{% endif %}
					{% endfor %}
					{% if hasType %}
						{% set catTotal = catTotal + 1 %}
						{{ noticeType.notice_type }}<br/>
					{% endif %}
				{% endfor %}
				{% if lastCat != "" and catTotal == 0 %}None Selected{% endif %}
				<hr/>
				{{ link_to("/notifications", "Change this information") }}
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">My Volcanoes</h3>
			</div>
			<div class="panel-body">
				
				{% set hasSingle = false %}
				{% for volcpref in volcprefs %}
					{% set hasObs = false %}
					{% for volcprefAll in volcprefsAll %}
						{% if volcprefAll.volcano_cd == "all_" ~ volcpref.obs_abbr %}{% set hasObs = true %}{% endif %}
					{% endfor %}
					{% if not hasObs %}{% set hasSingle = true %}{% endif %}
				{% endfor %}

				{% if volcprefsAll|length > 0 %}
					<h4>Selected Observatories</h4>
					{% for volcprefAll in volcprefsAll %}
						<div class="row vsc-padded">
							<div class="col-md-12">{{ volcprefAll.vpref }}</div>
						</div>
					{% endfor %}
				{% endif %}

				{% if volcprefsAll|length > 0 and hasSingle %}<hr/>{% endif %}

				{% if hasSingle %}
					<div class="row vsc-padded">
						<div class="col-md-4"><h4>Volcano</h4></div>
						<div class="col-md-4"><h4>Region</h4></div>
						<div class="col-md-4"><h4>Volcano Observatory</h4></div>
					</div>
					{% for volcpref in volcprefs %}
						{% set hasObs = false %}
						{% for volcprefAll in volcprefsAll %}
							{% if volcprefAll.volcano_cd == "all_" ~ volcpref.obs_abbr %}{% set hasObs = true %}{% endif %}
						{% endfor %}

						{% if not hasObs %}
							<div class="row vsc-padded">
								<div class="col-md-4">{{ volcpref.vpref }}</div>
								<div class="col-md-4">{{ volcpref.region }}</div>
								<div class="col-md-4">{{ volcpref.obs_fullname }}</div>							
							</div>
						{% endif %}
					{% endfor %}
				{% endif %}

				{% if volcprefs|length == 0 %}
					<h4>No Selected Volcanoes</h4>
				{% endif %}

				<hr/>

				{{ link_to("/volcanoes", "Change this information") }}
			</div>
		</div>

	</div>
</div>

{% endblock %}