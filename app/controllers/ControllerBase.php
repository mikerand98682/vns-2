<?php

namespace VHP\Vns\Controllers;

use \Phalcon\Mvc\Controller;
use \Phalcon\Mvc\Dispatcher;
use VHP\Vns\Models\replication_info\SyncTime;

class ControllerBase extends Controller {

    function initialize() {
        
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher) {

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        //echo $controller . ' ' . $action;
        //exit();

        global $isReplicationCurrent;
        $isReplicationCurrent = SyncTime::isReplicationCurrent();
        $this->view->isReplicationCurrent = $isReplicationCurrent;

        if ($controller != 'index' && !$isReplicationCurrent) {
            return $this->response->redirect("index");
        }

        /* The USGS requires all pages to show the page URL in the footer, this block generates and cleanses it. */
        $queryString = "";
        if (strpos($_SERVER["REQUEST_URI"], "?")) {
            $queryString = filter_var(
                                substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], "?")), 
                                FILTER_SANITIZE_STRING
                            );
        }
        if (isset($_SERVER["REDIRECT_SCRIPT_URI"])) {
            $this->view->redirectUri = $_SERVER["REDIRECT_SCRIPT_URI"] . $queryString;
        } else {
            $this->view->redirectUri = "";
        }

        if (!$this->session->get("token")) {
            $this->session->set("token", $this->security->getToken());
        }
    }

}
