<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailType;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\ActiveRecipientsWorkspace;
use VHP\Vns\Models\hans\NoticeType;

class NotificationsController extends ControllerBase {

    public function indexAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $this->view->user = MailUser::findFirst($this->session->get("userid"));
        if ($this->view->user->user_id == $this->session->get("userid")) {
            $this->view->noticeTypes = NoticeType::getNoticeTypes();
            //$this->view->noticeActivity = NoticeType::getRecentActivity();
            $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
        }
    }

    public function addAction($hans_type_id) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if (!$hans_type_id || !is_numeric($hans_type_id)) {
            $this->flash->error("Incorrect notice type.");
            return $this->response->redirect("notifications");
        }

        // Look up notice type in hans2.notice_type table to verify type exists.
        // TODO: need to convert from legacy_type_id
        $noticeType = NoticeType::findFirst(array(
                    "conditions" => "legacy_type_id = ?1",
                    "bind" => array(1 => $hans_type_id)
        ));

        if (!$noticeType) {
            $this->flash->error("Notice type not found.");
            return $this->response->redirect("notifications");
        }

        // Save new type.
        $mailType = new MailType();
        $mailType->user_id = $this->session->get("userid");
        $mailType->hans_type_id = $hans_type_id;

        if ($mailType->save()) {
            $this->flash->success("Notice type added.");
            foreach (MailAddress::getUserEmails($mailType->user_id) as $maRec) {
                ActiveRecipientsWorkspace::refreshARW($maRec->email);
            }
        } else {
            foreach ($mailType->getMessages() as $message) {
                $this->flash->error($message);
            }
        }

        //
        return $this->response->redirect("notifications");
    }

    public function removeAction($hans_type_id) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if (!$hans_type_id || !is_numeric($hans_type_id)) {
            $this->flash->error("Incorrect notice type.");
            return $this->response->redirect("notifications");
        }

        $mailTypes = MailType::find(array(
                    "conditions" => "user_id = ?1 AND hans_type_id = ?2",
                    "bind" => array(1 => $this->session->get("userid"), 2 => $hans_type_id)
        ));

        foreach ($mailTypes as $mailType) {
            if ($mailType->delete()) {
                foreach (MailAddress::getUserEmails($this->session->get("userid")) as $maRec) {
                    ActiveRecipientsWorkspace::refreshARW($maRec->email);
                }
                $this->flash->success("Notice type removed.");
            } else {
                foreach ($mailType->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        return $this->response->redirect("notifications");
    }

}
