<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Library\DailyReport;
use VHP\Vns\Library\VnsMailer;

class SenddailyController extends ControllerBase {

    public function indexAction () {

        $recipients = DailyReport::getRecipients();
        DailyReport::resetRecepients();
        //$this->view->recipients = $recipients;

        $reportData = DailyReport::getReportData();
        
        $today = date('l, M j, Y');
        $thetime = date('H:i T');
        $subject = "USGS Daily Elevated Volcano Status Summary as of $today at $thetime";

        $reportHtml = DailyReport::makeHtml($reportData);
        $reportText = DailyReport::makeText($reportData);
        $reportCell = DailyReport::makeCell($reportData);

        $this->view->reportHtml = $reportHtml;
        $this->view->reportText = $reportText;
        $this->view->reportCell = $reportCell;

        $recipientsHtml = [];
        $recipientsText = [];
        $recipientsCell = [];

        foreach ($recipients as $recipient) {
            
            $item = [
                'email' => $recipient->email, 'user_fullname' => $recipient->user_fullname, 
                'address_id' => $recipient->address_id, 'unsubscribe_key' => $recipient->unsubscribe_key
            ];
            
            if ($recipient->email_format == "html") {
                $recipientsHtml[] = $item;
            } else if ($recipient->email_format == "plaintext") {
                $recipientsText[] = $item;
            } else if ($recipient->email_format == "short (cell)") {
                $recipientsCell[] = $item;
            }
        }

        VnsMailer::addBatch($this, $recipientsHtml, $subject, $reportHtml, "html");
        VnsMailer::addBatch($this, $recipientsText, $subject, $reportText, "text");
        VnsMailer::addBatch($this, $recipientsCell, $subject, $reportCell, "text");
        VnsMailer::execSend($this);

        // DONE
        $msgs =
            '<h4>Daily Report Status</h4>' .
            count($recipientsHtml) . ' users received the HTML version.<br/>' .
            count($recipientsText) . ' users received the Text version.<br/>' .
            count($recipientsCell) . ' users received the Short version.<br/>';

        $this->flash->success($msgs);
    }
}