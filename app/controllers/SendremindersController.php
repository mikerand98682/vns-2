<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Library\Reminders;
use VHP\Vns\Library\VnsMailer;
use VHP\Vns\Models\MailUser;

class SendremindersController extends ControllerBase {

    public function indexAction () {

        $reminders = new Reminders();
        $recipients = Reminders::getRecipients();
        $subject = "USGS Volcano Notification Service Account Reminder";
        $sentTotal = 0;
        foreach ($recipients as $recipient) {

            $muRec = MailUser::findFirst($recipient->user_id);
            $muRec->reminder_sent_dttm = new \Phalcon\Db\RawValue('now()');
            $muRec->save();

            $reportContent = '';
            if ($recipient->email_format == 'html') {
                $reportContent = $reminders->makeHtml($recipient, $muRec);
                $isHtml = true;
            } else {
                $reportContent = $reminders->makeText($recipient, $muRec);
                $isHtml = false;
            }
            $batchRecipients = []; // Creating a batch one record at a time.
            $batchRecipients[] = ['email' => $recipient->email, 'user_fullname' => $recipient->user_fullname];
            VnsMailer::addBatch($this, $batchRecipients, $subject, $reportContent, ($isHtml ? "html" : "plaintext"));
            $sentTotal++;
        }
        
        VnsMailer::execSend($this);
        $this->flash->success('<h4>Reminders Sent</h4>' . $sentTotal . ' users were sent reminders.');
    }
}