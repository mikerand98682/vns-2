<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\vns2024\VnsUser;

class IndexController extends ControllerBase {

    public function indexAction() {
    }

    public function urlkeyAction() {
        
        $urlKey = $this->request->get('auth');
        $user = MailUser::findFirst(['conditions' => 'url_key = ?1', 'bind' => [1 => $urlKey]]);
        if ($user && $user->url_key == $urlKey) {

            $user->lastlogin = new \Phalcon\Db\RawValue('now()');
            $user->save();

            $this->session->set("username", $user->username);
            $this->session->set("userid", $user->user_id);
            $this->session->set("user_fullname", $user->user_fullname);

            $admins = explode(',', $this->config->application->adminUids);
            if (in_array($user->user_id, $admins)) { $this->session->set("isAdmin", true); }

            $this->flash->success(
                    "Welcome " . $user->user_fullname . " (" . $user->username . "), successfully logged in."
                    );
            return $this->response->redirect("summary");

        } else {

            $this->flash->error("Incorrect username or password.");
        }
        
        return $this->response->redirect("/profile/");
    }
    
    public function loginAction() {

        if ($this->request->isPost()) {

            if (!$this->session->get("token") || !$this->request->getPost("token") || 
                    $this->session->get("token") != $this->request->getPost("token")
            ) {
                $this->flash->error("Token rejected, did you use the login form on this site?");
                $this->session->destroy();
                return $this->response->redirect("/");
                exit();
            }
            $username = $this->request->getPost("username");

            $user = MailUser::findFirst([
                "conditions" => "LOWER(TRIM(username)) = ?1", "bind" => [1 => strtolower(trim($username))]
                ]);

            if (!$user || strtolower(trim($user->username)) != strtolower(trim($username))) {
                $this->flash->error("Incorrect username.");
                return $this->response->redirect("/");
            }
            
            if (!$user->password_hash) {
                $this->flash->error(
                        "Your old password has been removed. Please use recover account to update your settings."
                    );
                return $this->response->redirect("/subscribe/recover");
            }
            
            if (password_verify($this->request->getPost("password"), $user->password_hash)) {
            
                if (!isset($user->password_hash) || !$user->password_hash) {
                    $user->password_hash = password_hash($this->request->getPost("password"), PASSWORD_DEFAULT);
                }
                $user->lastlogin = new \Phalcon\Db\RawValue('now()');
                $user->save();

                $this->session->set("username", $user->username);
                $this->session->set("userid", $user->user_id);
                $this->session->set("user_fullname", $user->user_fullname);

                $admins = explode(',', $this->config->application->adminUids);
                if (in_array($user->user_id, $admins)) { $this->session->set("isAdmin", true); }
                    
                $this->flash->success(
                        "Welcome " . $user->user_fullname . " (" . $user->username . "), successfully logged in."
                        );
                
                // Get user emails to see if present in VNS3.
                /*
                $maRecs = MailAddress::getUserEmails($user->user_id);
                foreach($maRecs as $maRec) {
                    if(VnsUser::hasAccountInNewVns($maRec->email)) {
                        $this->flash->error(
                                "You have an account for $maRec->email in the new version of VNS, please change any "
                                . "settings by logging into <a href='/vns3/'>the new VNS here</a>."
                            );
                    }
                }
                */
                //
                return $this->response->redirect("summary");

            } else {

                $this->flash->error("Incorrect password.");
            }

        } else {

            $this->flash->error("Login method incorrect, must be http POST method.");
        }

        return $this->response->redirect("/");
    }

    public function logoutAction() {

        $this->session->destroy();
        return $this->response->redirect("/");
    }
}