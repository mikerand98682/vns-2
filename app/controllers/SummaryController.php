<?php

// TODO: Need to write the send reminders code.

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailVolcpref;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\hans\NoticeType;
use VHP\Vns\Models\replication_info\SyncTime;
use VHP\Vns\Models\vns2024\VnsUser;

class SummaryController extends ControllerBase {

    public function indexAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $this->view->user = MailUser::findFirst($this->session->get("userid"));
        $this->view->volcprefsAll = MailVolcpref::getMyAll($this->session->get("userid"));
        $this->view->volcprefs = MailVolcpref::getMyVolcanoes($this->session->get("userid"));
        
        $this->view->noticeTypes = NoticeType::getNoticeTypes();

        $addresses = MailAddress::find(array(
            "conditions" => "user_id = ?1",
            "bind"       => array(1 => $this->session->get("userid")),
            "order"      => "UPPER(email)"
            ));
        $this->view->addresses = $addresses;
        $this->view->confirm_interface = $this->rendering_view->render("templates/confirm_addresses_template", array(
            'addresses' => $addresses
        ));
        
        foreach($addresses as $maRec) {
            if(VnsUser::hasAccountInNewVns($maRec->email)) {
                $this->flash->error(
                        "You have an account for $maRec->email in the new version of VNS, please change any "
                        . "settings by logging into <a href='/vns3/'>the new VNS here</a>."
                    );
            }
        }
        
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
        
        if ($this->session->get("userid") == '161') {
            
            $dupesInfo = MailAddress::houseclean();
            $this->flash->error(
                    "Duplicate processing info: easyTotal " . $dupesInfo['easyTotal'] . ", " . 
                    "flaggedTotal " . $dupesInfo['flaggedTotal']
                    );
            
            if (!SyncTime::hasTableAccess()) {
                $this->flash->error('Warning: replication_info.sync_time table not available.');
            }
        }
    }
}