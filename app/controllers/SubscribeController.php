<?php

namespace VHP\Vns\Controllers;

use \VHP\Vns\Models\MailUser;
use \VHP\Vns\Models\MailAddress;
use \VHP\Vns\Models\MailAffiliation;
use \VHP\Vns\Models\MailVolcpref;
use \VHP\Vns\Models\MailType;
use \VHP\Vns\Models\hans\NoticeType;

use \VHP\Vns\Models\ActiveRecipientsWorkspace;
use \VHP\Vns\Models\hans\Observatory;
use \VHP\Vns\Models\mailman\Mailman;
use \VHP\Vns\Library\VnsMailer;

class SubscribeController extends ControllerBase {

    public function indexAction() {

        if ($this->request->isPost()) {

            $user = new MailUser();

            $user->user_fullname = $this->request->getPost("user_fullname");
            $user->username = trim($this->request->getPost("username"));
            $user->affiliation_id = $this->request->getPost("affiliation_id");
            $user->added = new \Phalcon\Db\RawValue('now()');

            if ($this->request->getPost("send_reminder") && $this->request->getPost("send_reminder") == "y") {
                $user->send_reminder = "y";
            } else {
                $user->send_reminder = "n";
            }

            if (count(explode(" ", $user->username)) > 1) {
                $this->flash->error("Usernames cannot contain spaces.");
                return $this->response->redirect("subscribe");
            }

            $password = $this->request->getPost("password");
            $verify = $this->request->getPost("verify");

            if (!$password || empty(trim($password)) || $password != $verify) {

                $this->flash->error("Must provide a password and verify value when creating an account, both must " .
                        "be the same value.");
            } else {

                $user->password_hash = password_hash($password, PASSWORD_DEFAULT);

                if ($user->save()) {

                    $email = $this->request->getPost("email"); // Send verification email to user.
                    if ($email) {

                        if (MailAddress::exists($this->request->getPost("email"))) {
                            $this->flash->error("Email address already present in VNS.");
                            return;
                        }

                        $address = new MailAddress();
                        $address->email = $this->request->getPost("email");
                        $address->user_id = $user->user_id;
                        $address->username = $user->username;
                        $address->confirmation_code = mt_rand(1000, 9999);
                        $address->active = "y";
                        $address->email_format = "html"; // Creating user account, email format defaults to html.
                        $address->confirmed = "n";
                        $address->duplicate_ind = "n";
                        $address->unsubscribe_key = SubscribeController::generatePassword(12);
                        $address->added = new \Phalcon\Db\RawValue('now()');
                        if ($address->save()) {
                            VnsMailer::sendConfirmation($this, $address, true);
                        }
                    }

                    $defaultsError = false;
                    // Add all observatories to mail_volcpref, default starting value.
                    $oRecs = Observatory::find();
                    foreach ($oRecs as $oRec) {
                        $mvRec = new MailVolcpref();
                        $mvRec->user_id = $user->user_id;
                        $mvRec->volcano_cd = 'all_' . $oRec->obs_abbr;
                        $mvRec->obs_abbr = $oRec->obs_abbr;
                        if (!$mvRec->save()) {
                            $this->flash->error('Encountered a problem when setting up default volcano preferences, ' .
                                    'please select at least one volcano or observatory.');
                            $defaultsError = true;
                        }
                    }

                    // Add event driven report types, default starting value.
                    $ntRecs = NoticeType::getNewUserNoticeTypes();
                    foreach ($ntRecs as $ntRec) {
                        $mtRec = new MailType();
                        $mtRec->user_id = $user->user_id;
                        $mtRec->hans_type_id = $ntRec->legacy_type_id;
                        if (!$mtRec->save()) {
                            $this->flash->error('Encountered a problem when setting up default report preferences, ' .
                                    'please select at least one report type.');
                            $defaultsError = true;
                        }
                    }

                    $defaultsMsg = 'You will receive notices from all U.S. observatories, but only during ' .
                            'volcanic unrest. You may modify your settings for specific volcanoes or to receive ' .
                            'reports more frequently by selecting notification types and volcanoes after logging ' .
                            'in.';

                    if ($defaultsError) { $defaultsMsg = ''; }
                    $this->flash->success("Account created for " . $user->user_fullname .
                            " (" . $user->username . "). $defaultsMsg");

                    foreach (MailAddress::getUserEmails($user->user_id) as $maRec) {
                        ActiveRecipientsWorkspace::refreshARW($maRec->email);
                    }
                    return $this->response->redirect("/");
                    
                } else {

                    foreach ($user->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                }
            }
        }

        $this->view->affiliations = MailAffiliation::find(array("order" => "UPPER(affiliation)"));
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    public function recoverAction() {

        if ($this->request->isPost()) {

            if (!$this->session->get("token") || !$this->request->getPost("token") ||
                    $this->session->get("token") != $this->request->getPost("token")
            ) {
                $this->flash->error("Token rejected, did you use the recover form on this site?");
                $this->session->destroy();
                return $this->response->redirect("/subscribe/recover");
                exit();
            }

            // TODO: Email a valid format?
            $email = $this->request->getPost("email");

            $address = MailAddress::findFirst(array(
                        "conditions" => "UPPER(TRIM(email)) = UPPER(TRIM(?1))",
                        "bind" => array(1 => $email)
            ));

            if ($address) { $user = MailUser::findFirst($address->user_id); }

            if (isset($user) && $address && $user->user_id == $address->user_id) {

                // Has an account recovery email been sent recently? Is it pending?
                $mailStatusRec = Mailman::getUserSubjectStatus($address->email, "VNS Account Login Link");
                if ($mailStatusRec) {
                    
                    $hasRecentRequest = false;
                    
                    if ($mailStatusRec->pending_ind == 'N' && $mailStatusRec->sent_ind == 'N') {
                        
                        $this->flash->error(
                                "Recovery email has not been sent - please wait before requesting another."
                            );
                        $hasRecentRequest = true;
                        
                    } else if ($mailStatusRec->pending_ind == 'Y' && $mailStatusRec->sent_ind == 'N') {
                        
                        $this->flash->error(
                                "Recovery email is being sent - please wait before requesting another."
                            );
                        $hasRecentRequest = true;
                        
                    } else if ($mailStatusRec->sent_ind == 'Y' && (int)$mailStatusRec->age_minutes == 1) {
                        
                        $this->flash->error("
                                Recovery email was sent $mailStatusRec->age_minutes minute ago - 
                                please wait at least 15 minutes before requesting another.
                                ");
                        $hasRecentRequest = true;
                        
                    } else if ($mailStatusRec->sent_ind == 'Y' && (int)$mailStatusRec->age_minutes < 15) {
                        
                        $this->flash->error("
                                Recovery email was sent $mailStatusRec->age_minutes minutes ago - 
                                please wait at least 15 minutes before requesting another.
                                ");
                        $hasRecentRequest = true;
                    }
                    
                    if ($hasRecentRequest) {
                        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
                        return;
                    }
                }

                // If user account was previously deactivated - re-enable it so they can log in.
                if ($address && $address->active != 'y') {
                    $address->active = 'y';
                    $address->deactivated_reason_code = null;
                    $address->deactivated_date = null;
                    $address->save();
                    ActiveRecipientsWorkspace::refreshARW($address->email);
                    $this->flash->error("Your email address was inactive but has been re-activated.");
                }
                
                $user->url_key = md5(SubscribeController::generatePassword(12));

                if ($user->save()) {

                    $loginUrl = $this->config->application->publicUrl . $this->config->application->baseUrl .
                            'index/urlkey?auth=' . $user->url_key;

                    $body = "
                            This is an automatically generated email being sent because you have requested a link to 
                            login to your Volcano Notification Service account.
                            <br/><br/>\r\n\r\n
                            You can log in to your account by visiting: <br/>\r\n
                            <a href=\"$loginUrl\">$loginUrl</a><br/>\r\n
                            ";
 
                    VnsMailer::sendSingle($this, $email, $user->user_fullname, "VNS Account Login Link", $body, "html");
                    $this->flash->success("
                            An account login link has been sent to " . $email . ". 
                            Please be patient, it can take several minutes for your recovery email to arrive. Each
                            new request makes any previous ones invalid.
                            ");
                    return $this->response->redirect("/");
                    
                } else {

                    $this->flash->error("There was a problem when trying to update account information, please email " .
                            $this->config->application->helpEmail .
                            " with details about how and when you encountered the problem.");
                }
                
            } else {

                $this->flash->error("Email address not found, nothing to recover.");
            }
        }

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    /**
     * Allow user to delete account.
     */
    public function unsubscribeAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    // Provides a way to directly unsubscribe without needing to log in. For list-unsubscribe headers, etc.
    public function listUnsubscribeAction() {

        if ($this->config->application->mode != 'maintenance') {
            $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
            $this->view->unsubscribeMsg = MailAddress::listUnsubscribe($this->request->get('aid'), $this->request->get('usk'));
        }
    }

    public function complaintUnsubscribeAction() {

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");

        $result = null;

        if ($this->config->mail->autoUnsubscribeKey != $this->request->get('auk')) {
            $this->view->unsubscribeMsg = 'ERROR: provided parameter auk incorrect.';
        } else {
            $result = MailAddress::complaintUnsubscribe($this->request->get('rem'));
            $this->view->unsubscribeMsg = $result['inactiveMsg'];
        }
        // DO NOT SEND ANY NEW MESSAGES TO RECIPIENT! WILL CREATE ENDLESS LOOP WITH AMAZON EMAIL SERVICES. 
    }

    public function bounceAction() {

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");

        if ($this->config->mail->autoUnsubscribeKey != $this->request->get('auk')) {
            $this->view->unsubscribeMsg = 'ERROR: provided parameter auk incorrect.';
        } else {
            $this->view->unsubscribeMsg = MailAddress::bounceUnsubscribe($this->request->get('rem'));
        }
    }

    public function deleteaccountAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        // Delete from all tables.
        $user = MailUser::findFirst($this->session->get("userid"));
        if ($user && $user->user_id == $this->session->get("userid")) {

            $addresses = $user->mail_address;
            foreach ($addresses as $address) { $address->delete(); }

            $types = $user->mail_type;
            foreach ($types as $type) { $type->delete(); }

            $prefs = $user->mail_volcpref;
            foreach ($prefs as $pref) { $pref->delete(); }

            if ($user->delete()) {
                
                ActiveRecipientsWorkspace::deleteUser($user->user_id);
                $this->flash->success('All account information removed for user ' . $user->username . '.');
                $this->session->set("username", "");
                $this->session->set("userid", "");
                $this->session->set("user_fullname", "");
                
            } else {
                
                foreach ($user->getMessages() as $message) { $this->flash->error($message); }
            }
            
        } else {

            $this->flash->error("User information not found, nothing to remove.");
        }

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
        return $this->response->redirect("/");
    }

    // Create random password for use during reset.
    static function generatePassword($length = 8, $complexity = 2) {

        if ($complexity > 4) {
            $complexity = 4;
        }
        $lower = 'a,b,c,d,e,f,g,h,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z';
        $nums = '2,3,4,5,6,7,8,9';
        $upper = strtoupper($lower);
        $special = '!,@,#,$,%,^,&,*,(,)';
        $level[0] = explode(',', $lower);
        $level[1] = explode(',', $nums);
        $level[2] = explode(',', $upper);
        $level[3] = explode(',', $special);

        $chars = [];
        for ($x = 0; $x < $complexity; $x++) {
            $chars = array_merge($chars, $level[$x]);
        }

        $charlen = count($chars) - 1;

        $password = '';
        for ($i = 0; $i < $length; $i++) { $password .= $chars[mt_rand(0, $charlen)]; }

        return $password;
    }

}
