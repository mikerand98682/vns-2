<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\ActiveRecipientsWorkspace;
use VHP\Vns\Library\VnsMailer;
use VHP\Vns\Controllers\SubscribeController;

class EmailController extends ControllerBase {

    public function indexAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $addresses = MailAddress::find(array(
            "conditions" => "user_id = ?1",
            "bind"       => array(1 => $this->session->get("userid")),
            "order"      => "UPPER(email)"
            ));
        $invalidAddresses = [];
        foreach ($addresses as $maRec) {
            if (!isset($maRec->unsubscribe_key) || !is_numeric($maRec->unsubscribe_key)) {
                $maRec->unsubscribe_key = SubscribeController::generatePassword(12);
                $maRec->save();
                ActiveRecipientsWorkspace::refreshARW($maRec->email);
            }
            if (!VnsMailer::isEmailFormatOk($maRec->email)) {
                $invalidAddresses[] = $maRec->email;
            }
        }
        $this->view->addresses = $addresses;
        $this->view->formatOptions = MailAddress::getFormats();
        $this->view->activeOptions = MailAddress::getActive();
        $this->view->summaryOptions = MailAddress::getDailySummaryOptions();
        $this->view->invalidAddresses = $invalidAddresses;

        $this->view->confirm_interface = $this->rendering_view->render("templates/confirm_addresses_template", array(
            'addresses' => $addresses
        ));
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    public function confirmAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($this->request->isPost()) {

            $address_id = $this->request->getPost("address_id");
            $confirmation_code = $this->request->getPost("confirmation_code");

            $address = MailAddress::findFirst($address_id);

            if ($address->confirmation_code == $confirmation_code) {

                $address->active = "y";
                $address->confirmed = "y";

                if ($address->save() == false) {

                    foreach ($address->getMessages() as $message) { $this->flash->error($message); }

                } else {

                    ActiveRecipientsWorkspace::refreshARW($address->email);
                    $this->flash->success("Confirmation code accepted, address is active.");
                }

            } else {

                $this->flash->error("Incorrect confirmation code.");
            }

        } else {

            $this->flash->error("Method incorrect, must be http POST method.");
        }

        return $this->response->redirect("email");
    }

    public function createAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($this->request->isPost()) {

            if (!preg_match("/\S+@\S+\.\S+/", $this->request->getPost("email"))) {
                $this->flash->error("Email addresses must be formatted properly.");
                return $this->response->redirect("email");
            }
            
            if (MailAddress::exists($this->request->getPost("email"))) {
                $this->flash->error("Email address already present in VNS.");
                return $this->response->redirect("email");
            }

            $address = new MailAddress();
            $address->user_id = $this->session->get("userid");
            $address->username = $this->session->get('username');
            $address->email_format = $this->request->getPost("email_format");
            $address->email = $this->request->getPost("email");
            $address->receive_daily_summary_ind = $this->request->getPost("receive_daily_summary_ind");
            $address->active = "y";
            $address->confirmed = "n";
            $address->duplicate_ind = "n";
            $address->unsubscribe_key = SubscribeController::generatePassword(12);
            $address->added = new \Phalcon\Db\RawValue('now()');
            
            if ($address->save() == false) {

                foreach ($address->getMessages() as $message) { $this->flash->error($message); }

            } else {

                ActiveRecipientsWorkspace::refreshARW($address->email);
                VnsMailer::sendConfirmation($this, $address, false);
                $this->flash->success("Address saved.");
            }

        } else {

            $this->flash->error("Method incorrect, must be http POST method.");
        }

        return $this->response->redirect("email");
    }

    public function editAction($address_id = null) {
        
        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($address_id && is_numeric($address_id)) {
            $address = MailAddress::getAddress($address_id);
            if ($address->address_id != $address_id) { 
                unset($address); 
                $this->flash->error("Address not found for provided id $address_id");
            }
        }

        if ($this->request->isPost()) {

            $address_id = $this->request->getPost("address_id");
            $address = MailAddress::findFirst($address_id);
            if ($address->address_id != $address_id) { unset($address); }

            $address->active = $this->request->getPost("active");
            $address->receive_daily_summary_ind = $this->request->getPost("receive_daily_summary_ind");
            $address->email_format = $this->request->getPost("email_format");

            if ($address->active == 'n') {
                
                $address->deactivated_reason_code = 'U';
                $address->deactivated_date = new \Phalcon\Db\RawValue('now()');
                
            } else if ($address->active == 'y') {

                $address->deactivated_reason_code = null;
                $address->deactivated_date = null;
            }

            if ($address->email != $this->request->getPost("email")) {

                if (MailAddress::exists($this->request->getPost("email"))) {
                    $this->flash->error("Email address already present in VNS.");
                    return $this->response->redirect("email");
                }

                $address->username = $this->session->get('username');
                $address->email = $this->request->getPost("email");
                $address->active = "n";
                $address->confirmed = "n";
                VnsMailer::sendConfirmation($this, $address, false);
            }

            if ($address->save() == false) {

                foreach ($address->getMessages() as $message) { $this->flash->error($message); }

            } else {

                ActiveRecipientsWorkspace::refreshARW($address->email);
                $this->flash->success("Address updated.");
            }
            
            return $this->response->redirect("email");
        }

        \Phalcon\Tag::setDefault("address_id", $address->address_id);
        \Phalcon\Tag::setDefault("user_id", $this->session->get("userid"));
        \Phalcon\Tag::setDefault("email", $address->email);
        \Phalcon\Tag::setDefault("active", $address->active);
        \Phalcon\Tag::setDefault("email_format", $address->email_format);
        \Phalcon\Tag::setDefault("confirmed", $address->confirmed);
        \Phalcon\Tag::setDefault("receive_daily_summary_ind", $address->receive_daily_summary_ind);

        $address->deactivatedMsg = "";
        if ($address->active == 'n') {
            foreach (MailAddress::getDeactivatedReasonCodes() as $reasonCode => $reason) {
                if ($address->deactivated_reason_code == $reasonCode) {
                    $address->deactivatedMsg = $reason . " Deactivated " . $address->deactivated_date . " UTC.";
                }
            }
        }

        $this->view->maRec = $address;
        $this->view->formatOptions = MailAddress::getFormats();
        $this->view->activeOptions = MailAddress::getActive();
        $this->view->summaryOptions = MailAddress::getDailySummaryOptions();
        
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    public function removeAction($address_id) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $address = MailAddress::findFirst($address_id);
        if ($address->user_id == $this->session->get("userid")) {
            $address->delete();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success($address->email . " removed from VNS.");
        } else {
            $this->flash->error("Cannot remove email for another user.");
        }

        return $this->response->redirect("email");
    }

    public function resendAction($address_id) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }
        if (!is_numeric($address_id)) {
            $this->flash->error("Invalid address.");
            return $this->response->redirect("/");
        }
        $address = MailAddress::findFirst($address_id);
        $address->username = $this->session->get('username');
        if ($address->address_id != $address_id) {
            $this->flash->error("Address found is different from requested address.");
            return $this->response->redirect("/");
        }
        VnsMailer::sendConfirmation($this, $address, false);

        return $this->response->redirect("email");
    }
}