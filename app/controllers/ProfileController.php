<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailAffiliation;

class ProfileController extends ControllerBase {

    public function indexAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $user = MailUser::findFirst($this->session->get("userid"));
        if ($user && $user->user_id == $this->session->get("userid")) {

            \Phalcon\Tag::setDefault("user_id", $user->user_id);
            \Phalcon\Tag::setDefault("username", $user->username);
            \Phalcon\Tag::setDefault("user_fullname", $user->user_fullname);
            \Phalcon\Tag::setDefault("affiliation_id", $user->affiliation_id);
            \Phalcon\Tag::setDefault("send_reminder", $user->send_reminder);

            $this->view->user = $user;
            $this->view->affiliations = MailAffiliation::find(array("order" => "UPPER(affiliation)"));
            $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
        }
    }

    /**
     * Change data in mail_user table, 
     */
    public function profileAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($this->request->isPost()) {

            $user = MailUser::findFirst($this->request->getPost("user_id"));
            if ($user->user_id != $this->session->get("userid")) {
                $this->flash->error("Userid mismatch, trying to update different user? Changes not saved.");
                return $this->response->redirect("profile");
            }

            $user->user_fullname = $this->request->getPost("user_fullname");
            $user->username = $this->request->getPost("username");
            $user->affiliation_id = $this->request->getPost("affiliation_id");

            if ($this->request->getPost("send_reminder") && $this->request->getPost("send_reminder") == "y") {
                $user->send_reminder = "y";
            } else {
                $user->send_reminder = "n";
            }

            if (count(explode(" ", $user->username)) > 1) {
                $this->flash->error("Usernames cannot contain spaces.");
                return $this->response->redirect("profile");
            }

            if ($user->save() == false) {

                foreach ($user->getMessages() as $message) {
                    $this->flash->error($message);
                }
                //return $this->dispatcher->forward(array("controller" => "notice", "action" => "index", "params" => array($notice_id)));
            } else {

                $this->flash->success("Profile changes saved.");
            }
        } else {

            $this->flash->error("Method incorrect, must be http POST method.");
        }

        return $this->response->redirect("profile");
    }

    public function passwordAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($this->request->isPost()) {

            $user = MailUser::findFirst($this->request->getPost("user_id"));
            if ($user->user_id != $this->session->get("userid")) {
                $this->flash->error("Userid mismatch, trying to update different user? Changes not saved.");
                return $this->response->redirect("profile");
            }

            $password = $this->request->getPost("password");
            $verify = $this->request->getPost("verify");

            if (strlen(trim($password)) < 1 || strlen(trim($verify)) < 1) {
                $this->flash->error("Both the password and verify boxes need to be filled to change your password.");
                return $this->response->redirect("profile");
            }

            if ($password != $verify) {
                $this->flash->error("Password and verify do not match. Password not changed.");
                return $this->response->redirect("profile");
            }
            /* DOLATER: Determine best password check and also implement in javascript.
              if (!preg_match("/(?=.*\d).{4,15}/", $password)) {
              $this->flash->error("Passwords must be between 4 and 15 characters, and contain at least one number.");
              return $this->response->redirect("profile");
              }
             */
            $user->password_hash = password_hash($password, PASSWORD_DEFAULT);

            if ($user->save() == false) {

                foreach ($user->getMessages() as $message) {
                    $this->flash->error($message);
                }
                //return $this->dispatcher->forward(array("controller" => "notice", "action" => "index", "params" => array($notice_id)));
            } else {

                $this->flash->success("Password changed.");
            }
        } else {

            $this->flash->error("Method incorrect, must be http POST method.");
        }

        return $this->response->redirect("profile");
    }

}
