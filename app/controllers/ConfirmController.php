<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\ActiveRecipientsWorkspace;

class ConfirmController extends ControllerBase {

    public function indexAction() {

        $address_id = $this->request->get("address_id");
        $confirmation_code = $this->request->get("confirmation_code");

        $address = MailAddress::findFirst($address_id);
        $this->view->confirmed = false;

        if ($address->confirmation_code == $confirmation_code) {

            $address->active = "y";
            $address->confirmed = "y";

            if ($address->save() == false) {

                foreach ($address->getMessages() as $message) { $this->flash->error($message); }

                $this->view->confirmationStatus = 
                        'Errors (above) were detected when trying to confirm your email address.';
                
            } else {

                ActiveRecipientsWorkspace::refreshARW($address->email);
                $this->view->confirmed = true;
                $this->view->confirmationStatus = 
                        "Confirmation code accepted, address " . $address->email . " is active.";
            }

        } else {

            $this->view->confirmationStatus = "Incorrect confirmation code.";
        }
    }
}