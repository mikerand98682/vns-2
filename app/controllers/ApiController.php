<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\MailAddressActiveTotal;
use VHP\Vns\Models\ActiveRecipientsWorkspace;
use VHP\Vns\Models\hans\Observatory;
use VHP\Vns\Models\hans\Volcano;
use Phalcon\Http\Response;

class ApiController extends ControllerBase {

    private $isData = false;

    public function indexAction() {
        $this->isData = false;
    }

    /**
     * Provide a username value with GET method, returns available|unavailable|ERROR: {message}
     */
    public function usernameusedAction($username) {

        $this->isData = true;
        $dataOut = 'ERROR: don\'t know how to handle request.';

        if ($this->request->isGet() && $username) { // Select a notice

            $user_id = 0;
            // Is there a session? Is user logged in?
            if ($this->session->get("userid")) { $user_id = $this->session->get("userid"); }

            // Try to find a record for different user with username, trimmed and case insensitive. user_id will be 0 unless a value is found in a session.
            $mailUser = MailUser::findFirst([
                            'conditions' => 'UPPER(TRIM(username)) = UPPER(TRIM(:username:)) AND user_id <> :user_id:',
                            'bind'       => ['username' => $username, 'user_id' => $user_id]
                            ]);

            if ($mailUser) {
                $dataOut = 'unavailable';
            } else {
                $dataOut = 'available';
            }
        }

        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        return $this->rendering_view->render("templates/api_template", ["data" => json_encode($dataOut)]);
    }

    /**
     * Provide a username value with GET method, returns available|unavilable|ERROR: {message}
     */
    public function emailusedAction($email) {

        $this->isData = true;
        $dataOut = 'ERROR: don\'t know how to handle request.';

        if ($this->request->isGet() && $email) { // Select a notice

            $user_id = 0;
            // Is there a session? Is user logged in?
            if ($this->session->get("userid")) { $user_id = $this->session->get("userid"); }

            // Try to find a record for email, trimmed and case insensitive.
            $maRecs = MailAddress::find([
                            'conditions' => 'UPPER(TRIM(email)) = UPPER(TRIM(:email:))',
                            'bind'       => ['email' => $email]
                            ]);

            $usedTot = 0; // Any value greater than 1 means it's a duplicate and can't be used. Prevents need 
            foreach ($maRecs as $maRec) {
                if ($maRec->user_id == $user_id) {
                    $usedTot = $usedTot + 1;
                } else { // A different user has claimed the email address, just pop usedTot over the limit.
                    $usedTot = 2;
                }
            }

            if ($usedTot == 1) {
                $dataOut = 'mine';
            } else if ($usedTot > 1) {
                $dataOut = 'unavailable';
            } else {
                $dataOut = 'available';
            }
        }

        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        return $this->rendering_view->render("templates/api_template", ["data" => json_encode($dataOut)]);
    }

    public function updateactivetotalsAction() {

        MailAddressActiveTotal::updateToday();
        
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        return $this->rendering_view->render(
                        "templates/api_template", 
                        ["data" => json_encode("Active email total for today saved.")]
                );
    }

    public function updateARWAction() {
        
        set_time_limit(0);
        
        $start = time();
        $data = ActiveRecipientsWorkspace::refreshARW();
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        return $this->rendering_view->render("templates/api_template", [ 
            "data" => json_encode("Active recipients workspace table refreshed. Ran for " . (time() - $start) . "s.")
            ]);
    }
    
    /**
     * 
     */
    public function afterExecuteRoute($dispatcher) {

        if ($this->isData) {
            $response = new Response();
            $response->setHeader('Content-Type', 'application/json');
            $response->setContent($dispatcher->getReturnedValue());
            $dispatcher->setReturnedValue($response);
        }
    }
    
    // Create various json files used by various JavaScript features. This is duplicated in AdminController.
    public function makeJsonAction() {

        $msgs = "";
        $start = time();
        
        // observatories.json
        $data = [];
        $observatories = Observatory::find(array(
            "columns" => "obs_abbr, obs_name, obs_fullname",
            "order"   => "UPPER(obs_fullname)"
            ));

        foreach ($observatories as $observatory) {
            $data[] = array(
                "obs_abbr"     => $observatory->obs_abbr,
                "obs_name"     => $observatory->obs_name,
                "obs_fullname" => $observatory->obs_fullname,
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "observatories.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= " File observatories.json created/refreshed. ";

        // regions.json
        $data = [];
        $regions = Volcano::selectRegions();
        foreach ($regions as $region) {
            $data[] = array(
                "region"       => $region->region,
                "obs_abbr"     => $region->obs_abbr,
                "obs_fullname" => $region->obs_fullname,
                "vtot"         => $region->vtot
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "regions.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= " File regions.json created/refreshed. ";

        // volcanoes.json
        $data = [];
        $volcanoes = Volcano::find(array(
            "order"   => "UPPER(volcano_name)"
            ));

        foreach ($volcanoes as $volcano) {
            $data[] = array(
                "volcano_cd"   => $volcano->volcano_cd,
                "volcano_name" => $volcano->volcano_name,
                "region"       => $volcano->region,
                "latitude"     => $volcano->latitude,
                "longitude"    => $volcano->longitude,
                "vnum"         => $volcano->vnum,
                "obs_abbr"     => $volcano->observatory->obs_abbr
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "volcanoes.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= " File volcanoes.json created/refreshed. ";

        // DONE
        $response = new Response();
        $response->setHeader('Content-Type', 'application/json');
        return $this->rendering_view->render(
                        "templates/api_template", 
                        ["data" => json_encode($msgs . " Ran for " . (time() - $start) . "s. ")]
                );
    }
}