<?php

// TODO: Need to code reminders. Do we need this feature?
// TODO: Create a crypt function in a library and implement better encryption of passwords. Implement in phases. See https://alias.io/2010/01/store-passwords-safely-with-php-and-mysql/

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\ActiveRecipientsWorkspace;

class AdminController extends ControllerBase {

    // Executed before any action. All actions on this controller must be done by someone with admin privileges.
    // To get admin privileges, add user_id value to the appropriate element in the config.ini file.
    public function initialize() {

        if (!$this->session->get("userid") || !$this->session->get("isAdmin")) {
            $this->flash->error("Please login as admin user.");
            return $this->response->redirect("/");
            exit;
        }

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    public function indexAction() {

    }

    // Search for users based on username, name or email address.
    public function searchAction() {

        if ($this->request->isPost()) {
            $search = $this->request->getPost("search");
            \Phalcon\Tag::setDefault("search", $search);
            $this->view->results = MailUser::search($search);
        }
    }

    // Change a user's password.
    public function passwordAction() {
        
        if ($this->request->isPost()) {

            $user = MailUser::findFirst($this->request->getPost("user_id"));

            if (!$user || $user->user_id != $this->request->getPost("user_id")) {
                $this->flash->error('User not found to reset password.');
                return $this->response->redirect("/admin/user/" . $user->user_id);
            }

            $password = $this->request->getPost("password");
            $verify = $this->request->getPost("verify");

            if (strlen(trim($password)) < 1 || strlen(trim($verify)) < 1) {
                $this->flash->error("Both the password and verify boxes need to be filled to change your password.");
                return $this->response->redirect("/admin/user/" . $user->user_id);
            }

            if ($password != $verify) {
                $this->flash->error("Password and verify do not match. Password not changed.");
                return $this->response->redirect("/admin/user/" . $user->user_id);
            }

            /* TODO: Deterine a minimum password strategy.
            if (!preg_match("/(?=.*\d).{4,15}/", $password)) {
                $this->flash->error("Passwords must be between 4 and 15 characters, and contain at least one number.");
                return $this->response->redirect("/admin/user/" . $user->user_id);
            }
            */
            $user->password_hash = password_hash($password, PASSWORD_DEFAULT);
            
            if ($user->save() == false) {

                foreach ($user->getMessages() as $message) { $this->flash->error($message); }

            } else {

                $this->flash->success("Password changed for " . $user->username);
            }

            return $this->response->redirect("/admin/user/" . $user->user_id);
        }
    }

    // Display a user's information.
    public function userAction($user_id) {

        $user = MailUser::findFirst($user_id);
        if (!$user || $user->user_id != $user_id) {
            $this->flash->error('User not found.');
            return $this->response->redirect("/admin/user/" . $user_id);
        }
        \Phalcon\Tag::setDefault("user_id", $user->user_id);

        $this->view->user = $user;
        
        $maRecs = MailAddress::getUser($user_id);

        $data = [];
        foreach ($maRecs as $maRec) {

            $maRec->deactivatedMsg = "";
            if ($maRec->active == 'n') {
                foreach (MailAddress::getDeactivatedReasonCodes() as $reasonCode => $reason) {
                    if ($maRec->deactivated_reason_code == $reasonCode) { $maRec->deactivatedMsg = $reason . "<br/>"; }
                }
            }
            
            $ddInfo = ActiveRecipientsWorkspace::getByEmail($maRec->email);
            
            if (isset($ddInfo['user_id']) && ((int)$maRec->user_id != (int)$ddInfo['user_id'])) { 
                $data[] = array_merge((array)$maRec, [ 'deepdive' => [] ]);
            } else {
                $data[] = array_merge(
                        (array)$maRec, [ 'deepdive' => ActiveRecipientsWorkspace::getByEmail($maRec->email) ]
                    );
            }
        }
        $this->view->addresses = $data;
    }
        
    // Delete a user, much of this functionality is duplicated in SubscribeController->unsubscribeAction
    public function deleteuserAction($user_id) {

        // Delete from all applicable tables.
        $user = MailUser::findFirst($user_id);
        if ($user && $user->user_id == $user_id) {

            $addresses = $user->mail_address;
            foreach ($addresses as $address) { $address->delete(); }

            $types = $user->mail_type;
            foreach ($types as $type) { $type->delete(); }

            $prefs = $user->mail_volcpref;
            foreach ($prefs as $pref) { $pref->delete(); }

            if ($user->delete()) {
                foreach(MailAddress::getUserEmails($user->user_id) as $maRec) {
                    ActiveRecipientsWorkspace::refreshARW($maRec->email);
                }
                $this->flash->success("User " . $user->username . ", account removed/deleted.");

            } else {

                foreach ($user->getMessages() as $message) { $this->flash->error($message); }
            }

        } else {

            $this->flash->error("User account not found to delete.");
        }
        return $this->response->redirect("/admin");
    }
 
    // Remove an email address.
    public function deleteaddressAction($address_id) {

        $address = MailAddress::findFirst($address_id);
        if ($address && $address->address_id == $address_id) {
            $address->delete();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success("Address " . $address->email . " deleted.");
            $this->response->redirect("admin/user/" . $address->user_id);
        }
    }

    // Mark an email address as confirmed.
    public function confirmAction($address_id) {

        $address = MailAddress::findFirst($address_id);
        if ($address && $address->address_id == $address_id) {
            $address->confirmed = "y";
            $address->save();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success("Address " . $address->email . " confirmed.");
            $this->response->redirect("admin/user/" . $address->user_id);
        }
    }

    // Mark an email address as unconfirmed.
    public function unconfirmAction($address_id) {

        $address = MailAddress::findFirst($address_id);
        if ($address && $address->address_id == $address_id) {
            $address->confirmed = "n";
            if (!$address->confirmation_code) { $address->confirmation_code = mt_rand(1000, 9999); }
            $address->save();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success("Address " . $address->email . " marked unconfirmed.");
            $this->response->redirect("admin/user/" . $address->user_id);
        }
    }

    // Mark an email address as active, should be done by the user.
    public function activateAction($address_id) {

        $address = MailAddress::findFirst($address_id);
        if ($address && $address->address_id == $address_id) {
            $address->active = "y";
            $address->deactivated_reason_code = null;
            $address->deactivated_date = null;
            $address->save();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success("Address " . $address->email . " activated.");
            $this->response->redirect("admin/user/" . $address->user_id);
        }
    }

    // Mark an email address as inactive, should be done by the user.
    public function deactivateAction($address_id) {

        $address = MailAddress::findFirst($address_id);
        if ($address && $address->address_id == $address_id) {
            $address->active = "n";
            $address->deactivated_reason_code = 'A';
            $address->deactivated_date = new \Phalcon\Db\RawValue('now()');
            $address->save();
            ActiveRecipientsWorkspace::refreshARW($address->email);
            $this->flash->success("Address " . $address->email . " deactivated.");
            $this->response->redirect("admin/user/" . $address->user_id);
        }
    }

    public function stopaddressAction($address_id) {
        
        $msg = '';
        $maRec = null;
        if ($this->request->isGet()) {
        
            if ($address_id && is_numeric($address_id) && $address_id > 0) {
                $maRec = MailAddress::findFirst($address_id);
            }
            if ($maRec->address_id != $address_id) { $maRec = null; }
            $stopaddress = $maRec->email;
            
        } else if ($this->request->isPost()) {
            
            $stopaddress = trim($this->request->getPost('stopaddress'));
            $maRec = MailAddress::findFirst(array(
                        'conditions' => 'email = ?1 AND duplicate_ind = ?2', 
                        'bind' => array(1 => $stopaddress, 2 => 'n')
                        ));
        }

        if (!$maRec || $maRec == null || $maRec->address_id != $address_id) {
            $this->view->msg = "Did not find email address, $stopaddress, in VNS.";
            return;
        } else if ($maRec->active != 'y') {
            $this->view->msg = "$stopaddress is already inactive, no message sent.";
            return;
        } else if ($maRec->confirmed != 'y') {
            $this->view->msg = "$stopaddress is un-confirmed, no message sent.";
            return;
        }

        $maRec->active = 'n';
        $maRec->deactivated_reason_code = 'A';
        $maRec->deactivated_date = new \Phalcon\Db\RawValue('now()');
        $maRec->save();
        ActiveRecipientsWorkspace::refreshARW($stopaddress);
        $muRec = MailUser::findFirst($maRec->user_id);
        if (!$muRec || $muRec->user_id != $maRec->user_id) { exit; }
        $msg = "Address $stopaddress has been disabled for user $muRec->user_fullname ($muRec->username).";
        $subject = "USGS Volcano Notification Service Emails Disabled";
        $body = "
            You are receiving this message because we have received a request to disable email to this account 
            from the Volcano Notification Service. Mail you receive from VNS to other email addresses will continue.
            <br/><br/>
            If you did not specifically request that mail to this address be stopped, you may have hit the spam
            button in your email application by mistake.
            <br/><br/>
            You can re-enable email by visiting the <a href='https://volcanoes.usgs.gov/vns2/'>Volcano Notification 
            Service</a> and selecting My Email Addresses after logging in.
            ";
        \VHP\Vns\Library\VnsMailer::sendSingle($this, $maRec->email, $muRec->user_fullname, $subject, $body, 'html');

        $this->view->msg = $msg;
    }
    
    // Become a user so you can change their information.
    public function masqueradeAction($user_id) {

        $user = MailUser::findFirst($user_id);

        if ($user && $user->user_id == $user_id) {
        
            $this->flash->success("Masquerading as user " . $user->username . ", any changes will be permanent!");
            
            $this->session->set("username", $user->username);
            $this->session->set("userid", $user->user_id);
            $this->session->set("user_fullname", $user->user_fullname);

            $admins = explode(',', $this->config->application->adminUids);
            if (in_array($user->user_id, $admins)) {
                $this->session->set("isAdmin", true);
            } else {
                $this->session->set("isAdmin", false);
            }

            $this->response->redirect("/summary");
        }

    }

    // Create various json files used by various JavaScript features. This is duplicated in ApiController.
    /*
    public function makeJsonAction() {

        $msgs = "";

        // observatories.json
        $data = [];
        $observatories = Observatory::find(array(
            "columns" => "obs_abbr, obs_name, obs_fullname",
            "order"   => "UPPER(obs_fullname)"
            ));

        foreach ($observatories as $observatory) {
            $data[] = array(
                "obs_abbr"     => $observatory->obs_abbr,
                "obs_name"     => $observatory->obs_name,
                "obs_fullname" => $observatory->obs_fullname,
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "observatories.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= "observatories.json created/refreshed.<br/>";

        // regions.json
        $data = [];
        $regions = Volcano::selectRegions();
        foreach ($regions as $region) {
            $data[] = array(
                "region"       => $region->region,
                "obs_abbr"     => $region->obs_abbr,
                "obs_fullname" => $region->obs_fullname,
                "vtot"         => $region->vtot
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "regions.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= "regions.json created/refreshed.<br/>";

        // volcanoes.json
        $data = [];
        $volcanoes = Volcano::find(array(
            "order"   => "UPPER(volcano_name)"
            ));

        foreach ($volcanoes as $volcano) {
            $data[] = array(
                "volcano_cd"   => $volcano->volcano_cd,
                "volcano_name" => $volcano->volcano_name,
                "region"       => $volcano->region,
                "latitude"     => $volcano->latitude,
                "longitude"    => $volcano->longitude,
                "vnum"         => $volcano->vnum,
                "obs_abbr"     => $volcano->observatory->obs_abbr
                );
        }

        // Write to file.
        if ( !$handle = fopen($this->config->application->jsonDir . "volcanoes.json", "w") ) { exit; }
        if ( fwrite($handle, json_encode($data)) == FALSE ) { exit; }
        fclose($handle);

        $msgs .= "volcanoes.json created/refreshed.<br/>";

        // DONE
        $this->flash->success($msgs);
        return $this->response->redirect("admin");
    }
     */
}