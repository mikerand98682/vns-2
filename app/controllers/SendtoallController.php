<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Library\VnsMailer;
use VHP\Vns\Library\SendToAll;
use VHP\Vns\Models\ActiveRecipientsWorkspace;
use VHP\Vns\Models\mailman\Mailman;

class SendtoallController extends ControllerBase {
    
    public function indexAction () {

        // IF NOT ADMIN USER - DO NOT DO ANY OF THIS!
        if (!$this->session->get("isAdmin")) {
            $this->flash->error("Please login as admin user.");
            return $this->response->redirect("/");
            exit;
        }

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
        
        $sendToAll = new SendToAll();
        
        // 
        
        global $config, $logger;
        
        // ActiveRecipientsWorkspace::refreshARW();
        $arwTotal = ActiveRecipientsWorkspace::getTotal();
        $this->view->config = $config;
        $this->view->arwTotal = $arwTotal;
        $subject = "";
        $body = "";
        $recipients = "";
        $includeSettings = false;
        $ageFilterDays = 'all';
        $recentSkippedTotal = 0;
        $sendingTotal = 0;
        $dryRun = true;
        $startingBatch = 0;
        
        if ($this->request->isPost()) { 
                    
            $adminUidsArray = explode(',', $this->config->application->adminUids);
            $adminEmails = [];
            foreach($adminUidsArray as $user_id) {
                $newEmails = ActiveRecipientsWorkspace::getUserEmails($user_id);
                $adminEmails = array_merge($adminEmails, $newEmails);
            }
            
            $subject = $this->request->getPost("subject");
            $body = $this->request->getPost("body");
            $recipients = $this->request->getPost("recipients");
            $ageFilterDays = $this->request->getPost("age-filter");

            $confirmFullSend = null !== $this->request->getPost("confirm-full-send");
            $dryRun = null !== $this->request->getPost("dry-run");
                        
            $logger->info("Preparing Send To All - $subject");
            
            if (is_numeric($this->request->getPost("starting-batch"))) {
                $startingBatch = (int)$this->request->getPost("starting-batch");
                $logger->info(" Send to All - starting at batch $startingBatch");
            }

            $includeSettings = null !== $this->request->getPost("include-settings");
            if ($includeSettings) { 
                $logger->info(" Send To All - including user settings");
                $this->flash->success("Including user settings in their emails."); 
            } else {
                $logger->info(" Send To All - NOT including user settings");
            }
            
            $emails = [];
                        
            if (isset($recipients) && strlen(trim($recipients)) > 0) { $emails = explode(",", $recipients); }
            
            if (count($emails) == 0 && $config->application->mode != "production") {
                $this->flash->error("NOT PRODUCTION MODE - ONLY SENDING TO " . $config->mail->devemail);
                $emails[] = $config->mail->devemail;
            }
            
            if (count($emails) > 0) { // We're either in dev mode or just sending to a few recipients.
                
                $logger->info(" Send To All - limited batch, dev mode or recipients specified.");
                
                foreach ($emails as $email) {
                    
                    if (!VnsMailer::isEmailFormatOk($email)) {
                        $this->flash->error("Email, $email, is invalid format, SKIPPING");
                        continue;
                    }
                    
                    if ($sendToAll->hasRecent($email, $ageFilterDays)) {
                        $this->flash->error("Recently received notice, SKIPPING " . $email);
                        $recentSkippedTotal++;
                        continue;
                    }
                    
                    $arwRec = ActiveRecipientsWorkspace::getByEmail($email);
                    if (!$arwRec || !$arwRec['address_id'] || !is_numeric($arwRec['address_id'])) {
                        $this->flash->error("Email $email is not a VNS user, not sending.");
                        continue;
                    }
                    
                    $this->flash->success("SENDING TO " . $email);
                    $sendingTotal++;
                    if (!$dryRun) { 
                        
                        if (in_array($email, $adminEmails)) {
                             
                            $this->flash->success(
                                    "Because $email is an admin user, sending in html and text formats."
                                );
                            VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject . ' - admin example html format', 
                                    nl2br($body) . ($includeSettings ? "<br/><br/>" . $sendToAll->makeHtml($arwRec) : ""), 
                                    'html'
                                ); 
                            VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject . ' - admin example text format', 
                                    strip_tags($body) . ($includeSettings ? "\r\n\r\n" . $sendToAll->makeText($arwRec) : ""), 
                                    'plaintext'
                                ); 

                        } else {

                            if ($arwRec['email_format'] == 'html') {
                                VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject, 
                                    nl2br($body) . ($includeSettings ? "<br/><br/>" . $sendToAll->makeHtml($arwRec) : ""), 
                                    'html'
                                ); 
                            } else { // Text format
                                VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject, 
                                    strip_tags($body) . ($includeSettings ? "\r\n\r\n" . $sendToAll->makeText($arwRec) : ""), 
                                    'plaintext'
                                ); 
                            } 
                        }
                    }
                }
                VnsMailer::execSend($this);

            } else if ($config->application->mode == "production" && $confirmFullSend) { // Full send
                            
                $this->flash->success("Preparing to send to any users that did not already receive this subject."); 
                
                $logger->info(
                        " Send To All - Preparing to send to any users that did not already receive this subject."
                    );
                
                $batchNum = $startingBatch;
                $maxBatch = ceil($arwTotal / ActiveRecipientsWorkspace::$batchSize);
                while ($batchNum <= $maxBatch) {
                    
                    $batchPct = round($batchNum / $maxBatch, 2) * 100;
                    $logger->info(" Send To All - Getting Batch $batchNum of $maxBatch ($batchPct %)");
                    $emails = ActiveRecipientsWorkspace::getBatch($batchNum);
                    foreach ($emails as $email) {

                        if (!VnsMailer::isEmailFormatOk($email)) {
                            $this->flash->error("Email, $email, is invalid format, SKIPPING");
                            continue;
                        }
                        
                        if (Mailman::getTotalForEmailAndSubject($email, $subject) > 0) {
                            $recentSkippedTotal++;
                            continue;
                        }
                        
                        if ($sendToAll->hasRecent($email, $ageFilterDays)) {
                            $recentSkippedTotal++;
                            continue;
                        }
                        $arwRec = ActiveRecipientsWorkspace::getByEmail($email);
                        if (!$arwRec || !$arwRec['address_id'] || !is_numeric($arwRec['address_id'])) {
                            $this->flash->error("Email $email is not a VNS user, not sending.");
                            continue;
                        }
                        $sendingTotal++;
                        if (!$dryRun) { 

                            if ($arwRec['email_format'] == 'html') {
                                VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject, 
                                    nl2br($body) . ($includeSettings ? "<br/><br/>" . $sendToAll->makeHtml($arwRec) : ""), 
                                    'html'
                                ); 
                            } else { // Text format
                                VnsMailer::addOneToBatch(
                                    $this, $arwRec, 
                                    $subject, 
                                    strip_tags($body) . ($includeSettings ? "\r\n\r\n" . $sendToAll->makeText($arwRec) : ""), 
                                    'plaintext'
                                ); 
                            }
                        }
                    }
                    if (count($emails) < ActiveRecipientsWorkspace::$batchSize) { break; } // We're done.
                    $logger->info(" Send To All - Sending Batch $batchNum");
                    VnsMailer::execSend($this);
                    sleep(5);
                    $batchNum++;
                }
                VnsMailer::execSend($this);
                
            } else {
                
                $this->flash->error("Did not send anything because confirm full send was not checked.");
            }
            
            if ($sendingTotal > 0) { 
                $this->flash->success(
                        "Sending/Sent to $sendingTotal addresses - if subject + recipient not already present."
                        ); 
            }
            if ($recentSkippedTotal > 0) { 
                $logger->info(" Send To All - Skipped $recentSkippedTotal, they have recently received a notice.");
                $this->flash->success("Skipped $recentSkippedTotal because they have recently received a notice."); 
            }
            if ($dryRun) {
                $logger->info(" Send To All - Did not actually send any messages because dry run was checked.");
                $this->flash->error("Did not actually send any messages because dry run was checked.");
            }
            
            $logger->info(" Send To All - Done Sending");
            $this->flash->success("Done Sending"); 
        }

        $this->view->subject = $subject;
        $this->view->body = $body;
        $this->view->recipients = $recipients;
        $this->view->includeSettings = $includeSettings;
        $this->view->ageFilterDays = $ageFilterDays;
        $this->view->recentSkippedTotal = $recentSkippedTotal;
        $this->view->sendingTotal = $sendingTotal;
        $this->view->dryRun = $dryRun;
        $this->view->logfile = $config->application->logsDir . 'vns.log';
        $this->view->startingBatch = $startingBatch;
    }
}