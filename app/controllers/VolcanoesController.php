<?php

namespace VHP\Vns\Controllers;

use VHP\Vns\Models\MailVolcpref;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\ActiveRecipientsWorkspace;
use VHP\Vns\Models\hans\Volcano;
use VHP\Vns\Models\hans\Observatory;
use VHP\Vns\Models\hans\CurrentCodes;
use VHP\Vns\Models\RawSql;

// TODO: when going to all_OBS, remove any other volcanoes for that observatory (leaving is a bug factory)

class VolcanoesController extends ControllerBase {

    public function indexAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $volcprefs = MailVolcpref::getMyVolcanoes($this->session->get("userid"));
        $this->view->volcprefs = $volcprefs;
        
        $prefsData = [];
        foreach ($volcprefs as $volcpref) {
            $prefsData[] = [
                "volcpref_id" => $volcpref->volcpref_id,
                "obs_abbr" => $volcpref->obs_abbr,
                "volcano_cd" => $volcpref->volcano_cd,
                "volcano_name" => $volcpref->volcano_name,
                "vnum" => $volcpref->vnum,
                "region" => $volcpref->region,
                "alert_level" => $volcpref->alert_level,
                "color_code" => $volcpref->color_code,
                "hzd_qual" => $volcpref->hzd_qual,
                "aviat_qual" => $volcpref->aviat_qual
            ];
        }
        $this->view->prefsData = json_encode($prefsData);

        $volcprefsAll = MailVolcpref::getMyAll($this->session->get("userid"));
        $this->view->volcprefsAll = $volcprefsAll;
        $allPrefsData = [];
        foreach ($volcprefsAll as $volcprefAll) {
            $allPrefsData[] = [
                "volcpref_id" => $volcprefAll->volcpref_id,
                "obs_abbr" => $volcprefAll->obs_abbr,
                "volcano_cd" => $volcprefAll->volcano_cd,
                "obs_fullname" => $volcprefAll->vpref,
                "alert_level" => $volcprefAll->alert_level,
                "color_code" => $volcprefAll->color_code,
                "hzd_qual" => $volcprefAll->hzd_qual,
                "aviat_qual" => $volcprefAll->aviat_qual
            ];
        }
        $this->view->allPrefsData = json_encode($allPrefsData);

        $this->view->regions = Volcano::selectRegions();
        $this->view->observatories = Observatory::find(["order" => "UPPER(obs_fullname)"]);
        $this->view->volcanoes = Volcano::find(["order" => "UPPER(volcano_name), observatory_id"]);
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");

    }

    public function regionAction($region) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $this->view->region = $region;
        $this->view->volcprefs = MailVolcpref::getMyVolcanoes($this->session->get("userid"));
        $this->view->volcanoes = Volcano::find(array(
                    "conditions" => "region = ?1",
                    "bind" => array(1 => $region),
                    "order" => "UPPER(volcano_name)"
                ));

        $this->view->observatories = Observatory::find(array("order" => "UPPER(obs_fullname)"));
        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    public function addAction($volcano_cd = null) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if (!$volcano_cd && $this->request->isPost()) {
            $volcano_cd = $this->request->getPost("volcano_cd");
        }

        $msg = $this->_addVolcano($volcano_cd);
        foreach (MailAddress::getUserEmails($this->session->get("userid")) as $maRec) {
            ActiveRecipientsWorkspace::refreshARW($maRec->email);
        }
        $this->flash->success($msg);
        return $this->response->redirect("volcanoes");
    }

    public function addregionAction($region) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $msg = '';

        $volcanoes = Volcano::find(array(
                    "columns" => "volcano_cd",
                    "conditions" => "region = ?1",
                    "bind" => array(1 => $region),
                    "order" => "UPPER(volcano_name)"
        ));
        foreach ($volcanoes as $volcano) {
            $this->_addVolcano($volcano->volcano_cd) . ' ';
        }
        foreach (MailAddress::getUserEmails($this->session->get("userid")) as $maRec) {
            ActiveRecipientsWorkspace::refreshARW($maRec->email);
        }
        $this->flash->success("Added all volcanoes in the $region region.");
        return $this->response->redirect("volcanoes");
    }

    public function addobservatoryAction($obs_abbr) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        // See if user already has a volcpref record.
        $volcpref = MailVolcpref::findFirst([
                    "conditions" => "user_id = ?1 AND volcano_cd = ?2",
                    "bind" => [1 => $this->session->get("userid"), 2 => 'all_' . $obs_abbr]
        ]);

        // No volcpref, so add one.
        if (!$volcpref) {

            // Purge any single volcano selections for this observatory.
            RawSql::execUpdate(
                    "DELETE FROM mail_volcpref WHERE user_id = ? AND obs_abbr = ?",
                    [$this->session->get("userid"), $obs_abbr]
                );

            $volcpref = new MailVolcpref();
            $volcpref->user_id = $this->session->get("userid");
            $volcpref->obs_abbr = $obs_abbr;
            $volcpref->volcano_cd = 'all_' . $obs_abbr;
            if ($volcpref->save()) {

                $obs = Observatory::findFirst([
                            'conditions' => 'obs_abbr = :obs_abbr:', 'bind' => ['obs_abbr' => $obs_abbr]
                ]);
                foreach (MailAddress::getUserEmails($volcpref->user_id) as $maRec) {
                    ActiveRecipientsWorkspace::refreshARW($maRec->email);
                }
                $this->flash->success($obs->obs_fullname . ' added.');
                
            } else {

                foreach ($volcpref->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        return $this->response->redirect("volcanoes");
    }

    public function editprefAction($volcano_cd = null) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($this->request->isPost()) {

            $volcano_cd = $this->request->getPost("volcano_cd");

            $volcpref = MailVolcpref::findFirst(array(
                        "conditions" => "user_id = ?1 AND volcano_cd = ?2",
                        "bind" => array(1 => $this->session->get("userid"), 2 => $volcano_cd)
            ));

            $volcpref->alert_level = $this->request->getPost("alert_level");
            $volcpref->color_code = $this->request->getPost("color_code");
            $volcpref->hzd_qual = $this->request->getPost("hzd_qual");
            $volcpref->aviat_qual = $this->request->getPost("aviat_qual");

            if ($volcpref->save()) {
                $this->flash->success("Changes saved.");
                foreach (MailAddress::getUserEmails($volcpref->user_id) as $maRec) {
                    ActiveRecipientsWorkspace::refreshARW($maRec->email);
                }
            } else {
                foreach ($volcpref->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }

            return $this->response->redirect("volcanoes/editpref/" . $volcano_cd);
        } else {

            $volcpref = MailVolcpref::findFirst(array(
                        "conditions" => "user_id = ?1 AND volcano_cd = ?2",
                        "bind" => array(1 => $this->session->get("userid"), 2 => $volcano_cd)
                    ));
        }

        $this->view->volcpref = $volcpref;

        if (substr($volcpref->volcano_cd, 0, 4) == 'all_') {

            $obs = Observatory::findFirst(array(
                        "conditions" => "obs_abbr = ?1",
                        "bind" => array(1 => substr($volcano_cd, 4))
                    ));

            $volcano_name = $obs->obs_fullname;
        } else {

            $volcano_name = Volcano::findFirst(array(
                        "conditions" => "volcano_cd = ?1",
                        "bind" => array(1 => $volcano_cd)
                    ))->volcano_name;
        }

        $this->view->volcano_name = $volcano_name;
        $this->view->aviat_qual_Options = MailVolcpref::getAviatQual();
        $this->view->hzd_qual_Options = MailVolcpref::getHzdQual();
        $this->view->alert_level_Options = MailVolcpref::getAlertLevels();
        $this->view->color_code_Options = MailVolcpref::getColorCodes();

        \Phalcon\Tag::setDefault("volcpref_id", $volcpref->volcpref_id);
        \Phalcon\Tag::setDefault("obs_abbr", $volcpref->obs_abbr);
        \Phalcon\Tag::setDefault("volcano_cd", $volcpref->volcano_cd);
        \Phalcon\Tag::setDefault("alert_level", $volcpref->alert_level);
        \Phalcon\Tag::setDefault("color_code", $volcpref->color_code);
        \Phalcon\Tag::setDefault("hzd_qual", $volcpref->hzd_qual);
        \Phalcon\Tag::setDefault("aviat_qual", $volcpref->aviat_qual);

        $volcano = Volcano::findFirst(array(
                    'conditions' => 'volcano_cd = ?1',
                    'bind' => array(1 => $volcano_cd)
        ));
        $this->view->currentCodes = CurrentCodes::findFirst(array(
                    "conditions" => "volcano_id = ?1",
                    "bind" => array(1 => $volcano->volcano_id)
        ));

        $this->view->left_menu = $this->rendering_view->render("templates/left_menu");
    }

    /**
     * Deletes all volcpref records for a user.
     */
    public function deleteAllAction() {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        $volcprefs = MailVolcpref::find(array(
                    "conditions" => "user_id = ?1",
                    "bind" => array(1 => $this->session->get("userid"))
                ));

        $totalRemoved = 0;
        $msgs = '';
        foreach ($volcprefs as $volcpref) {

            if (substr($volcpref->volcano_cd, 0, 4) == 'all_') {

                $obs = Observatory::findFirst(array(
                            "conditions" => "obs_abbr = ?1",
                            "bind" => array(1 => substr($volcpref->volcano_cd, 4))
                        ));

                $msg = $obs->obs_fullname . ' removed. ';
            } else {

                $msg = Volcano::findFirst(array(
                            "conditions" => "volcano_cd = ?1",
                            "bind" => array(1 => $volcpref->volcano_cd)
                        ))->volcano_name . ' removed. ';
            }

            if ($volcpref->delete()) {
                $totalRemoved = $totalRemoved + 1;
                $msgs .= $msg;
            } else {
                foreach ($volcpref->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

        foreach (MailAddress::getUserEmails($this->session->get("userid")) as $maRec) {
            ActiveRecipientsWorkspace::refreshARW($maRec->email);
        }

        if ($totalRemoved <= 1) {
            $this->flash->success($msgs);
        } else {
            $this->flash->success($totalRemoved . ' volcanoes removed.');
        }

        return $this->response->redirect("volcanoes");
    }

    /**
     * Removes a volcano preference.
     */
    public function deleteAction($volcano_cd) {

        if (!$this->session->get("userid")) {
            $this->flash->error("Please login.");
            return $this->response->redirect("/");
        }

        if ($volcano_cd) {

            $volcprefs = MailVolcpref::find(array(
                        "conditions" => "volcano_cd = ?1 AND user_id = ?2",
                        "bind" => array(1 => $volcano_cd, 2 => $this->session->get("userid"))
                    ));

            if (substr($volcano_cd, 0, 4) == 'all_') {

                $obs = Observatory::findFirst(array(
                            "conditions" => "obs_abbr = ?1",
                            "bind" => array(1 => substr($volcano_cd, 4))
                        ));

                $msg = $obs->obs_fullname . ' removed. ';
            } else {

                $msg = Volcano::findFirst(array(
                            "conditions" => "volcano_cd = ?1",
                            "bind" => array(1 => $volcano_cd)
                        ))->volcano_name . ' removed. ';
            }

            foreach ($volcprefs as $volcpref) {
                if ($volcpref->delete()) {
                    $this->flash->success($msg);
                } else {
                    foreach ($volcpref->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                }
            }
        }

        foreach (MailAddress::getUserEmails($this->session->get("userid")) as $maRec) {
            ActiveRecipientsWorkspace::refreshARW($maRec->email);
        }

        return $this->response->redirect("volcanoes");
    }

    /**
     * Used to add a single volcano preference or repeatedly when adding a region.
     */
    private function _addVolcano($volcano_cd) {

        $volc = Volcano::findFirst(array("conditions" => "volcano_cd = ?1", "bind" => array(1 => $volcano_cd)));

        if (!$volc) {
            $this->flash->error("Volcano not found, nothing to add.");
            return;
        }

        // See if user already has a volcpref record.
        $volcpref = MailVolcpref::findFirst(array(
                    "conditions" => "user_id = ?1 AND volcano_cd = ?2",
                    "bind" => array(1 => $this->session->get("userid"), 2 => $volcano_cd)
                ));

        $msg = $volc->volcano_name . ' already selected.';

        // No volcpref, so add one.
        if (!$volcpref) {

            $volcpref = new MailVolcpref();
            $volcpref->user_id = $this->session->get("userid");
            $volcpref->obs_abbr = $volc->observatory->obs_abbr;
            $volcpref->volcano_cd = $volc->volcano_cd;
            if ($volcpref->save()) {
                $msg = $volc->volcano_name . " added.";
            } else {
                foreach ($volcpref->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

        return $msg;
    }

}
