<?php

namespace VHP\Vns\Library;

class Utils {

    const VN_ALLCAPS = 1;
    const VN_LOWER = 2;
    const VN_INITCAP = 3;

    
    public static function appendToVolcanoName($volcano_name, $CAPITALIZATION_SCHEME = 3) {

        if (!isset($volcano_name) || strlen(trim($volcano_name)) == 0) { return ''; }
        
        // If volcano name contains the word complex just reutrn the volcano name. This is because
        // some things like Atka Volcanic Complex should not have volcano apppended to them.
        if (strpos(strtoupper($volcano_name), 'COMPLEX') != false) { return $volcano_name; }
        
        // If volcano name contains the word volcano just reutrn the volcano name.
        if (strpos(strtoupper($volcano_name), 'VOLCANO') != false) { return $volcano_name; }
                
        switch($CAPITALIZATION_SCHEME) {
            case Utils::VN_ALLCAPS:
                if (trim(strtoupper($volcano_name)) == 'CASCADE RANGE') { return 'CASCADE RANGE VOLCANOES'; }
                else { return $volcano_name . ' VOLCANO'; }
            case Utils::VN_LOWER:
                if (trim(strtoupper($volcano_name)) == 'CASCADE RANGE') { return 'Cascade Range volcanoes'; }
                else { return $volcano_name . ' volcano'; }
            default:
                if (trim(strtoupper($volcano_name)) == 'CASCADE RANGE') { return 'Cascade Range Volcanoes'; }
                else { return $volcano_name . ' Volcano'; }
        }
    }
}