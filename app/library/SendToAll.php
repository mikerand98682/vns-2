<?php

namespace VHP\Vns\Library;

use VHP\Vns\Models\hans\Observatory;
use VHP\Vns\Models\hans_mailman\HansMailman;

class SendToAll {

    private $_oRecs;

    function __construct() {
        $this->_oRecs = Observatory::find(array(
                    "columns" => "observatory_id, obs_abbr, obs_fullname",
                    "order" => "UPPER(obs_fullname)"
        ));
    }

    public function hasRecent($email, $daysBack) {
        
        if ($daysBack == 'all' || !$daysBack || !is_numeric($daysBack)) { return false; }
        $timestamp = (int)$daysBack * 86400;
        return HansMailman::getTotalSentSinceTimestamp($email, $timestamp) > 0;
    }
    
    public function makeHtml($arwRec) {

        global $config;

        $body = "
            <br/><br/>
            <h3>VNS Account Information</h3>
            ";

        // NOTIFICATION TYPES //
        $body .= "<h4>My Notification Types</h4>";
        foreach ($arwRec['notice_types'] as $ntRec) {
            //$body .= $ntRec['notice_category'] . ' - ' . $ntRec['notice_type'] . '<br/>';
            $body .= $ntRec['notice_type'] . '<br/>';
         }

        // VOLCANOES //

        $body .= "<h4>My Volcanoes</h4>";
        
        foreach ($arwRec['volcanoes'] as $vRec) {
            
            $qualTxt = '';
            
            if ($vRec['qualifiers'] && isset($vRec['qualifiers']['hzd_qual_text'])) {
                $qualTxt .= ' ' . $vRec['qualifiers']['hzd_qual_text'] . ' ';
            }
            
            if ($vRec['qualifiers'] && isset($vRec['qualifiers']['aviat_qual_text'])) {
                $qualTxt .= ($qualTxt != '' ? ', ' : ' ') . $vRec['qualifiers']['aviat_qual_text'] . ' ';
            }

            if ($qualTxt != '') {
                $qualTxt = ', <small><i>' . trim($qualTxt) . '</i></small>';
            }
            
            if (str_starts_with($vRec['volcano_cd'], 'all_')) {
                foreach($this->_oRecs as $oRec) {
                    if ('all_' . $oRec->obs_abbr == $vRec['volcano_cd']) {
                        $body .= "All " . $oRec->obs_fullname . " Volcanoes"  . $qualTxt . "<br/>";
                    }
                }
            } else {
                $body .= $vRec['volcano_name'] . $qualTxt . '<br/>';
            }
        }

        // //
        return $body;
    }

    public function makeText($arwRec) {

        global $config;
        
        $body = "\r\nVNS ACCOUNT INFORMATION\r\n";

        // NOTIFICATION TYPES //
        $body .= "\r\n\r\n- My Notification Types\r\n\r\n";
        
        foreach ($arwRec['notice_types'] as $ntRec) {
            //$body .= $ntRec['notice_category'] . ' - ' . $ntRec['notice_type'] . "\r\n";
            $body .= $ntRec['notice_type'] . "\r\n";
        }

        // VOLCANOES //
        $body .= "\r\n\r\n- My Volcanoes\r\n\r\n";
        foreach ($arwRec['volcanoes'] as $vRec) {
            
            $qualTxt = '';
            
            if ($vRec['qualifiers'] && isset($vRec['qualifiers']['hzd_qual_text'])) {
                $qualTxt .= ' ' . $vRec['qualifiers']['hzd_qual_text'] . ' ';
            }
            
            if ($vRec['qualifiers'] && isset($vRec['qualifiers']['aviat_qual_text'])) {
                $qualTxt .= ($qualTxt != '' ? ', ' : ' ') . $vRec['qualifiers']['aviat_qual_text'] . ' ';
            }

            if ($qualTxt != '') {
                $qualTxt = ', ' . trim($qualTxt) . '.';
            }
            
            if (str_starts_with($vRec['volcano_cd'], 'all_')) {
                foreach($this->_oRecs as $oRec) {
                    if ('all_' . $oRec->obs_abbr == $vRec['volcano_cd']) {
                        $body .= "All " . $oRec->obs_fullname . " Volcanoes"  . $qualTxt . "\r\n";
                    }
                }
            } else {
                $body .= $vRec['volcano_name'] . $qualTxt . "\r\n";
            }
        }

        // //
        return $body;
    }

}
