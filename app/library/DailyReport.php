<?php

namespace VHP\Vns\Library;

use \Phalcon\Di\Injectable;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\hans\NoticeType;
use VHP\Vns\Library\Utils;

class DailyReport {

    public static function makeHtml($reportData) {

        global $config;

        $today = date('l, M j, Y');
        $thetime = date('H:i T');

        $body = "<h2>Elevated Volcanic Activity</h2>
            <h4>USGS Daily Elevated Volcano Status Summary as of $today at $thetime</h4>
            The following U.S. volcanoes are known to be above normal background (elevated unrest or eruptions) as of 
            the time stamped above. Volcanoes previously elevated are also included.<br/>
            Please see <a href='https://www.usgs.gov/programs/VHP/volcano-updates'>https://www.usgs.gov/programs/VHP/volcano-updates</a> for the latest information.<br/>
            Times are local in military format. Volcano Alert Levels & Aviation Color Codes are defined at 
            <a href='https://www.usgs.gov/natural-hazards/volcano-hazards/notifications'>https://www.usgs.gov/natural-hazards/volcano-hazards/notifications</a>.<br/>
            ";

        foreach ($reportData as $item) {

            $cc = $item->color_code;
            if ($cc == "GREEN") {
                $cc = "<span style='color:green'>GREEN</span>";
            } else if ($cc == "YELLOW") {
                $cc = "<span style='color:gold'>YELLOW</span>";
            } else if ($cc == "ORANGE") {
                $cc = "<span style='color:orange'>ORANGE</span>";
            } else if ($cc == "RED") {
                $cc = "<span style='color:red'>RED</span>";
            }

            $href = "https://volcanoes.usgs.gov/hans2/view/notice/" . $item->notice_identifier;

            $volcano_name_appended = Utils::appendToVolcanoName($item->volcano_name, Utils::VN_INITCAP);

            $body .= "<hr/>
                <b>$volcano_name_appended</b> Alert Level = <b>$item->alert_level</b>. Aviation Color Code = <b>$cc</b>. Issued $item->newdate, $item->newtime<br/>
                <small>(Change to current status occurred on $item->curr_newdate, $item->curr_newtime from Volcano Alert Level $item->prev_alert_level and Aviation Color Code $item->prev_color_code )</small><br/>
                <b>$item->synopsis</b><br/>
                For more information see <a href='$href'>$href</a>
                ";
        }

        return $body;
    }

    public static function makeText($reportData) {

        global $config;

        $today = date('l, M j, Y');
        $thetime = date('H:i T');

        $body = "USGS Daily Elevated Volcano Status Summary as of $today at $thetime\r\n\r\n";
        $body .= "The following U.S. volcanoes are known to be above normal background (elevated unrest or eruptions) as of the time stamped above.\r\n";
        $body .= "Volcanoes previously elevated are also included.\r\n";
        $body .= "Please see https://www.usgs.gov/programs/VHP/volcano-updates for the latest information.\r\n";
        $body .= "Times are local in military format. Volcano Alert Levels & Aviation Color Codes are defined at https://www.usgs.gov/natural-hazards/volcano-hazards/notifications.\r\n";

        foreach ($reportData as $item) {

            $volcano_name_appended = Utils::appendToVolcanoName($item->volcano_name, Utils::VN_INITCAP);

            $href = "https://volcanoes.usgs.gov/hans-public/notice/" . $item->notice_identifier;

            $body .= "---\r\n";
            $body .= "$volcano_name_appended Alert Level = $item->alert_level. Aviation Color Code = $item->color_code. Issued $item->newdate, $item->newtime\r\n";
            $body .= "(Change to current status occurred on $item->curr_newdate, $item->curr_newtime from Volcano Alert Level $item->prev_alert_level and Aviation Color Code $item->prev_color_code )\r\n";
            $body .= $item->synopsis . "\r\n";
            $body .= "For more information see " . $href . "\r\n";
        }

        return $body;
    }

    public static function makeCell($reportData) {

        $today = date('l, M j, Y');
        $thetime = date('H:i T');

        $body = "USGS Daily Elevated Volcano Status Summary as of $today at $thetime\r\n";

        foreach ($reportData as $item) {

            $volcano_name_appended = Utils::appendToVolcanoName($item->volcano_name, Utils::VN_INITCAP);

            $body .= "-\r\n";
            $body .= "$volcano_name_appended Alert Level = $item->alert_level. Aviation Color Code = $item->color_code. Issued $item->newdate, $item->newtime\r\n";
            $body .= "$item->synopsis\r\n";
        }

        return $body;
    }

    // Update the daily_summary_sent_ddtm value so repeated runs will not spam recipients.
    //
    // Updates active and confirmed email addresses where receive_daily_summary_ind = y
    // and users have not received a report in last 23 hours, sets the daily_summary_sent_ddtm value to NOW().
    public static function resetRecepients() {

        $sql = "
            UPDATE
                mail_address 
            SET
                daily_summary_sent_dttm = NOW()
            WHERE 
                (
                    daily_summary_sent_dttm IS NULL 
                    OR 
                    daily_summary_sent_dttm <= DATE_ADD(NOW(), INTERVAL -23 HOUR)
                )
                AND active = 'y'
                AND confirmed = 'y'
                AND receive_daily_summary_ind = 'y'
            ";

        $ma = new MailAddress();
        $ma->getWriteConnection()->execute($sql);
    }

    // Select active and confirmed email addresses where receive_daily_summary_ind = y
    // and users have not received a report in last 23 hours.
    // Once reports are sent, resetRecipients must be run to update the daily_summary_sent_ddtm value
    // so repeated runs will not spam recipients.
    public static function getRecipients() {

        $sql = "
            SELECT
                ma.email_format, ma.email, mu.user_fullname, ma.address_id, ma.unsubscribe_key
            FROM
                mail_address ma, mail_user mu
            WHERE 
                (
                    ma.daily_summary_sent_dttm IS NULL 
                    OR 
                    ma.daily_summary_sent_dttm <= DATE_ADD(NOW(), INTERVAL -23 HOUR)
                )
                AND ma.active                    = 'y'
                AND ma.confirmed                 = 'y'
				AND ma.user_id                   = mu.user_id
                AND ma.receive_daily_summary_ind = 'y'
                AND LOWER(TRIM(ma.email)) NOT IN (SELECT LOWER(TRIM(email)) FROM vns2024.vns_user) 
            ORDER BY 
                ma.email_format, UPPER(ma.email)
            ";

        $ma = new MailAddress();
        return new Resultset(null, $ma, $ma->getReadConnection()->query($sql));
    }

    public static function getReportData() {

        return NoticeType::getActivityReport();
    }

}
