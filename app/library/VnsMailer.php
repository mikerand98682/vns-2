<?php

namespace VHP\Vns\Library;

use VHP\Vns\Models\mailman\Mailman;
use VHP\Vns\Models\MailUser;

class VnsMailer {

    public static function isEmailFormatOk($email) {
        
        global $config;
        $exePath = $config->application->javaEmailValidatorExe;
        if (file_exists($exePath) && is_executable($exePath)) {
            exec("$exePath $email", $msgs, $rc);
            return $rc == 0;
        } else {
            //global $logger;
            //$logger->error("ERROR, VnsMailer.php: did not find $exePath or is not executable!");
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
    }
    
    public static function sendSingle($controller, $recipient, $recipientName, $subject, $body, $type) {

        if ($type == 'plaintext') { $type = 'text'; }
        else if ($type == 'short (cell)') { $type = 'sms'; }

        // Get recipients.....
        if ($controller->config->application->mode != "production") {
            $recipient = $controller->config->mail->devemail;
        }

        $mailRec = new Mailman();
        $mailRec->sender         = $controller->config->mail->helpEmail;
        $mailRec->sender_name    = "USGS VNS";
        $mailRec->subject        = $subject;
        $mailRec->body           = $body;
        $mailRec->type           = $type;
        $mailRec->pending_ind    = "N";
        $mailRec->sent_ind       = "N";
        $mailRec->error_ind      = "N";
        $mailRec->recipient      = $recipient;
        $mailRec->recipient_name = $recipientName;

        if ($mailRec->save()) {
            exec($controller->config->mail->mailmanExe . " > /dev/null &");
            //$controller->flash->success("Mail sent to " . $recipient);
        } else {
            foreach ($mailRec->getMessages() as $message) {
                $controller->flash->error("ERROR trying to send $subject to " . $recipient . ": " . $message);
            }
        }
    }

    // Add a batch of emails without sending. Used for repeated addBatch calls, then call execSend to send.
    public static function addBatch($controller, $recipients, $subject, $body, $type) {
        
        if ($type == 'plaintext') { $type = 'text'; }
        else if ($type == 'short (cell)') { $type = 'sms'; }
        
        foreach($recipients as $recipient) {

            $unsubscribe_url = null;
            $unsubscribeA = '';
            $unsubscribeText = '';
            if (isset($recipient['address_id']) && is_numeric($recipient['address_id'])) {
                
                $unsubscribe_url = $controller->config->application->publicUrl . 
                        $controller->config->application->baseUrl . 'subscribe/listUnsubscribe' . 
                        '?aid=' . $recipient['address_id'] . '&usk=' . $recipient['unsubscribe_key'];
                $unsubscribeA = ' - <a href="' . $unsubscribe_url . '">UNSUBSCRIBE NOW</a>';
                $unsubscribeText = ' - UNSUBSCRIBE HERE: ' . $unsubscribe_url;
            }

            $toContent = '';
            if ($type == 'html') {
                $toContent = "<br/><br/><small>- Sent to " . $recipient['email'] . " from VNS.$unsubscribeA</small>";
            } else {
                $toContent = "\r\n\r\n- Sent to " . $recipient['email'] . " from VNS.$unsubscribeText";
            }

            if ($controller->config->application->mode != "production") {
                $recipient = ['email' => $controller->config->mail->devemail, 'user_fullname' => 'VNS DEV'];
                if ($type == 'html') {
                    $toContent .= "<br/><br/><small>- NOT PRODUCTION, sent to " . $recipient['email'] . " instead.</small>";
                } else {
                    $toContent .= "\r\n\r\n- NOT PRODUCTION,  sent to " . $recipient['email'] . " instead.";
                }
            }

            $mailRec = new Mailman();
            $mailRec->sender             = $controller->config->mail->helpEmail;
            $mailRec->sender_name        = "USGS VNS";
            $mailRec->subject            = $subject;
            $mailRec->body               = $body . $toContent;
            $mailRec->type               = $type;
            $mailRec->pending_ind        = "N";
            $mailRec->sent_ind           = "N";
            $mailRec->error_ind          = "N";
            $mailRec->recipient          = $recipient['email'];
            $mailRec->recipient_name     = $recipient['user_fullname'];
            $mailRec->unsubscribe_mailto = $controller->config->mail->helpEmail;
            $mailRec->unsubscribe_url    = $unsubscribe_url;

            if ($mailRec->save() == false) {
                foreach ($mailRec->getMessages() as $message) {
                    $controller->flash->error("ERROR trying to send $subject to " . $recipient['email'] . ": " . $message);
                }
            }
        }
    }

    // Add an email without sending. Used for repeated addBatch calls, then call execSend to send.
    // This function does not consider production/development/maintenance modes.
    public static function addOneToBatch($controller, $arwRec, $subject, $body, $type) {
        
        global $config;
        
        if ($type == 'plaintext') { $type = 'text'; }
        else if ($type == 'short (cell)') { $type = 'sms'; }
        
        $unsubscribe_url = null;
        $unsubscribeA = '';
        $unsubscribeText = '';
        if (isset($arwRec['address_id']) && is_numeric($arwRec['address_id'])) {

            $unsubscribe_url = $config->application->publicUrl . 
                    $config->application->baseUrl . 'subscribe/listUnsubscribe' . 
                    '?aid=' . $arwRec['address_id'] . '&usk=' . $arwRec['unsubscribe_key'];
            $unsubscribeA = ' - <a href="' . $unsubscribe_url . '">UNSUBSCRIBE NOW</a>';
            $unsubscribeText = ' - UNSUBSCRIBE HERE: ' . $unsubscribe_url;
        }

        $vnsPublicUrl = $config->application->publicUrl . $config->application->baseUrl;
        
        $toContent = '';
        if ($type == 'html') {
            $toContent = "
                <br/><br/>
                <small>- <a href='$vnsPublicUrl'>Log into my VNS account here</a> 
                    with username " . $arwRec['username'] . ".</small>
                <br/>
                <small>- Sent to " . $arwRec['email'] . " from VNS.$unsubscribeA</small>
                ";
        } else {
            $toContent = "\r\n" . 
                    "\r\n- Log into my VNS account here: $vnsPublicUrl with username " . $arwRec['username'] . "." .
                    "\r\n- Sent to " . $arwRec['email'] . " from VNS.$unsubscribeText";
        }

        $mailRec = new Mailman();
        $mailRec->sender             = $config->mail->helpEmail;
        $mailRec->sender_name        = "USGS VNS";
        $mailRec->subject            = $subject;
        $mailRec->body               = $body . $toContent;
        $mailRec->type               = $type;
        $mailRec->pending_ind        = "N";
        $mailRec->sent_ind           = "N";
        $mailRec->error_ind          = "N";
        $mailRec->recipient          = $arwRec['email'];
        $mailRec->recipient_name     = $arwRec['user_fullname'];
        $mailRec->unsubscribe_mailto = $config->mail->helpEmail;
        $mailRec->unsubscribe_url    = $unsubscribe_url;

        if ($mailRec->save() == false) {
            foreach ($mailRec->getMessages() as $message) {
                $controller->flash->error("ERROR trying to send $subject to " . $arwRec['email'] . ": " . $message);
            }
        }   
    }
    
    // This will fire the mailman script, if pending email exists it will get sent.
    // This is intended to be used after the addBatch function.
    public static function execSend($controller) {
        
         exec($controller->config->mail->mailmanExe . " > /dev/null &");
    }
    
    public static function sendConfirmation($controller, $address, $isFirstEmail = false) {

        // TODO: Validate Address Format

        if (!$address->confirmation_code || !is_numeric($address->confirmation_code)) {
            $address->confirmation_code = mt_rand(1000, 9999);
            if (!$address->save()) {
                foreach ($address->getMessages() as $message) { $controller->flash->error($message); }
            }
        }

        $vnsUrl = $controller->config->application->publicUrl . $controller->config->application->baseUrl;

        $link = $vnsUrl . "confirm?address_id=" . $address->address_id . "&confirmation_code=" . 
                                $address->confirmation_code;

        if ($address->email_format == 'html') {
            $body = 
                "<b>Volcano Notification Service Address Confirmation</b><br/><br/>" . 
                "<b>Confirm your email address <a href='$link'>by clicking here</a></b>.<br/><br/>" . 
                "<b>Or</b> you may enter your confirmation code, $address->confirmation_code, here: " . 
                "<a href='" . $vnsUrl . "email'>" . $vnsUrl . "email</a> (after logging into VNS).<br/>" .
                "Login with your username, $address->username, and the password you chose when you created your " . 
                "account.";
        } else {
            $body = 
                "Volcano Notification Service Address Confirmation\r\n\r\n" . 
                "Confirm your email address here: $link\r\n\r\n" . 
                "OR you may enter your confirmation code, $address->confirmation_code, here: " . 
                $vnsUrl . "email (after logging into VNS).\r\n" . 
                "Login with your username, $address->username, and the password you chose when you created your " . 
                "account.";
        }
        
        if ($isFirstEmail) {
            $body .= "
                <br/>
                Your account has been set up with default settings. You will receive email from all volcano 
                observatories when they issue a status report, information statement or event response. These types of 
                notices are generally issued during volcanic unrest.
                <br/><br/>
                <b>If you desire</b>, you may modify your VNS settings.
                <br/><br/>
                In addition to information statements and event responses, volcano observatories issue scheduled 
                updates. Choose <b>My Notification Types</b> from the left menu after logging in to change the types of
                observatory notifications you receive.
                <br/><br/>
                You will receive notices for any U.S. volcanoes included in your selected notification types. Select 
                <b>My Volcanoes</b> from the left menu after logging in to change which volcanoes you receive notices 
                for. You can choose to receive notices for specific volcanoes, regions or observatories.
                <br/><br/>
                <i>You must have a combination of notification types and volcanoes selected to receive notices from
                observatories.</i>
                <br/><br/>
                You can also choose to receive a <b>Daily Report</b>, which provides a summary of elevated volcanoes.
                Receive the daily report by selecting <b>My Email Addresses</b> from the left menu, then in the 
                <i>Current Addresses</i> box, select <i>Edit Address</i>, then select the Receive option from the 
                <i>Daily Summary Report</i> selector. Users with multiple email addresses can choose which receives the 
                daily summary report.
                ";
        }
        
        $user = MailUser::findFirst($address->user_id);
        
        if ($isFirstEmail) {
            VnsMailer::sendSingle($controller, $address->email, $user->user_fullname, 
                                "Welcome to the Volcano Notification Service (VNS)", $body, $address->email_format);
            $controller->flash->success("Welcome message sent to " . $address->email);
        } else {
            VnsMailer::sendSingle($controller, $address->email, $user->user_fullname, 
                                "Volcano Notification Service (VNS) Confirmation Code", $body, $address->email_format);
            $controller->flash->success("Confirmation message sent to " . $address->email);
        }
    }
}