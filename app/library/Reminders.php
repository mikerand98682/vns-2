<?php

namespace VHP\Vns\Library;

use \Phalcon\Di\Injectable;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use VHP\Vns\Models\MailUser;
use VHP\Vns\Models\MailAddress;
use VHP\Vns\Models\hans\NoticeType;
use VHP\Vns\Models\hans\Observatory;
use VHP\Vns\Models\hans\Volcano;

class Reminders {

    private $_ntRecs;
    private $_oRecs;
    private $_vRecs;

    function __construct() {

        $this->_ntRecs = NoticeType::find(array(
                    "conditions" => "type_deleted_ind = ?1",
                    "bind" => array(1 => "N"),
                    "order" => "'notice_category_sort_order', type_sort_order"
        ));

        $this->_oRecs = Observatory::find(array(
                    "columns" => "observatory_id, obs_abbr, obs_fullname",
                    "order" => "UPPER(obs_fullname)"
        ));

        $this->_vRecs = Volcano::find(array(
                    "columns" => "observatory_id, volcano_cd, volcano_name",
                    "order" => "observatory_id, UPPER(volcano_name)"
        ));
    }

    public function makeHtml($recipient, $muRec) {

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");

        $body = "
            <h2>USGS Volcano Notification Service (VNS) Reminder</h2>
            
            You have received this message because reminders are enabled for your VNS account. You do not need to
            respond to this message, it is simply for your information.
            <br/><br/>
            To modify your account please visit the Volcano Notification Service at 
            <a href='https://volcanoes.usgs.gov/vns2/'>https://volcanoes.usgs.gov/vns2/</a> and login with
            username <b>$recipient->username</b>. You may also recover account information there.
            
            <br/><br/>
            <h2>VNS Account Information</h4>
            ";

        // EMAIL ADDRESSES //
        $body .= "<h4>Email Addresses</h4>";
        $maRecs = MailAddress::find(array(
                    'conditions' => 'user_id = ?1',
                    'bind' => [1 => $recipient->user_id],
                    'order' => 'UPPER(email)'
        ));
        foreach ($maRecs as $maRec) {

            $unsubscribe_url = $config->application->publicUrl .
                    $config->application->baseUrl . 'subscribe/listUnsubscribe' .
                    '?aid=' . $maRec->address_id . '&usk=' . $maRec->unsubscribe_key;

            $unsubscribeA = ' - <a href="' . $unsubscribe_url . '">UNSUBSCRIBE NOW</a>';

            $body .= $maRec->email . ($maRec->confirmed == 'y' && $maRec->active == 'y' ? $unsubscribeA : '') .
                    ', ' . $maRec->email_format . ' format. ' .
                    ($maRec->confirmed == 'y' ? '' : ' - Not confirmed. ') .
                    ($maRec->active == 'y' ? '' : ' - Not active. ') .
                    ($maRec->receive_daily_summary_ind == 'y' ? ' - Receives daily summary report. ' : '') .
                    '<br/>';
        }

        // NOTIFICATION TYPES //
        $body .= "<h4>My Notification Types</h4>";
        foreach ($this->_ntRecs as $ntRec) {
            $hasNt = false;
            foreach ($muRec->mail_type as $mtRec) {
                if ($mtRec->hans_type_id == $ntRec->legacy_type_id) {
                    $hasNt = true;
                }
            }
            if ($hasNt) {
                $body .= $ntRec->notice_category . ' - ' . $ntRec->notice_type . '<br/>';
            }
        }

        // VOLCANOES //
        $adtlQualMsg = ', limited by color code and/or alert level.';
        $body .= "<h4>My Volcanoes</h4>";
        foreach ($this->_oRecs as $oRec) {

            $hasAllObs = false;
            $adtlQual = false;
            foreach ($muRec->mail_volcpref as $mvpRec) {
                if ($mvpRec->volcano_cd == 'all_' . $oRec->obs_abbr) {
                    $hasAllObs = true;
                    if ($mvpRec->hzd_qual != '1' || $mvpRec->aviat_qual != '1') {
                        $adtlQual = true;
                    }
                }
            }
            if ($hasAllObs) {
                $body .= $oRec->obs_fullname . ' - All Volcanoes' . ($adtlQual ? $adtlQualMsg : '') . '<br/>';
            } else {
                foreach ($this->_vRecs as $vRec) {
                    if ($vRec->observatory_id != $oRec->observatory_id) {
                        continue;
                    }
                    foreach ($muRec->mail_volcpref as $mvpRec) {
                        if ($mvpRec->volcano_cd == $vRec->volcano_cd) {
                            $adtlQual = false;
                            if ($mvpRec->hzd_qual != '1' || $mvpRec->aviat_qual != '1') {
                                $adtlQual = true;
                            }
                            $body .= $oRec->obs_fullname . ' - ' . $vRec->volcano_name .
                                    ($adtlQual ? $adtlQualMsg : '') . '<br/>';
                        }
                    }
                }
            }
        }

        // //
        return $body;
    }

    public function makeText($recipient, $muRec) {

        global $config;

        $body = "USGS VOLCANO NOTIFICATION SERVICE (VNS) REMINDER\r\n\r\n";
        $body .= "You have received this message because reminders are enabled for your VNS account. \r\n";
        $body .= "You do not need to respond to this message it is simply for your information. To modify \r\n";
        $body .= "your account please visit the Volcano Notification Service at https://volcanoes.usgs.gov/vns2/ \r\n";
        $body .= "and login with username $recipient->username. You may also recover account information there.\r\n";
        $body .= "\r\nVNS ACCOUNT INFORMATION\r\n";

        // EMAIL ADDRESSES //
        $body .= "\r\n- Email Addresses\r\n\r\n";
        $maRecs = MailAddress::find(array(
                    'conditions' => 'user_id = ?1 AND duplicate_ind = ?2',
                    'bind' => array(1 => $recipient->user_id, 2 => 'n'),
                    'order' => 'UPPER(email)'
        ));
        foreach ($maRecs as $maRec) {
            $body .= $maRec->email . ', ' . $maRec->email_format . ' format. ' .
                    ($maRec->confirmed == 'y' ? '' : ' - Not confirmed. ') .
                    ($maRec->active == 'y' ? '' : ' - Not active. ') .
                    ($maRec->receive_daily_summary_ind == 'y' ? ' - Receives daily summary report. ' : '') .
                    "\r\n";
        }

        // NOTIFICATION TYPES //
        $body .= "\r\n\r\n- My Notification Types\r\n\r\n";
        foreach ($this->_ntRecs as $ntRec) {
            $hasNt = false;
            foreach ($muRec->mail_type as $mtRec) {
                if ($mtRec->hans_type_id == $ntRec->legacy_type_id) {
                    $hasNt = true;
                }
            }
            if ($hasNt) {
                $body .= $ntRec->notice_category . ' - ' . $ntRec->notice_type . "\r\n";
            }
        }

        // VOLCANOES //
        $adtlQualMsg = ', limited by color code and/or alert level.';
        $body .= "\r\n\r\n- My Volcanoes\r\n\r\n";
        foreach ($this->_oRecs as $oRec) {

            $hasAllObs = false;
            $adtlQual = false;
            foreach ($muRec->mail_volcpref as $mvpRec) {
                if ($mvpRec->volcano_cd == 'all_' . $oRec->obs_abbr) {
                    $hasAllObs = true;
                    if ($mvpRec->hzd_qual != '1' || $mvpRec->aviat_qual != '1') {
                        $adtlQual = true;
                    }
                }
            }
            if ($hasAllObs) {
                $body .= $oRec->obs_fullname . ' - All Volcanoes' . ($adtlQual ? $adtlQualMsg : '') . "\r\n";
            } else {
                foreach ($this->_vRecs as $vRec) {
                    if ($vRec->observatory_id != $oRec->observatory_id) {
                        continue;
                    }
                    foreach ($muRec->mail_volcpref as $mvpRec) {
                        if ($mvpRec->volcano_cd == $vRec->volcano_cd) {
                            $adtlQual = false;
                            if ($mvpRec->hzd_qual != '1' || $mvpRec->aviat_qual != '1') {
                                $adtlQual = true;
                            }
                            $body .= $oRec->obs_fullname . ' - ' . $vRec->volcano_name .
                                    ($adtlQual ? $adtlQualMsg : '') . "\r\n";
                        }
                    }
                }
            }
        }

        // //
        return $body;
    }

    public static function getRecipients() {

        $sql = "
            SELECT
                mu.user_id, mu.user_fullname, mu.username, b.email, b.email_format
            FROM
                mail_user mu JOIN (
                    SELECT ma.user_id, ma.email, ma.email_format
                    FROM mail_address ma JOIN (
                                SELECT user_id, MIN(address_id) AS address_id 
                                FROM mail_address 
                                WHERE active = 'y' AND confirmed = 'y' AND duplicate_ind = 'n'
                                GROUP BY user_id
                            ) a ON ma.user_id = a.user_id AND ma.address_id = a.address_id
                ) b ON mu.user_id = b.user_id
            WHERE
                mu.send_reminder = 'y'
                AND TIMESTAMPDIFF(MONTH, IFNULL(mu.reminder_sent_dttm, mu.added), NOW()) > 6
                AND LOWER(TRIM(b.email)) NOT IN (SELECT LOWER(TRIM(email)) FROM vns2024.vns_user) 
            ORDER BY
                UPPER(mu.user_fullname) LIMIT 1000
            ";

        $mu = new MailUser();
        return new Resultset(null, $mu, $mu->getReadConnection()->query($sql));
    }

}
