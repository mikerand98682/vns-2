<?php

namespace VHP\Vns\Models\vns2024;

use Phalcon\Mvc\Model;
use VHP\Vns\Models\RawSql;

class VnsUser extends Model {

    public $vns_user_id, $email, $token, $token_datetime, $visited_datetime, $message_format, $notification_category, 
            $has_vona, $has_daily_summary, $daily_summary_sent_datetime, $obs_volc_cds_csv, $paused_datetime, 
            $paused_reason, $created_datetime, $reminder_sent_datetime;

    public function initialize() {

        $this->setSource("vns_user");
        $this->setSchema("vns2024");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function hasAccountInNewVns($email) {

        $total = RawSql::execMasterReadQueryOneRow("
                            SELECT IFNULL(COUNT(*),0) AS total FROM vns2024.vns_user
                            WHERE LOWER(TRIM(email)) = LOWER(TRIM(?))
                            ", [$email]
                        )->total;
        
        return $total > 0;
    }

}
