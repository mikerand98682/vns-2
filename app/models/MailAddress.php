<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class MailAddress extends Model {

    public $address_id, $user_id, $email, $email_format, $confirmed, $confirmation_code, $receive_daily_summary_ind,
            $daily_summary_sent_dttm, $duplicate_ind, $unsubscribe_key, $added, $active, $deactivated_reason_code,
            $deactivated_date;
    
    public $deactivatedMsg;

    public function initialize() {

        $this->setSource('mail_address');
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
        $this->belongsTo('user_id', 'VHP\Vns\Models\MailUser', 'user_id', ['alias' => 'mail_user']);
    }

    public static function getUserEmails($user_id) {

        return MailAddress::find(['conditions' => 'user_id = ?1', 'bind' => [1 => $user_id]]);
    }

    public static function getUser($user_id) {
        
        if (!isset($user_id) || !is_numeric($user_id)) { return []; }
                
        $recs = RawSql::execReadQuery(
                    "
                    SELECT 
                        address_id, user_id, email, email_format, confirmed, confirmation_code, 
                        receive_daily_summary_ind, daily_summary_sent_dttm, duplicate_ind, unsubscribe_key, 
                        added, active, deactivated_reason_code, deactivated_date 
                    FROM 
                        mail_address 
                    WHERE 
                        user_id = ? 
                    ORDER BY 
                        UPPER(TRIM(email))
                    ", [$user_id]
                );
        $data = [];
        foreach($recs as $row) {
            $data[] = (object)[
                'address_id'                => $row->address_id,
                'user_id'                   => $row->user_id,
                'email'                     => $row->email,
                'email_format'              => $row->email_format,
                'confirmed'                 => $row->confirmed,
                'confirmation_code'         => $row->confirmation_code,
                'receive_daily_summary_ind' => $row->receive_daily_summary_ind,
                'daily_summary_sent_dttm'   => $row->daily_summary_sent_dttm,
                'duplicate_ind'             => $row->duplicate_ind,
                'unsubscribe_key'           => $row->unsubscribe_key,
                'added'                     => $row->added,
                'active'                    => $row->active,
                'deactivated_reason_code'   => $row->deactivated_reason_code, 
                'deactivated_date'          => $row->deactivated_date
            ];
        }
        return $data;
    }
    
    public static function exists($email) {

        $maRec = MailAddress::findFirst([
                    'conditions' => 'upper(trim(email)) = upper(trim(?1))', 'bind' => [1 => $email]]
        );
        return ($maRec && trim(strtoupper($maRec->email)) == trim(strtoupper($email)));
    }

    public static function getFormats() {

        return [
            'html' => 'HTML',
            'plaintext' => 'Plain Text',
            'short (cell)' => 'Short (Cell)'
        ];
    }

    public static function getDeactivatedReasonCodes() {

        return [
            'L' => 'This address was deactivated by clicking the Unsubscribe link in the email footer.',
            'C' => 'This address was deactivated because VNS received a complaint from the recipient.',
            'B' => 'This address was deactivated because of permanent failed deliveries.',
            'A' => 'This address was deactivated by a VNS administrator.',
            'U' => 'This address was deactivated by the account owner.'
        ];
    }

    public static function getActive() {

        return [
            'y' => 'Yes - Notifications sent to this address.',
            'n' => 'No - Notifications not sent.'
        ];
    }

    public static function getDailySummaryOptions() {

        return [
            'n' => 'Do Not Receive',
            'y' => 'Receive'
        ];
    }

    public static function listUnsubscribe($address_id, $unsubscribe_key) {

        if (!$address_id || !$unsubscribe_key || strlen(trim($unsubscribe_key)) == 0 || !is_numeric($address_id)) {
            return 'Unsubscribe information not found for ' . $address_id . ', ' . $unsubscribe_key . '.';
        }
        $maRecs = MailAddress::find([
                    'conditions' => 'address_id = ?1 AND unsubscribe_key = ?2',
                    'bind' => [1 => $address_id, 2 => $unsubscribe_key]
        ]);

        $inactiveMsg = '';

        if (!isset($maRecs) || count($maRecs) == 0) {
            return 'Unsubscribe information not found.';
        }
        foreach ($maRecs as $maRec) {

            if ($maRec->address_id != $address_id || $maRec->unsubscribe_key != $unsubscribe_key) {

                return 'Unsubscribe information not found.';
            } else {

                if ($maRec->active == 'n') {

                    $inactiveMsg = 'Email address ' . $maRecs[0]->email . ' was already inactive.';
                } else {

                    $inactiveMsg = 'Email address ' . $maRecs[0]->email . ' has been made inactive and will not ' .
                            'receive messages  from VNS.<br/><br/>You may make ' . $maRecs[0]->email . ' active ' .
                            'again at any time after logging in.';

                    $maRec->active = 'n';
                    $maRec->deactivated_reason_code = 'L';
                    $maRec->deactivated_date = new \Phalcon\Db\RawValue('now()');
                    $maRec->save();

                    ActiveRecipientsWorkspace::refreshARW($maRecs[0]->email);
                }
            }
        }

        //
        return $inactiveMsg;
    }

    public static function complaintUnsubscribe($email) {

        if (!$email || strlen(trim($email)) == 0) {
            return 'Missing email address.';
        }
        $maRecs = MailAddress::find([
                    'conditions' => 'trim(lower(email)) = ?1', 'bind' => [1 => trim(strtolower($email))]
        ]);

        $inactiveMsg = '';

        if (!isset($maRecs) || count($maRecs) == 0) {
            return 'Unsubscribe information not found.';
        }
        foreach ($maRecs as $maRec) {

            if (trim(strtolower($maRec->email)) != trim(strtolower($email))) {

                return 'Unsubscribe information not found for provided email address.';
            } else {

                if ($maRec->active == 'n') {

                    $inactiveMsg = 'Email address ' . $maRecs[0]->email . ' was already inactive.';
                } else {

                    $inactiveMsg = 'Email address ' . $maRecs[0]->email . ' has been made inactive due to complaints ' .
                            ' and will not receive messages from VNS.<br/><br/>You may make ' .
                            $maRecs[0]->email . ' active again at any time after logging in.';

                    $maRec->active = 'n';
                    $maRec->deactivated_reason_code = 'C';
                    $maRec->deactivated_date = new \Phalcon\Db\RawValue('now()');
                    $maRec->save();

                    ActiveRecipientsWorkspace::refreshARW($email);
                }
            }
        }

        //
        return [
            'inactiveMsg' => $inactiveMsg,
            'user_id' => $maRecs[0]->user_id
        ];
    }

    public static function bounceUnsubscribe($email) {

        if (!$email || strlen(trim($email)) == 0) {
            return 'Missing email address.';
        }
        $maRecs = MailAddress::find([
                    'conditions' => 'trim(lower(email)) = ?1', 'bind' => [1 => trim(strtolower($email))]
        ]);

        $inactiveMsg = '';

        if (!isset($maRecs) || count($maRecs) == 0) {
            return 'Bounce unsubscribe information not found.';
        }
        foreach ($maRecs as $maRec) {

            if (trim(strtolower($maRec->email)) != trim(strtolower($email))) {

                return 'Bounce unsubscribe information not found for provided email address.';
            } else {

                if ($maRec->active == 'n') {

                    $inactiveMsg = 'Email address ' . $email . ' was already inactive.';
                } else {

                    $inactiveMsg = 'Email address ' . $email . ' has been made inactive due to permanent ' .
                            'bounces and will not receive messages from VNS.<br/><br/>' .
                            'You may make ' . $email . ' active ' . 'again at any time after logging in.';

                    $maRec->active = 'n';
                    $maRec->deactivated_reason_code = 'B';
                    $maRec->deactivated_date = new \Phalcon\Db\RawValue('now()');
                    $maRec->save();

                    ActiveRecipientsWorkspace::refreshARW($email);
                }
            }
        }

        //
        return $inactiveMsg;
    }

    // Marks duplicate ind for any duplicate email addresses created after the first one.
    public static function houseclean() {

        $model = new MailAddress();

        // Set duplicate_ind = 'n' where null
        $recs = MailAddress::find(['conditions' => 'duplicate_ind IS NULL']);
        foreach ($recs as $row) {
            $row->duplicate_ind = 'n';
            $row->save();
        }

        // Easy deletes
        $easyTotal = 0;
        $sql = "
            SELECT address_id FROM vns2.mail_address tgt
            WHERE  TRIM(UPPER(email)) IN (
                       SELECT utemail FROM (
                            SELECT TRIM(UPPER(email)) AS utemail
                            FROM   vns2.mail_address 
                            GROUP BY TRIM(UPPER(email)) 
                            HAVING COUNT(*) > 1
                        ) a
                        )
            AND (tgt.active = 'n' OR tgt.confirmed = 'n')
            ";
        $deleteRecs = new Resultset(null, $model, $model->getReadConnection()->query($sql));
        foreach ($deleteRecs as $rec) {
            $maRec = MailAddress::findFirst($rec->address_id);
            if ($maRec->address_id == $rec->address_id) {
                $maRec->delete();
                ActiveRecipientsWorkspace::refreshARW($maRec->email);
                $easyTotal++;
            }
        }

        // Set duplicate_ind = 'n' where there aren't any duplicates.
        $sql = "
            UPDATE vns2.mail_address tgt 
            SET    tgt.duplicate_ind = 'n'
            WHERE  IFNULL(tgt.duplicate_ind, 'y') = 'y'
                   AND TRIM(UPPER(email)) IN (
                       SELECT utemail FROM (
                            SELECT TRIM(UPPER(email)) AS utemail
                            FROM   vns2.mail_address 
                            GROUP BY TRIM(UPPER(email)) 
                            HAVING COUNT(*) = 1
                        ) a
                   )
            ";
        $model->getWriteConnection()->query($sql);

        /*
          UPDATE vns2.mail_address SET duplicate_ind = 'n';

          UPDATE vns2.mail_address SET duplicate_ind = 'y'
          WHERE
          active = 'y' AND confirmed = 'y'
          AND TRIM(UPPER(email)) IN (
          SELECT TRIM(UPPER(email)) AS tuemail
          FROM vns2.mail_address
          WHERE active = 'y' AND confirmed = 'y'
          GROUP BY TRIM(UPPER(email)) HAVING COUNT(*) > 1
          )
          AND (email, user_id) NOT IN (
          SELECT TRIM(UPPER(email)) AS tuemail, MAX(user_id) AS user_id
          FROM vns2.mail_address
          WHERE active = 'y' AND confirmed = 'y'
          GROUP BY TRIM(UPPER(email)) HAVING COUNT(*) > 1
          );
         */

        // Now address duplicates. Duplicates are email addresses added after first one. 
        // NOT SURE BEST WAY TO HANDLE THESE - REALLY NEED TO JUST NOT HAVE ANY DUPLICATES!
        $flaggedTotal = 0;
        return ['easyTotal' => $easyTotal, 'flaggedTotal' => $flaggedTotal];
    }

    public static function getAllForEmail($email) {

        $rows = RawSql::execReadQuery("
            SELECT 
                ma.address_id, ma.user_id, mu.username, mu.user_fullname, mu.added AS user_added, mu.lastlogin, 
                ma.email, ma.email_format, ma.confirmed, ma.confirmation_code, ma.receive_daily_summary_ind, 
                ma.daily_summary_sent_dttm, ma.duplicate_ind, ma.unsubscribe_key, ma.added, ma.active, 
                ma.deactivated_reason_code, ma.deactivated_date,
                CASE 
                    WHEN ma.deactivated_reason_code = 'L' THEN 'User clicked link in email.'
                    WHEN ma.deactivated_reason_code = 'C' THEN 'Received complaint from external source.'
                    WHEN ma.deactivated_reason_code = 'B' THEN 'Received bounced email.'
                    WHEN ma.deactivated_reason_code = 'A' THEN 'Admin disabled.'
                    WHEN ma.deactivated_reason_code = 'U' THEN 'User disabled.'
                    ELSE NULL
                END AS deactivated_reason
            FROM 
                mail_address ma JOIN mail_user mu ON mu.user_id = ma.user_id
            WHERE 
                UPPER(TRIM(ma.email)) = UPPER(TRIM(?))
            ORDER BY 
                ma.user_id, UPPER(TRIM(ma.email))
            ", [$email]);

        $data = [];
        foreach ($rows as $rec) {
            $data[] = [
                'user_id' => (int) $rec->user_id,
                'username' => $rec->username,
                'fullname' => $rec->user_fullname,
                'user_added' => $rec->user_added,
                'lastlogin' => $rec->lastlogin,
                'email' => $rec->email,
                'email_format' => $rec->email_format,
                'confirmed' => $rec->confirmed,
                'receive_daily_summary_ind' => $rec->receive_daily_summary_ind,
                'duplicate_ind' => $rec->duplicate_ind,
                'added' => $rec->added,
                'active' => $rec->active,
                'active_ind' => $rec->active,
                'deactivated_reason_code' => $rec->deactivated_reason_code,
                'deactivated_date' => $rec->deactivated_date,
                'deactivated_reason' => $rec->deactivated_reason
            ];
        }
        return $data;
    }

    public static function getAddress($address_id) {
        
        if (!$address_id || !is_numeric($address_id)) { return []; }
        return RawSql::execReadQueryOneRow("SELECT * FROM mail_address WHERE address_id = ?", [$address_id]);
    }
    
}
