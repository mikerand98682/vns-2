<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;

class MailAffiliation extends Model {

    public $affiliation_id;
    public $affiliation;

    public function initialize() {

        $this->setSource("mail_affiliation");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
        $this->hasMany("affiliation_id", "\VHP\Vns\Models\MailUser", "affiliation_id", array("alias" => "mail_user"));
    }

}
