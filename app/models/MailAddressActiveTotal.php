<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;

class MailAddressActiveTotal extends Model {

    public $mail_address_active_total_id, $total_date, $active_total;

    public function initialize() {

        $this->setSource('mail_address_active_total');
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function updateToday() {

        // Delete any records from today.
        RawSql::execUpdate("DELETE FROM vns2.mail_address_active_total WHERE total_date = CURDATE()");

        // Add new record for today.
        RawSql::execUpdate("
                INSERT INTO vns2.mail_address_active_total(total_date, active_total)
                SELECT CURDATE() as total_date, COUNT(*) AS active_total
                FROM (
                    SELECT DISTINCT email 
                    FROM vns2.mail_address
                    WHERE confirmed = 'y' AND active = 'y'
                        AND LOWER(TRIM(email)) NOT IN (SELECT LOWER(TRIM(email)) FROM vns2024.vns_user)
                ) a
            ");
    }

}
