<?php

namespace VHP\Vns\Models\mailman;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use VHP\Vns\Models\RawSql;

class Mailman extends Model {

    public $mailman_id, $sender, $sender_name, $recipient, $recipient_name, $recipient_priority, $subject, $body, $type,
            $unsubscribe_mailto, $unsubscribe_url, $pending_ind, $pending_dttm, $sent_ind, $sent_dttm, $error_ind,
            $error_text, $batch_dttm, $mailman_confirmation_id;

    public function initialize() {

        global $config;
        $this->setSchema($config->mail->mailmanSchema);

        $this->setSource("mailman");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function getTotalForEmailAndSubject($recipient, $subject) {
        
        global $config;
        $mailmanSchema = $config->mail->mailmanSchema;
        
        return RawSql::execMasterReadQueryOneRow("
            SELECT IFNULL(COUNT(*), 0) AS total FROM $mailmanSchema.mailman
                WHERE LOWER(TRIM(recipient)) = LOWER(TRIM(?)) AND LOWER(TRIM(subject)) = LOWER(TRIM(?))
        ", [$recipient, $subject])->total;
    }
    
    public static function getSentSinceTimestamp($email, $timestamp) {

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");
        $mailmanSchema = $config->mail->mailmanSchema;

        $email = trim(strtolower($email));

        if (strlen(trim($email)) == 0 || !is_numeric($timestamp)) {
            return false;
        }

        $model = new Mailman();
        $sql = "
            SELECT m.recipient, unix_timestamp(m.sent_dttm) AS sent_timestamp
            FROM " . $mailmanSchema . ".mailman m
            WHERE LOWER(TRIM(m.recipient)) = ? AND unix_timestamp(m.sent_dttm) > ?
            UNION ALL
            SELECT ma.recipient, unix_timestamp(ma.sent_dttm) AS sent_timestamp
            FROM " . $mailmanSchema . ".mailman_archive ma
            WHERE LOWER(TRIM(ma.recipient)) = ? AND unix_timestamp(ma.sent_dttm) > ?
            ";

        $params = [$email, $timestamp, $email, $timestamp];
        return new Resultset(null, $model, $model->getReadConnection()->query($sql, $params));
    }
    
    // Returns pending_ind (Y|N), sent_ind (Y|N), age_minutes 0,1,2,...
    // Or an empty rec if nothing is found.
    // This is used to determine if enough time has passed to send another message.
    public static function getUserSubjectStatus($email, $subject) {

        return RawSql::execMasterReadQueryOneRow("            
                        SELECT 
                            pending_ind, sent_ind,
                            ROUND((
                                    UNIX_TIMESTAMP(NOW()) -
                                    UNIX_TIMESTAMP(GREATEST(IFNULL(pending_dttm, NOW()), IFNULL(sent_dttm, NOW())))
                                ) / 60) AS age_minutes
                        FROM 
                            mailman.mailman
                        WHERE 
                            recipient = ? AND subject = ?
                        ORDER BY 
                            mailman_id DESC
                            LIMIT 1
                        ", [$email, $subject]
                );
    }
}
