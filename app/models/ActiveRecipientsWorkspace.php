<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;
use VHP\Vns\Models\hans\Volcano;
use VHP\Vns\Models\RawSql;

class ActiveRecipientsWorkspace extends Model {

    public $active_recipients_workspace_id, $address_id, $email, $volcano_cds_csv, $notice_type_cds_csv,
            $has_qualifiers_ind;

    public static $batchSize = 100;
    
    private static function _getConsolidatedMailHistory($email) {
        
        global $config;
        $hansMailmanSchema = $config->mail->hansMailmanSchema;
        $mailmanSchema = $config->mail->mailmanSchema;
        
        $recs = RawSql::execReadQuery("
                SELECT 
                    sent_ind, error_ind, sent_dttm, subject, sender, sender_name, 
                    UNIX_TIMESTAMP(sent_dttm) AS sent_unixtime
                FROM (
                    SELECT 
                        sent_ind AS sent_ind, error_ind AS error_ind, sent_dttm, 
                        subject AS subject, sender AS sender, sender_name AS sender_name
                    FROM 
                        $hansMailmanSchema.mailman 
                    WHERE 
                        UPPER(TRIM(recipient)) = UPPER(TRIM(?))
                    UNION ALL
                    SELECT 		
                        sent_ind AS sent_ind, error_ind AS error_ind, sent_dttm, 
                        subject AS subject, sender AS sender, sender_name AS sender_name
                    FROM 
                        $mailmanSchema.mailman 
                    WHERE 
                        UPPER(TRIM(recipient)) = UPPER(TRIM(?))
                    ) a 
                ORDER BY sent_dttm DESC
                ", 
                [$email, $email]
            );
        $data = [];
        foreach ($recs as $row) {
            $data[] = [
                'sent_ind'      => $row->sent_ind,
                'error_ind'     => $row->error_ind,
                'sent_dttm'     => $row->sent_dttm,
                'subject'       => $row->subject,
                'sender'        => $row->sender,
                'sender_name'   => $row->sender_name,
                'sent_unixtime' => $row->sent_unixtime
            ];
        }
        return $data;
    }
    
    public function initialize() {
        $this->setSource('active_recipients_workspace');
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    // Duplicates can sometimes be created when many updates are issued in a short period of time.
    private static function _removeDuplicates($email = null) {

        $emailClauseWithWhere = '';
        if ($email) {
            $emailClauseWithWhere = " WHERE TRIM(UPPER(email)) = '" . trim(strtoupper($email)) . "' ";
        }

        // Get duplicates
        $recs = RawSql::execReadQuery("
            SELECT arw.active_recipients_workspace_id FROM active_recipients_workspace arw 
		JOIN (
			SELECT UPPER(TRIM(email)) AS email, 
					MIN(active_recipients_workspace_id) AS first_active_recipients_workspace_id
			FROM active_recipients_workspace $emailClauseWithWhere 
			GROUP BY UPPER(TRIM(email)) 
			HAVING COUNT(*) > 1
		) dupes 
                    ON UPPER(TRIM(arw.email)) = dupes.email 
			AND arw.active_recipients_workspace_id <> dupes.first_active_recipients_workspace_id
            ");

        foreach ($recs as $row) {
            RawSql::execUpdate(
                    "DELETE FROM active_recipients_workspace WHERE active_recipients_workspace_id = ?",
                    [$row->active_recipients_workspace_id]
            );
        }
    }

    // Re-check invalid email addresses. Found out that if they're flagged as invalid incorrectly, they stay that way.
    private static function _recheckInvalidEmails() {

        $recs = RawSql::execReadQuery("
            SELECT active_recipients_workspace_id, email FROM active_recipients_workspace WHERE valid_email_ind = 'N'
            ");

        foreach ($recs as $row) {
            if (ActiveRecipientsWorkspace::_isEmailFormatOk($row->email)) {
                RawSql::execUpdate(
                    "
                    UPDATE active_recipients_workspace SET valid_email_ind = 'Y' 
                    WHERE active_recipients_workspace_id = ?
                    ",
                    [$row->active_recipients_workspace_id]
                );
            }
        }
    }
    
    private static function _isEmailFormatOk($email) {
        
        global $config;
        $exePath = $config->application->javaEmailValidatorExe;
        if (file_exists($exePath) && is_executable($exePath)) {
            exec("$exePath $email", $msgs, $rc);
            return $rc == 0;
        } else {
            //global $logger;
            //$logger->error("ERROR, ActiveRecipientsWorkspace.php: did not find $exePath or is not executable!");
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
    }

    public static function deleteUser($user_id) {

        if (!isset($user_id) || !$user_id || !is_numeric($user_id)) {
            return null;
        }
        RawSql::execUpdate("DELETE FROM active_recipients_workspace WHERE user_id = ?", [$user_id]);
    }

    public static function refreshARW($email = null) {

        global $config, $logger;
        $hansSchema = $config->application->hansSchema;

        $logger->info("Refreshing Active Recipients Workspace - START");
        
        ActiveRecipientsWorkspace::_removeDuplicates($email);

        $emailClause = '';
        $emailClauseWithWhere = '';

        if ($email) {
            
            $emailClause = " AND TRIM(UPPER(email)) = '" . trim(strtoupper($email)) . "' ";
            $emailClauseWithWhere = " WHERE TRIM(UPPER(email)) = '" . trim(strtoupper($email)) . "' ";
            
            // Addresses need to be active and confirmed for all these operations.
            // Make sure duplicate email addresses are correctly marked. Highest user_id value will never be a duplicate.

            RawSql::execUpdate("
                    UPDATE mail_address tgt JOIN (
                        SELECT TRIM(UPPER(email)) AS email, MAX(user_id) AS max_user_id
                        FROM mail_address
                        WHERE TRIM(UPPER(email)) IN (
                            SELECT TRIM(UPPER(email)) AS email
                            FROM   mail_address
                            WHERE  active = 'y' AND confirmed = 'y'
                            GROUP BY TRIM(UPPER(email))
                        )
                        GROUP BY TRIM(UPPER(email))
                    ) src ON src.email = TRIM(UPPER(tgt.email))
                    SET tgt.duplicate_ind = CASE WHEN tgt.user_id = src.max_user_id THEN 'n' ELSE 'y' END
                    " . str_replace('email', 'tgt.email', $emailClauseWithWhere)
            );
        }
        
        // We ignore any duplicate email addresses going forward.
        // Insert any new email addresses using address_id
        
        $logger->info("Refreshing Active Recipients Workspace - INSERT MISSING");
        
        RawSql::execUpdate("
            INSERT INTO active_recipients_workspace (
                address_id, email, valid_email_ind, user_id, user_fullname, unsubscribe_key
            )(
                SELECT address_id, email, 'U' AS valid_email_ind, mu.user_id, mu.user_fullname, src.unsubscribe_key
                FROM mail_address src, mail_user mu
                WHERE 
                    src.active = 'y' AND src.confirmed = 'y' AND src.duplicate_ind = 'n' $emailClause
                    AND src.user_id = mu.user_id
                    AND NOT EXISTS (
                        SELECT NULL FROM active_recipients_workspace tgt 
                        WHERE src.address_id = tgt.address_id $emailClause
                    )
                    AND NOT EXISTS (
                        SELECT NULL FROM vns2024.vns_user v3 WHERE v3.email = LOWER(TRIM(src.email))
                    )
            )
            ");

        // Update any email address records that may have changed by joining on address_id
        
        $logger->info("Refreshing Active Recipients Workspace - UPDATE");
        
        RawSql::execUpdate("
            UPDATE active_recipients_workspace tgt 
                JOIN (
                        SELECT * FROM mail_address 
                        WHERE active = 'y' AND confirmed = 'y' AND duplicate_ind = 'n' $emailClause
                    ) src ON tgt.address_id = src.address_id
            SET 
                tgt.email           = src.email, 
                tgt.valid_email_ind = 'U',
                tgt.user_id         = src.user_id,
                tgt.email_format    = src.email_format,
                tgt.unsubscribe_key = src.unsubscribe_key
            WHERE 
                tgt.email              <> src.email
                OR tgt.user_id         <> src.user_id
                OR tgt.email_format    <> src.email_format
                OR IFNULL(tgt.unsubscribe_key, '') <> IFNULL(src.unsubscribe_key, '')
            ");

        // Delete any addresses, using address_id, not in mail_address as active, confired and unduplicated.
                
        $logger->info("Refreshing Active Recipients Workspace - CLEAN");
                
        RawSql::execUpdate("
            DELETE FROM active_recipients_workspace WHERE active_recipients_workspace_id IN (
                SELECT active_recipients_workspace_id FROM active_recipients_workspace tgt 
                WHERE NOT EXISTS (
                        SELECT NULL FROM mail_address src 
                        WHERE src.active = 'y' AND src.confirmed = 'y' AND src.duplicate_ind = 'n' 
                            AND src.address_id = tgt.address_id $emailClause
                    )
                    $emailClause
            )
            ");

        // Update any user_fullname values.

        RawSql::execUpdate("
            UPDATE active_recipients_workspace tgt 
                JOIN (
                        SELECT mu.user_id, mu.user_fullname 
                        FROM   mail_user mu, mail_address ma 
                        WHERE  mu.user_id = ma.user_id $emailClause
                    ) src ON tgt.user_id = src.user_id
            SET 
                tgt.user_fullname = src.user_fullname
            WHERE 
                IFNULL(tgt.user_fullname, '') <> IFNULL(src.user_fullname, '')
            ");

        //
        // Update recipient_priority
        //

        RawSql::execUpdate("
            UPDATE active_recipients_workspace SET recipient_priority = CASE 
                WHEN TRIM(UPPER(email)) = 'MJRANDALL@USGS.GOV' THEN 9
                WHEN TRIM(UPPER(email)) = 'MJRANDA@GMAIL.COM' THEN 9
                WHEN TRIM(UPPER(email)) = 'CHERYL.CAMERON@GMAIL.COM' THEN 2
                WHEN TRIM(UPPER(email)) LIKE '%FEMA.DHS.GOV' THEN 3
                WHEN TRIM(UPPER(email)) LIKE '%FAA.GOV' THEN 4
                WHEN TRIM(UPPER(email)) LIKE '%US.AF.MIL' THEN 4
                WHEN TRIM(UPPER(email)) LIKE '%USGS.GOV' THEN 5
                WHEN TRIM(UPPER(email)) LIKE '%.EDU' THEN 6
                WHEN TRIM(UPPER(email)) LIKE '%.GOV' THEN 6
                WHEN TRIM(UPPER(email)) LIKE '%.MIL' THEN 6
                ELSE 8
            END $emailClauseWithWhere
            ");

        //
        // Update has_qualifiers_ind
        //

        RawSql::execUpdate("
            UPDATE active_recipients_workspace tgt LEFT JOIN (
                SELECT DISTINCT 
                        ma.address_id
                    FROM 
                        mail_volcpref mv, mail_address ma
                    WHERE 
                        ma.active = 'y' AND ma.confirmed = 'y' AND ma.duplicate_ind = 'n' $emailClause
                        AND ma.user_id = mv.user_id
                        AND (TRIM(IFNULL(mv.alert_level,'')) <> '' OR TRIM(IFNULL(mv.color_code,'')) <> '')
            ) src ON src.address_id = tgt.address_id
            SET tgt.has_qualifiers_ind = CASE WHEN src.address_id IS NULL THEN 'N' ELSE 'Y' END $emailClauseWithWhere
            ");

        // Invalid emails that may have been set in error do not get refreshed.
        ActiveRecipientsWorkspace::_recheckInvalidEmails();
        
        // Now do the hard work.....
        //
        // Verify email address is ok for records that have not been verified. Because this can significantly delay 
        // sending if table is refreshed, limit updates to 1000 records at a time. Other queries won't be affected 
        // because when valid_email_ind = 'U' or 'Y', mail is allowed.
        //

        $logger->info("Refreshing Active Recipients Workspace - HARD WORK");
        
        $recs = RawSql::execMasterReadQuery("
                    SELECT active_recipients_workspace_id, email FROM active_recipients_workspace 
                    WHERE valid_email_ind = 'U'" . $emailClause . " LIMIT 1000
                ");
        foreach ($recs as $row) {
            $valid_email_ind = ActiveRecipientsWorkspace::_isEmailFormatOk($row->email) ? 'Y' : 'N';
            RawSql::execUpdate("
                    UPDATE active_recipients_workspace SET valid_email_ind = ? 
                    WHERE active_recipients_workspace_id = ?
                    ", [
                ActiveRecipientsWorkspace::_isEmailFormatOk($row->email) ? 'Y' : 'N',
                $row->active_recipients_workspace_id
                    ]
            );
        }

        //
        // Update notice_type_cds_csv field as needed
        //

        $recs = RawSql::execMasterReadQuery("
            SELECT DISTINCT
                ma.address_id, 
                CASE WHEN TRIM(nt.notice_type_cd) = 'VV' THEN 'VONA' ELSE TRIM(nt.notice_type_cd) END AS notice_type_cd,
                IFNULL(ao.notice_type_cds_csv, '') AS notice_type_cds_csv
            FROM 
                mail_type mt, mail_address ma,
                $hansSchema.notice_type nt, active_recipients_workspace ao
            WHERE 
                ma.user_id           = mt.user_id 
                AND mt.hans_type_id  = nt.legacy_type_id
                AND ma.address_id    = ao.address_id
                AND ma.active        = 'y' 
                AND ma.confirmed     = 'y' 
                AND ma.duplicate_ind = 'n' " . str_replace('email', 'ma.email', $emailClause) . "
            ORDER BY
                ma.address_id, TRIM(nt.notice_type_cd)
            ");

        $notice_type_cds_csv = '';
        $last_notice_type_cds_csv = '';
        $last_address_id = null;
        $typesUpdated = 0;

        foreach ($recs as $row) {

            if (!$last_address_id) { // First record, initialize values.
                $last_address_id = $row->address_id;
                $last_notice_type_cds_csv = $row->notice_type_cds_csv;
            }

            if ($last_address_id != $row->address_id) { // Moved to new address record save previous record's values.
                if ($notice_type_cds_csv != $last_notice_type_cds_csv) {
                    RawSql::execUpdate(
                            "UPDATE active_recipients_workspace SET notice_type_cds_csv = ? " .
                            "WHERE address_id = ?",
                            [$notice_type_cds_csv, $last_address_id]
                    );
                    $typesUpdated++;
                }
                $last_address_id = $row->address_id;
                $last_notice_type_cds_csv = $row->notice_type_cds_csv;
                $notice_type_cds_csv = '';
            }

            $notice_type_cds_csv .= $notice_type_cds_csv == '' ? $row->notice_type_cd : ',' . $row->notice_type_cd;
        }

        if ($notice_type_cds_csv != $last_notice_type_cds_csv) { // If last record was changed, it wasn't saved....
            RawSql::execUpdate(
                    "UPDATE active_recipients_workspace SET notice_type_cds_csv = ? WHERE address_id = ?",
                    [$notice_type_cds_csv, $last_address_id]
            );
            $typesUpdated++;
        }

        // Fix any users without notice types.
        RawSql::execUpdate("
            UPDATE active_recipients_workspace tgt JOIN (
                    SELECT ma.address_id 
                    FROM mail_address ma 
                            LEFT JOIN mail_type mt ON mt.user_id = ma.user_id
                    WHERE ma.active = 'y' AND ma.confirmed = 'y' AND ma.duplicate_ind = 'n'
                        AND mt.user_id IS NULL " . str_replace('email', 'ma.email', $emailClause) . "
                ) mt ON mt.address_id = tgt.address_id
            SET tgt.notice_type_cds_csv = '' $emailClauseWithWhere
        ");

        //
        // Update volcano_cds_csv field as needed.
        //

        $recs = RawSql::execMasterReadQuery("
            SELECT 
                ma.address_id, 
                IFNULL(ao.volcano_cds_csv, '') AS volcano_cds_csv, 
                TRIM(mv.volcano_cd) AS volcano_cd
            FROM 
                mail_volcpref mv, mail_address ma,
                active_recipients_workspace ao
            WHERE 
                ma.user_id           = mv.user_id 
                AND ma.address_id    = ao.address_id
                AND ma.active        = 'y' 
                AND ma.confirmed     = 'y' 
                AND ma.duplicate_ind = 'n' " . str_replace('email', 'ma.email', $emailClause) . "
            ORDER BY
                ma.address_id, TRIM(mv.volcano_cd)
            ");

        $volcano_cds_csv = '';
        $last_volcano_cds_csv = '';
        $last_address_id = null;
        $volcsUpdated = 0;

        foreach ($recs as $row) {

            if (!$last_address_id) { // First record, initialize values.
                $last_address_id = $row->address_id;
                $last_volcano_cds_csv = $row->volcano_cds_csv;
            }

            if ($last_address_id != $row->address_id) { // Moved to new address record save previous record's values.
                if ($volcano_cds_csv != $last_volcano_cds_csv) {
                    RawSql::execUpdate(
                            "UPDATE active_recipients_workspace SET volcano_cds_csv = ? WHERE address_id = ?",
                            [$volcano_cds_csv, $last_address_id]
                    );
                    $volcsUpdated++;
                }
                $last_address_id = $row->address_id;
                $last_volcano_cds_csv = $row->volcano_cds_csv;
                $volcano_cds_csv = '';
            }

            $volcano_cds_csv .= $volcano_cds_csv == '' ? $row->volcano_cd : ',' . $row->volcano_cd;
        }

        if ($volcano_cds_csv != $last_volcano_cds_csv) { // If last record was changed, it wasn't saved....
            RawSql::execUpdate(
                    "UPDATE active_recipients_workspace SET volcano_cds_csv = ? WHERE address_id = ?",
                    [$volcano_cds_csv, $last_address_id]
            );
            $volcsUpdated++;
        }

        // Fix any users without volcanoes.
        RawSql::execUpdate("
            UPDATE active_recipients_workspace tgt JOIN (
                    SELECT ma.address_id 
                    FROM mail_address ma 
                            LEFT JOIN mail_volcpref mv ON mv.user_id = ma.user_id
                    WHERE ma.active = 'y' AND ma.confirmed = 'y' AND ma.duplicate_ind = 'n'
                        AND mv.user_id IS NULL " . str_replace('email', 'ma.email', $emailClause) . "
                ) mv ON mv.address_id = tgt.address_id
            SET tgt.volcano_cds_csv = '' $emailClauseWithWhere
        ");

        // Some users have migrated to the new VNS (2024 version), delete them here
        RawSql::execUpdate("
                DELETE FROM vns2.active_recipients_workspace
                WHERE LOWER(TRIM(email)) IN (SELECT LOWER(TRIM(email)) FROM vns2024.vns_user) 
            ");

        $logger->info("Refreshing Active Recipients Workspace - DONE");
        
        //
        return ['typesUpdated' => $typesUpdated, 'volcsUpdated' => $volcsUpdated, 'email' => !$email ? 'ALL' : $email];
    }

    public static function getByEmail($email) {

        $rec = RawSql::execMasterReadQueryOneRow("
                    SELECT 
                        arw.active_recipients_workspace_id, arw.user_id, arw.user_fullname, arw.address_id, arw.email, 
                        arw.email_format, arw.unsubscribe_key, arw.volcano_cds_csv, arw.notice_type_cds_csv, 
                        arw.has_qualifiers_ind, arw.recipient_priority, arw.valid_email_ind, mu.username
                    FROM 
                        active_recipients_workspace arw
                            JOIN mail_user mu ON mu.user_id = arw.user_id
                    WHERE 
                        UPPER(TRIM(email)) = UPPER(TRIM(?))
                    ", [$email]);

        if (!$rec) {
            return [
                'email' => $email,
                'email_data' => MailAddress::getAllForEmail($email),
                'sent_mail_history' => ActiveRecipientsWorkspace::_getConsolidatedMailHistory($email)
            ];
        }

        $volcanoes = [];
        foreach (explode(',', $rec->volcano_cds_csv) as $volcano_cd) {
            if (str_contains($volcano_cd, 'all_')) {
                $volcanoes[] = [
                    'volcano_name' => $volcano_cd,
                    'volcano_cd' => $volcano_cd,
                    'region' => $volcano_cd,
                    'qualifiers' => []
                ];
            } else {
                $vRec = Volcano::getVolcanoRec($volcano_cd);
                if (!$vRec || !isset($vRec->volcano_cd)) {
                    continue;
                }
                $volcanoes[] = [
                    'volcano_name' => $vRec->volcano_name,
                    'volcano_cd' => $vRec->volcano_cd,
                    'region' => $vRec->region,
                    'qualifiers' => []
                ];
            }
        }

        $qualifiers = [];
        if ($rec->has_qualifiers_ind == 'Y') {
            $qualifiers = MailVolcpref::getByARWEmail($email);
        }

        foreach ($volcanoes as $idx => $volcano) {
            foreach ($qualifiers as $qualifier) {
                if ($volcano['volcano_cd'] == $qualifier['volcano_cd']) {
                    $volcanoes[$idx]['qualifiers'] = $qualifier;
                }
            }
        }

        return [
            'user_fullname'       => $rec->user_fullname,
            'username'            => $rec->username,
            'user_id'             => (int) $rec->user_id,
            'address_id'          => (int) $rec->address_id,
            'unsubscribe_key'     => $rec->unsubscribe_key,
            'email'               => $rec->email,
            'email_format'        => $rec->email_format,
            'volcano_cds_csv'     => $rec->volcano_cds_csv,
            'notice_type_cds_csv' => $rec->notice_type_cds_csv,
            'has_qualifiers_ind'  => $rec->has_qualifiers_ind,
            'valid_email_ind'     => $rec->valid_email_ind,
            'notice_types'        => MailType::getByARWEmail($email),
            'volcanoes'           => $volcanoes,
            'email_data'          => MailAddress::getAllForEmail($email),
            'sent_mail_history'   => ActiveRecipientsWorkspace::_getConsolidatedMailHistory($email)
        ];
    }

    public static function getTotal() {
        
        return RawSql::execMasterReadQueryOneRow("SELECT COUNT(*) AS total FROM active_recipients_workspace")-> total;
    }
    
    // If email subject is provided, process will not send another message matching the subject. This is helpful if
    // restarting the process after a crash.
    public static function getBatch($batchNum) {
        
        $batchSize = ActiveRecipientsWorkspace::$batchSize;
        
        $offset = $batchNum * $batchSize;
        $recs = RawSql::execReadQuery("
                SELECT email 
                FROM   active_recipients_workspace 
                WHERE  LENGTH(TRIM(IFNULL(email,''))) > 0
                ORDER BY UPPER(TRIM(email))
                LIMIT $batchSize OFFSET $offset
                ");
        
        $data = [];
        foreach($recs as $row) { $data[] = $row->email; }
        return $data; 
    }
    
    public static function getUserEmails($user_id) {
        
        if (!$user_id || !is_numeric($user_id)) { return []; }
        $recs = RawSql::execReadQuery("SELECT email FROM active_recipients_workspace WHERE user_id = ?", [$user_id]);
        $emails = [];
        foreach ($recs as $row) {
            if (isset($row->email) && strlen(trim($row->email)) > 0) { $emails[] = $row->email; }
        }
        return $emails;
    }
}
