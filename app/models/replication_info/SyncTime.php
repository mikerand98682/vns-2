<?php

namespace VHP\Vns\Models\replication_info;

use Phalcon\Mvc\Model;
use VHP\Vns\Models\RawSql;

class SyncTime extends Model {

    public $sync_time_id, $hostname, $sync_dttm;

    public function initialize() {

        $this->setSchema('replication_info');
        $this->setSource("sync_time");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function hasTableAccess() {

        // Does table exist and can I read it? If not, allow VNS login, something is wrong but don't break VNS.
        $rec = RawSql::execReadQueryOneRow("
                    SELECT COUNT(*) AS table_total FROM information_schema.tables 
                    WHERE table_schema = 'replication_info' AND table_name = 'sync_time'
                ");

        return (int) $rec->table_total > 0;
    }

    public static function isReplicationCurrent() {

        if (!SyncTime::hasTableAccess()) {
            return true;
        } // Something is wrong but don't break VNS.
        // Replication_info.sync_time table exists and is readable.
        $rec = RawSql::execReadQueryOneRow("
                    SELECT UNIX_TIMESTAMP() - UNIX_TIMESTAMP(MAX(sync_dttm)) AS age_seconds
                    FROM   replication_info.sync_time
                ");

        return (int) $rec->age_seconds <= 70; // If age_seconds is greater than 70 seconds, db sync is probably having 
        // problems. The value in the table is updated every minute.
    }

}
