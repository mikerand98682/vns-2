<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class MailVolcpref extends Model {

    public $volcpref_id;
    public $user_id;
    public $obs_abbr;
    public $volcano_cd;
    public $alert_level;
    public $color_code;
    public $hzd_qual;
    public $aviat_qual;

    public function initialize() {

        $this->setSource("mail_volcpref");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');

        $this->hasOne("volcano", "VHP\Vns\Models\hans\Volcano", "volcano_cd", array("alias" => "volcano"));
    }

    // TODO: should be able to have nulls here.
    public function beforeValidation() {

        if (!$this->alert_level) {
            $this->alert_level = '';
        }
        if (!$this->color_code) {
            $this->color_code = '';
        }
        if (!$this->hzd_qual) {
            $this->hzd_qual = 1;
        }
        if (!$this->aviat_qual) {
            $this->aviat_qual = 1;
        }
    }

    public static function getColorCodes() {

        return array(
            "UNASSIGNED" => "UNASSIGNED",
            "GREEN" => "GREEN",
            "YELLOW" => "YELLOW",
            "ORANGE" => "ORANGE",
            "RED" => "RED"
        );
    }

    public static function getAlertLevels() {

        return array(
            "UNASSIGNED" => "UNASSIGNED",
            "NORMAL" => "NORMAL",
            "ADVISORY" => "ADVISORY",
            "WATCH" => "WATCH",
            "WARNING" => "WARNING"
        );
    }

    public static function getAviatQual() {

        return array(
            "1" => "EQUAL TO",
            "2" => "LESS THAN",
            "3" => "LESS THAN OR EQUAL TO",
            "4" => "GREATER THAN",
            "5" => "GREATER THAN OR EQUAL TO"
        );
    }

    public static function getHzdQual() {

        return array(
            "1" => "EQUAL TO",
            "2" => "LESS THAN",
            "3" => "LESS THAN OR EQUAL TO",
            "4" => "GREATER THAN",
            "5" => "GREATER THAN OR EQUAL TO"
        );
    }

    /**
     * Returns specific volcano selections, all_{OBS} selections will not be included in this list.
     */
    public static function getMyVolcanoes($user_id) {

        if (!is_numeric($user_id)) {
            return null;
        }

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");
        $hansSchema = $config->application->hansSchema;

        $sql = "
			SELECT 
				v.volcano_name AS vpref, v.volcano_id,
				mv.*,
				o.obs_fullname,
				v.volcano_name, v.latitude, v.longitude, v.vnum, v.region,
				v.volcano_url, o.obs_abbr, v.nvews_threat
			FROM 
				mail_volcpref mv 
					LEFT JOIN $hansSchema.volcano v ON mv.volcano_cd = v.volcano_cd
					LEFT JOIN $hansSchema.observatory o ON mv.obs_abbr = o.obs_abbr
			WHERE 
				mv.user_id = $user_id
				AND mv.volcano_cd NOT LIKE 'all_%'
			ORDER BY
				UPPER(v.volcano_name), UPPER(v.region), UPPER(o.obs_fullname)
            ";

        $mvp = new MailVolcpref();
        return new Resultset(null, $mvp, $mvp->getReadConnection()->query($sql));
    }

    /**
     * Returns list of all_{OBS} selections. Mused records ever returned for a user should be < 10.
     */
    public static function getMyAll($user_id) {

        if (!is_numeric($user_id)) {
            return null;
        }

        $sql = "
			SELECT 
				CASE 
					WHEN mv.volcano_cd = 'all_avo'   THEN 'All Alaska Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_cvo'   THEN 'All Cascades Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_lvo'   THEN 'All California Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_calvo' THEN 'All California Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_hvo'   THEN 'All Hawaiian Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_nmi'   THEN 'All Northern Marianas Islands Volcanoes'
					WHEN mv.volcano_cd = 'all_yvo'   THEN 'All Yellowstone Volcano Observatory Volcanoes'
				END AS vpref,
				mv.*
			FROM 
				mail_volcpref mv 
			WHERE 
				mv.user_id = $user_id
				AND mv.volcano_cd LIKE 'all_%'
			ORDER BY
				UPPER(CASE 
					WHEN mv.volcano_cd = 'all_avo'   THEN 'All Alaska Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_cvo'   THEN 'All Cascades Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_lvo'   THEN 'All California Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_calvo' THEN 'All California Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_hvo'   THEN 'All Hawaiian Volcano Observatory Volcanoes'
					WHEN mv.volcano_cd = 'all_nmi'   THEN 'All Northern Marianas Islands Volcanoes'
					WHEN mv.volcano_cd = 'all_yvo'   THEN 'All Yellowstone Volcano Observatory Volcanoes'
				END)
            ";

        $mvp = new MailVolcpref();
        return new Resultset(null, $mvp, $mvp->getReadConnection()->query($sql));
    }

    public static function getByARWEmail($email) {

        global $config;
        $hansSchema = $config->application->hansSchema;

        $recs = RawSql::execReadQuery("
            SELECT 
                mv.volcpref_id, mv.user_id, mv.obs_abbr, 
                mv.volcano_cd, v.volcano_name,
                mv.alert_level, mv.color_code, 
                mv.hzd_qual, mv.aviat_qual,
                CASE 
                    WHEN LENGTH(TRIM(IFNULL(mv.color_code,''))) = 0 THEN NULL
                    WHEN mv.aviat_qual = 1 THEN CONCAT('EQUAL TO ', mv.color_code) 
                    WHEN mv.aviat_qual = 2 THEN CONCAT('LESS THAN ', mv.color_code) 
                    WHEN mv.aviat_qual = 3 THEN CONCAT('LESS THAN OR EQUAL TO ', mv.color_code) 
                    WHEN mv.aviat_qual = 4 THEN CONCAT('GREATER THAN ', mv.color_code) 
                    WHEN mv.aviat_qual = 5 THEN CONCAT('GREATER THAN OR EQUAL TO ', mv.color_code) 
                END AS aviat_qual_text,
                CASE 
                    WHEN LENGTH(TRIM(IFNULL(mv.alert_level,''))) = 0 THEN NULL
                    WHEN mv.hzd_qual = 1 THEN CONCAT('EQUAL TO ', mv.alert_level) 
                    WHEN mv.hzd_qual = 2 THEN CONCAT('LESS THAN ', mv.alert_level) 
                    WHEN mv.hzd_qual = 3 THEN CONCAT('LESS THAN OR EQUAL TO ', mv.alert_level) 
                    WHEN mv.hzd_qual = 4 THEN CONCAT('GREATER THAN ', mv.alert_level) 
                    WHEN mv.hzd_qual = 5 THEN CONCAT('GREATER THAN OR EQUAL TO ', mv.alert_level) 
                END AS hzd_qual_text
            FROM
                active_recipients_workspace arw 
                    JOIN mail_volcpref mv ON mv.user_id = arw.user_id
                        LEFT JOIN " . $hansSchema . ".volcano v ON v.volcano_cd = mv.volcano_cd
            WHERE 
                UPPER(TRIM(arw.email)) = UPPER(TRIM(?))
                AND (LENGTH(TRIM(IFNULL(mv.alert_level,''))) > 0 || LENGTH(TRIM(IFNULL(mv.color_code,''))) > 0)
            ORDER BY
                mv.obs_abbr, mv.volcano_cd
                ", [$email]);

        if (count($recs) == 0) {
            return null;
        }

        $data = [];
        foreach ($recs as $row) {
            $data[] = [
                'obs_abbr'        => $row->obs_abbr,
                'volcano_cd'      => $row->volcano_cd,
                'volcano_name'    => $row->volcano_name,
                'alert_level'     => $row->alert_level,
                'hzd_qual'        => (int) $row->hzd_qual,
                'hzd_qual_text'   => $row->hzd_qual_text,
                'color_code'      => $row->color_code,
                'aviat_qual'      => (int) $row->aviat_qual,
                'aviat_qual_text' => $row->aviat_qual_text
            ];
        }
        return $data;
    }

}
