<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;

class MailType extends Model {

    public $mail_type_id;
    public $user_id;
    public $hans_type_id;

    public function initialize() {

        $this->setSource("mail_type");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function getByARWEmail($email) {

        global $config;
        $hansSchema = $config->application->hansSchema;

        $recs = RawSql::execReadQuery("
                SELECT 
                    nt.notice_category, nt.notice_type, nt.notice_type_cd
                FROM
                    active_recipients_workspace arw
                        JOIN mail_type mt ON mt.user_id = arw.user_id
                            JOIN " . $hansSchema . ".notice_type nt ON nt.legacy_type_id = mt.hans_type_id
                WHERE 
                    UPPER(TRIM(arw.email)) = UPPER(TRIM(?))
                    AND nt.type_deleted_ind <> 'Y'
                ORDER BY
                    nt.notice_category_sort_order, nt.type_sort_order
                ", [$email]);

        if (count($recs) == 0) {
            return null;
        }

        $data = [];
        foreach ($recs as $row) {
            $data[] = [
                'notice_category' => $row->notice_category,
                'notice_type' => $row->notice_type,
                'notice_type_cd' => $row->notice_type_cd
            ];
        }
        return $data;
    }

}
