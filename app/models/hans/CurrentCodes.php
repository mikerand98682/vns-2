<?php

namespace VHP\Vns\Models\hans;

use Phalcon\Mvc\Model;

class CurrentCodes extends Model {

    public $current_codes_id;
    public $volcano_id;
    public $notice_id;
    public $sent_utc;
    public $notice_section_id;
    public $alert_level_id;
    public $color_code_id;
    public $alert_level;
    public $color_code;
    public $previous_notice_section_id;
    public $previous_alert_level_id;
    public $previous_color_code_id;
    public $previous_alert_level;
    public $previous_color_code;
    public $alert_date;
    public $color_date;

    public function initialize() {

        global $config;
        $this->setSchema($config->application->hansSchema);
        $this->setSource("current_codes");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

}
