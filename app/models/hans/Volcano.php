<?php

namespace VHP\Vns\Models\hans;

use VHP\Vns\Models\MailVolcpref;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Volcano extends Model {

    public $volcano_id;
    public $volcano_cd;
    public $volcano_name;
    public $observatory_id;
    public $region;
    public $latitude;
    public $longitude;
    public $vnum;
    public $elevation_meters;
    public $boilerplate;
    public $volcano_url;
    public $volcano_image_url;
    public $nvews_threat;
    public $last_update;

    public function initialize() {

        global $config;
        $this->setSchema($config->application->hansSchema);

        $this->setSource("volcano");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');

        $this->hasOne(
                    "observatory_id", "VHP\Vns\Models\hans\Observatory", "observatory_id", ["alias" => "observatory"]
                );
    }

    public static function selectRegions() {

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");
        $hansSchema = $config->application->hansSchema;

        $sql = "
                SELECT v.region, o.obs_abbr, o.obs_fullname, COUNT(*) AS vtot 
                FROM   $hansSchema.observatory o, $hansSchema.volcano v
                WHERE  v.observatory_id = o.observatory_id
                       AND TRIM(IFNULL(v.region, '')) <> ''
                GROUP BY v.region, o.obs_abbr, o.obs_fullname
                ORDER BY UPPER(v.region), UPPER(o.obs_fullname)
                ";

        $v = new Volcano();
        return new Resultset(null, $v, $v->getReadConnection()->query($sql));
    }

    /**
     * Returns json object with user's volcano selection data.
     */
    public static function getMyVolcsAsJson($user_id) {

        if (!is_numeric($user_id)) {
            return null;
        }

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");
        $hansSchema = $config->application->hansSchema;

        $sql = "
                SELECT
                    mv.user_id, mv.volcano_cd, mp.alert_level, mp.color_code, mp.hzd_qual, mp.aviat_qual, 
                    v.volcano_name, v.region, v.vnum, v.latitude, v.longitude
                FROM
                    mail_volcpref mv 
                        LEFT JOIN mail_volcpref mp ON mv.volcano_cd = mp.volcano_cd AND mv.user_id = mp.user_id
                        LEFT JOIN $hansSchema.volcano v ON mv.volcano_cd = v.volcano_cd
                WHERE
                    mv.user_id = $user_id
                ORDER BY
                    UPPER(v.volcano_name), mv.volcano_cd
                ";

        $mvp = new MailVolcpref();
        $rs = new Resultset(null, $mvp, $mvp->getReadConnection()->query($sql));
        $data = [];
        foreach ($rs as $rec) {
            $data[] = $rec->volcano_cd;
        }
        return json_encode($data);
    }

    public static function getVolcanoRec($volcano_cd) {

        $vRec = Volcano::findFirst(['conditions' => 'volcano_cd = ?1', 'bind' => [1 => $volcano_cd]]);
        return !$vRec || $vRec->volcano_cd != $volcano_cd ? null : $vRec;
    }

}
