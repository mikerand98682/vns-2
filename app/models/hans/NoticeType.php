<?php

namespace VHP\Vns\Models\hans;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use VHP\Vns\Models\RawSql;

class NoticeType extends Model {

    public $notice_type_id;
    public $notice_category;
    public $notice_category_sort_order;
    public $notice_type;
    public $type_description;
    public $type_sort_order;
    public $notice_type_title;
    public $notice_type_cd;
    public $legacy_type_id;
    public $cap_effective_period_days;
    public $type_deleted_ind;

    public function initialize() {

        global $config;
        $this->setSchema($config->application->hansSchema);
        $this->setSource("notice_type");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function getActivityReport() {

        $config = new \Phalcon\Config\Adapter\Ini("../app/config/config.ini");
        $hansSchema = $config->application->hansSchema;

        $sql = "
            SELECT 
                n.notice_identifier,
                curr.volcano_id,
                v.volcano_name,
                v.volcano_cd,
                v.vnum,
                v.volcano_url,
                ns.synopsis,
                curr.notice_id, 
                curr.notice_section_id, 
                curr.alert_level, 
                curr.color_code, 
                curr.sent_utc,
                DATE_FORMAT(n.sent_utc, '%b %e, %Y')     AS newdate,
                DATE_FORMAT(n.sent_utc, '%H:%i')         AS newtime,
                cdate.sent_utc                           AS curr_sent_utc,
                DATE_FORMAT(cdate.sent_utc, '%b %e, %Y') AS curr_newdate,
                DATE_FORMAT(cdate.sent_utc, '%H:%i')     AS curr_newtime,
                IFNULL(prev.alert_level, 'UNASSIGNED')   AS prev_alert_level,
                IFNULL(prev.color_code, 'UNASSIGNED')    AS prev_color_code,
                prev.sent_utc                            AS prev_sent_utc
            FROM 
                $hansSchema.current_codes  curr 
                    LEFT JOIN (
                            SELECT volcano_id, sent_utc FROM $hansSchema.code_change_date 
                            WHERE curr_prev_old_ind = 'C'
                        ) cdate ON curr.volcano_id = cdate.volcano_id
                    LEFT JOIN (
                            SELECT * FROM $hansSchema.code_change_date 
                            WHERE curr_prev_old_ind = 'P'
                        ) prev ON curr.volcano_id = prev.volcano_id,
                $hansSchema.notice         n,
                $hansSchema.notice_section ns,
                $hansSchema.notice_type    nt,
                $hansSchema.volcano        v
            WHERE
                curr.notice_id             = n.notice_id
                AND n.notice_status_cd     = 'P'
                AND curr.notice_section_id = ns.notice_section_id
                AND n.notice_type_id       = nt.notice_type_id
                AND curr.volcano_id        = v.volcano_id
                AND nt.type_deleted_ind    = 'N'
                AND
                (
                    (
                        curr.alert_level NOT IN ('UNASSIGNED', 'NORMAL')
                        AND curr.color_code_id NOT IN ('UNASSIGNED', 'GREEN')
                    )
                    OR
                    (
                        DATE_SUB(NOW(), INTERVAL 1 DAY) <= curr.sent_utc
                        AND nt.notice_category LIKE ('%Event Response%')
                    )
                    OR
                    (
                        DATE_SUB(NOW(), INTERVAL 1 DAY) <= curr.sent_utc
                        AND (
                                curr.alert_level IN ('UNASSIGNED', 'NORMAL')
                                AND curr.color_code_id IN ('UNASSIGNED', 'GREEN')
                            )
                        AND (
                                IFNULL(prev.alert_level, 'UNASSIGNED') NOT IN ('UNASSIGNED', 'NORMAL')
                                AND IFNULL(prev.color_code, 'UNASSIGNED') NOT IN ('UNASSIGNED', 'GREEN')
                            )
                    )
                )
            ORDER BY
                n.sent_utc DESC, UPPER(v.volcano_name)
            ";

        $nt = new NoticeType();
        return new Resultset(null, $nt, $nt->getReadConnection()->query($sql));
    }

    /**
     * Returns totals for each notice sent by each observatory to give VNS users an idea of how frequently each type of
     * notice is sent.
     */
    public static function getRecentActivity() {

        global $config;
        $hansSchema = $config->application->hansSchema;

        $sql = "
                SELECT 
                    o.obs_fullname, nt.notice_category_sort_order, nt.type_sort_order, 
                    nt.notice_category, nt.notice_type, nt.type_description,
                    MAX(n.sent_utc) AS sent_utc,
                    COUNT(*)        AS obstot
                FROM 
                    $hansSchema.notice n, $hansSchema.observatory o, $hansSchema.notice_type nt
                WHERE 
                    n.observatory_id        = o.observatory_id
                    AND n.notice_type_id    = nt.notice_type_id
                    AND nt.type_deleted_ind = 'N'
                    AND n.notice_status_cd  = 'P' 
                    AND n.sent_utc         IS NOT NULL 
                    AND n.sent_utc         >= DATE_ADD(NOW(), INTERVAL -1 YEAR)
                GROUP BY
                    o.obs_fullname, nt.notice_category_sort_order, nt.type_sort_order, 
                    nt.notice_category, nt.notice_type
                ORDER BY 
                    UPPER(o.obs_fullname), nt.notice_category_sort_order, nt.type_sort_order, 
                    UPPER(nt.notice_category), UPPER(nt.notice_type)
                ";

        $nt = new NoticeType();
        return new Resultset(null, $nt, $nt->getReadConnection()->query($sql));
    }

    public static function getNewUserNoticeTypes() {
        
        global $config;
        $hansSchema = $config->application->hansSchema;
        return RawSql::execReadQuery("
                    SELECT * 
                    FROM $hansSchema.notice_type 
                    WHERE type_deleted_ind = 'N' AND notice_category_sort_order = 1 AND notice_type_cd != 'VV'
                ");
    }
    
    public static function getNoticeTypes() {
        
        global $config;
        $hansSchema = $config->application->hansSchema;
        return RawSql::execReadQuery("
                    SELECT * 
                    FROM   $hansSchema.notice_type 
                    WHERE  type_deleted_ind = 'N'
                    ORDER BY notice_category_sort_order, type_sort_order
                ");
    }
    
}
