<?php

namespace VHP\Vns\Models\hans;

use Phalcon\Mvc\Model;

class Observatory extends Model {

    public $observatory_id;
    public $obs_abbr;
    public $obs_name;
    public $obs_fullname;
    public $identifier_prefix;
    public $obs_timezone;
    public $obs_email;
    public $notice_letter;
    public $last_update;

    public function initialize() {

        global $config;
        $this->setSchema($config->application->hansSchema);

        $this->setSource("observatory");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');

        $this->hasMany("observatory_id", "\VHP\Vns\Models\hans\Volcano", "observatory_id", array("alias" => "volcano"));
    }

}
