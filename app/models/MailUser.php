<?php

namespace VHP\Vns\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class MailUser extends Model {

    public $user_id, $username, $password_hash, $user_fullname, $affiliation_id, $added, $lastlogin,
            $send_reminder, $reminder_sent_dttm, $url_key, $vns_key;

    public function initialize() {

        $this->setSource("mail_user");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');

        $this->hasOne("affiliation_id", "VHP\Vns\Models\MailAffiliation", "affiliation_id", ["alias" => "mail_affiliation"]);
        $this->hasMany("user_id", "\VHP\Vns\Models\MailAddress", "user_id", ["alias" => "mail_address"]);
        $this->hasMany("user_id", "\VHP\Vns\Models\MailType", "user_id", ["alias" => "mail_type"]);
        $this->hasMany("user_id", "\VHP\Vns\Models\MailVolcpref", "user_id", ["alias" => "mail_volcpref"]);
    }

    /**
     * Searches both mail_user and mail_address for matching username/name/email values.
     * Search is case insensitive. Because this is an admin only function simplicity is used over prepared statements with LIKE
     */
    public static function search($searchText) {

        $searchText = trim($searchText);
        if (!$searchText || strlen(trim($searchText)) == 0) {
            return null;
        }

        $searchText = "%" . $searchText . "%";

        $params = [$searchText, $searchText, $searchText];

        $sql = "
			SELECT
				u.user_id, u.username, u.user_fullname, u.added, u.lastlogin, u.send_reminder, 
				a.address_id, a.email, a.active, a.email_format, a.confirmed
			FROM
				mail_user u LEFT JOIN mail_address a ON u.user_id = a.user_id
			WHERE
				UPPER(TRIM(u.username)) LIKE UPPER(TRIM(?))
				OR
				UPPER(TRIM(u.user_fullname)) LIKE UPPER(TRIM(?))
				OR
				UPPER(TRIM(a.email)) LIKE UPPER(TRIM(?))
			ORDER BY
				UPPER(username), UPPER(user_fullname), UPPER(email)
			LIMIT 100
			";

        $mu = new MailUser();
        return new Resultset(null, $mu, $mu->getReadConnection()->query($sql, $params));
    }

}
