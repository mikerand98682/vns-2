<?php

namespace VHP\Vns\Models\hans_mailman;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use VHP\Vns\Models\RawSql;

class HansMailman extends Model {

    public $mailman_id, $sender, $sender_name, $recipient, $recipient_name, $recipient_priority, $subject, $body, $type,
            $unsubscribe_mailto, $unsubscribe_url, $pending_ind, $pending_dttm, $sent_ind, $sent_dttm, $error_ind,
            $error_text, $batch_dttm, $mailman_confirmation_id;

    public function initialize() {

        global $config;
        $this->setSchema($config->mail->hansMailmanSchema);

        $this->setSource("mailman");
        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function getSentSinceTimestamp($email, $timestamp) {

        global $config;
        $hansMailmanSchema = $config->mail->hansMailmanSchema;

        $email = trim(strtolower($email));

        if (strlen(trim($email)) == 0 || !is_numeric($timestamp)) {
            return false;
        }

        $model = new Mailman();
        $sql = "
            SELECT m.recipient, unix_timestamp(m.sent_dttm) AS sent_timestamp
            FROM " . $hansMailmanSchema . ".mailman m
            WHERE LOWER(TRIM(m.recipient)) = ? AND unix_timestamp(m.sent_dttm) > ?
            UNION ALL
            SELECT ma.recipient, unix_timestamp(ma.sent_dttm) AS sent_timestamp
            FROM " . $hansMailmanSchema . ".mailman_archive ma
            WHERE LOWER(TRIM(ma.recipient)) = ? AND unix_timestamp(ma.sent_dttm) > ?
            ";

        $params = [$email, $timestamp, $email, $timestamp];
        return new Resultset(null, $model, $model->getReadConnection()->query($sql, $params));
    }
    
    public static function getTotalSentSinceTimestamp($email, $timestamp) {

        global $config;
        $hansMailmanSchema = $config->mail->hansMailmanSchema;

        $emailCleaned = trim(strtolower($email));
        if (strlen($emailCleaned) == 0 || !is_numeric($timestamp)) {
            return 0;
        }

        $total = RawSql::execReadQueryOneRow("
            SELECT COUNT(*) AS total FROM (
                SELECT m.recipient, unix_timestamp(m.sent_dttm) AS sent_timestamp
                FROM " . $hansMailmanSchema . ".mailman m
                WHERE LOWER(TRIM(m.recipient)) = ? AND unix_timestamp(m.sent_dttm) > ?
                UNION ALL
                SELECT ma.recipient, unix_timestamp(ma.sent_dttm) AS sent_timestamp
                FROM " . $hansMailmanSchema . ".mailman_archive ma
                WHERE LOWER(TRIM(ma.recipient)) = ? AND unix_timestamp(ma.sent_dttm) > ?
            ) a
            ", [$emailCleaned, $timestamp, $emailCleaned, $timestamp])->total;
        
        return $total;
    }
}
