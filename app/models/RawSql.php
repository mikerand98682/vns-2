<?php

namespace VHP\Vns\Models;

use \Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class RawSql extends Model {

    public function initialize() {

        $this->setReadConnectionService('dbr');
        $this->setWriteConnectionService('dbu');
    }

    public static function execReadQuery($sql, $params = []) {

        $rawSql = new RawSql();
        return new Resultset(null, $rawSql, $rawSql->getReadConnection()->query($sql, $params));
    }

    public static function execMasterReadQuery($sql, $params = []) {

        $rawSql = new RawSql();
        return new Resultset(null, $rawSql, $rawSql->getWriteConnection()->query($sql, $params));
    }

    public static function execReadQueryOneRow($sql, $params = []) {

        $rawSql = new RawSql();
        $rs = new Resultset(null, $rawSql, $rawSql->getReadConnection()->query($sql, $params));
        return count($rs) == 0 ? null : $rs[0];
    }

    public static function execMasterReadQueryOneRow($sql, $params = []) {

        $rawSql = new RawSql();
        $rs = new Resultset(null, $rawSql, $rawSql->getWriteConnection()->query($sql, $params));
        return count($rs) == 0 ? null : $rs[0];
    }

    public static function execUpdate($sql, $params = []) {

        $rawSql = new RawSql();
        return $rawSql->getWriteConnection()->execute($sql, $params);
    }

}
